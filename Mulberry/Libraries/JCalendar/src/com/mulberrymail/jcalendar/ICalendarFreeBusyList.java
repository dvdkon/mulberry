/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Vector;

import com.mulberrymail.utils.CompareFunctor;
import com.mulberrymail.utils.VectorUtils;

public class ICalendarFreeBusyList extends Vector {

	private static class comparator extends CompareFunctor
	{
		
		public Object cloneObject(Object o) {

			ICalendarFreeBusy fb = (ICalendarFreeBusy)o;

			return new ICalendarFreeBusy(fb);
		}

		public int compare(Object o1, Object o2) {

			ICalendarFreeBusy fb1 = (ICalendarFreeBusy)o1;
			ICalendarFreeBusy fb2 = (ICalendarFreeBusy)o2;

			ICalendarPeriod p1 = fb1.getPeriod();
			ICalendarPeriod p2 = fb2.getPeriod();

			if (p1.lt(p2))
				return -1;
			else if (p2.lt(p1))
				return 1;
			else
				return 0;
		}
}

	public ICalendarFreeBusyList()
	{
		super();
	}
	
	public void sort()
	{
		VectorUtils.sort(this, new comparator());
	}

}
