/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * This component is used to filter components, properties and values
 * when writing out a calendar object.
 */

package com.mulberrymail.jcalendar;

import com.mulberrymail.utils.Booleanref;
import com.mulberrymail.utils.SimpleMap;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarOutputFilter {

	protected int mType;

	protected boolean mAllSubComponents;

	protected SimpleMap mSubComponents;

	protected boolean mAllProperties;

	protected SimpleMap mProperties;

	public ICalendarOutputFilter(int type) {
		mType = type;
	}

	public int getType() {
		return mType;
	}

	// Test to see if component type can be written out
	public boolean testComponent(int type) {
		return mType == type;
	}

	public boolean isAllSubComponents() {
		return mAllSubComponents;
	}

	public void setAllSubComponents() {
		mAllSubComponents = true;
		mSubComponents = null;
	}

	public void addSubComponent(ICalendarOutputFilter comp) {
		if (mSubComponents == null)
			mSubComponents = new SimpleMap();

		mSubComponents.insert(new Integer(comp.getType()), comp);
	}

	// Test to see if sub-component type can be written out
	public boolean testSubComponent(int type) {
		return mAllSubComponents || (mSubComponents != null)
				&& (mSubComponents.count(new Integer(type)) == 1);
	}

	public boolean hasSubComponentFilters() {
		return (mSubComponents != null);
	}

	public ICalendarOutputFilter getSubComponentFilter(int type)
	{
		if (mSubComponents != null)
			return (ICalendarOutputFilter)mSubComponents.find(new Integer(type));
		else
			return null;
	}

	public boolean isAllProperties() {
		return mAllProperties;
	}

	public void setAllProperties() {
		mAllProperties = true;
		mProperties = null;
	}

	public void addProperty(String name, boolean no_value) {
		if (mProperties == null)
			mProperties = new SimpleMap();

		mProperties.insert(name, new Boolean(no_value));
	}

	public boolean hasPropertyFilters() {
		return (mProperties != null);
	}

	// Test to see if property can be written out and also return whether
	// the property value is used
	public boolean testPropertyValue(String name, Booleanref novalue) {

		if (mAllProperties)
		{
			novalue.set(false);
			return true;
		}
		
		if (mProperties == null)
			return false;
		
		Boolean result = (Boolean) mProperties.find(name);
		if (result != null)
			novalue.set(result.booleanValue());
		return (result != null);
	}
}