/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarVTimezoneStandard extends ICalendarVTimezoneElement {

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINSTANDARD;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDSTANDARD;

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String getVEnd() {
		return sEndDelimiter;
	}

	public ICalendarVTimezoneStandard(int calendar) {
		super(calendar);
	}

	public ICalendarVTimezoneStandard(ICalendarVTimezoneStandard copy) {
		super(copy);
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVTimezoneStandard(this);
	}

	public int getType() {
		return ICalendarComponent.eVTIMEZONESTANDARD;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

}