/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import com.mulberrymail.utils.SimpleMap;
import com.mulberrymail.utils.VectorUtils;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendar extends ICalendarComponentBase {

	// Add/remove components
	public static final int eRemoveAll = 0;

	public static final int eRemoveOnlyThis = 1;

	public static final int eRemoveThisFuture = 2;

	public static final int eFindExact = 0;

	public static final int eFindMaster = 1;

	private static final int eVEVENT = 0;

	private static final int eVTODO = 1;

	private static final int eVJOURNAL = 2;

	private static final int eVFREEBUSY = 3;

	private static final int eVTIMEZONE = 4;

	private static final int eMaxV = 5;

	protected static SimpleMap sICalendars = new SimpleMap();

	protected static int sICalendarRefCtr = 1;

	public static ICalendar sICalendar = new ICalendar();

	protected int mICalendarRef;

	protected boolean mReadOnly;

	protected boolean mDirty;

	protected boolean mTotalReplace;

	protected String mName;

	protected String mDescription;

	protected ICalendarComponentDB mV[];

	// Pseudo properties used for disconnected cache
	protected String mETag;

	/* ICalendarComponentRecordDB mRecordDB; */

	private static SimpleMap sComponents;

	private static SimpleMap sEmbeddedComponents;

	public static ICalendar getICalendar(int ref) {
		ICalendar found = (ICalendar) sICalendars.get(new Integer(ref));
		return found;
	}

	public static final void loadStatics() {
		initComponents();
	}

	public ICalendar() {
		super();

		mICalendarRef = sICalendarRefCtr++;
		sICalendars.put(new Integer(mICalendarRef), this);

		mReadOnly = false;
		mDirty = false;
		mTotalReplace = false;
		mName = new String();
		mDescription = new String();

		mV = new ICalendarComponentDB[eMaxV];
		for (int i = 0; i < eMaxV; i++)
			mV[i] = new ICalendarComponentDB();

		addDefaultProperties();

		// Special init for static item
		if (this == sICalendar) {
			initDefaultTimezones();
		}
	}

	public void close() {
		// Clean up the map items
		for (int i = 0; i < eMaxV; i++)
			mV[i].close();
	}

	public int getRef() {
		return mICalendarRef;
	}

	public void clear() {
		// Broadcast closing before removing components
		/* Broadcast_Message(eBroadcast_Closed, this); */

		// Clean up the map items
		for (int i = 0; i < eMaxV; i++)
			mV[i].removeAllComponents();

		sICalendars.remove(new Integer(mICalendarRef));
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public void editName(String name) {
		if (!mName.equals(name)) {
			// Updated cached value
			mName = name;

			// Remove existing items
			editProperty(ICalendarDefinitions.cICalProperty_XWRCALNAME, name);

			// Mark as dirty
			setDirty(true);

			// Broadcast change
			/* Broadcast_Message(eBroadcast_Edit, this); */
		}
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String description) {
		mDescription = description;
	}

	public void editDescription(String description) {
		if (!mDescription.equals(description)) {
			// Updated cached value
			mDescription = description;

			// Remove existing items
			editProperty(ICalendarDefinitions.cICalProperty_XWRCALDESC,
					description);

			// Mark as dirty
			setDirty(true);

			// Broadcast change
			/* Broadcast_Message(eBroadcast_Edit, this); */
		}
	}

	public String getMethod() {
		String result = null;
		if (hasProperty(ICalendarDefinitions.cICalProperty_METHOD))
			result = loadValueString(ICalendarDefinitions.cICalProperty_METHOD);
		return result != null ? result : new String();
	}

	public void finalise() {
		// Get calendar name if present

		// Get X-WR-CALNAME
		String temps = loadValueString(ICalendarDefinitions.cICalProperty_XWRCALNAME);
		if (temps != null)
			mName = temps;

		// Get X-WR-CALDESC
		temps = loadValueString(ICalendarDefinitions.cICalProperty_XWRCALDESC);
		if (temps != null)
			mDescription = temps;
	}

	public boolean parse(BufferedReader is) {
		// Always init the component maps
		initComponents();

		boolean result = false;

		final int eLookForVCalendar = 0;
		final int eGetPropertyOrComponent = 1;
		final int eGetComponentProperty = 2;
		final int eGetSubComponentProperty = 3;

		int state = eLookForVCalendar;

		// Get lines looking for start of calendar
		String lines[] = new String[2];
		lines[0] = new String();
		lines[1] = new String();
		ICalendarComponent comp = null;
		ICalendarComponent prevcomp = null;
		ICalendarComponentDB compmap = null;

		while ((lines[1] != null) && ICalendarUtils.readFoldedLine(is, lines)) {
			switch (state) {
			case eLookForVCalendar:
				// Look for start
				if (lines[0]
						.equals(ICalendarDefinitions.cICalComponent_BEGINVCALENDAR)) {
					// Next state
					state = eGetPropertyOrComponent;

					// Indicate success at this point
					result = true;
				}
				break;
			case eGetPropertyOrComponent: {
				// Parse property or look for start of component
				SComponentRegister found = (SComponentRegister) sComponents
						.get(lines[0]);
				if (found != null) {
					// Start a new component
					comp = makeComponent(found.mID, getRef());

					// Set the marker for the end of this component and the map
					// to store it in
					compmap = getComponents(found.mType);

					// Change state
					state = eGetComponentProperty;
				} else if (lines[0]
						.equals(ICalendarDefinitions.cICalComponent_ENDVCALENDAR)) {
					// Finalise the current calendar
					finalise();

					// Change state
					state = eLookForVCalendar;
				} else {
					// Parse attribute/value for top-level calendar item
					ICalendarProperty prop = new ICalendarProperty();
					if (prop.parse(lines[0])) {
						// Check for valid property
						if (!validProperty(prop))
							return false;
						else if (!ignoreProperty(prop))
							addProperty(prop);
					}
				}
				break;
			}
			case eGetComponentProperty:
			case eGetSubComponentProperty:
				// Look for end of current component
				if (lines[0].equals(comp.getEndDelimiter())) {
					// Finalise the component (this caches data from the
					// properties)
					comp.finalise();

					// Check whether this is embedded
					if (prevcomp != null) {
						// Embed component in parent and reset to use parent
						if (!prevcomp.addComponent(comp))
							comp = null;
						comp = prevcomp;
						prevcomp = null;

						// Reset state
						state = eGetComponentProperty;
					} else {
						// Check for valid component
						if (!compmap.addComponent(comp))
							comp = null;
						comp = null;
						compmap = null;

						// Reset state
						state = eGetPropertyOrComponent;
					}
				} else {
					// Look for start of embedded component (can only do once)
					SComponentRegister found = (SComponentRegister) sEmbeddedComponents
							.get(lines[0]);
					if ((state != eGetSubComponentProperty) && (found != null)) {
						// Start a new component (saving off the current one)
						prevcomp = comp;
						comp = makeComponent(found.mID, getRef());

						// Reset state
						state = eGetSubComponentProperty;
					} else {
						// Parse attribute/value and store in component
						ICalendarProperty prop = new ICalendarProperty();
						if (prop.parse(lines[0]))
							comp.addProperty(prop);
					}
				}
				break;
			}
		}

		// We need to store all timezones in the static object so they can be
		// accessed by any date object
		if (this != sICalendar) {
			sICalendar.mergeTimezones(this);
		}

		return result;
	}

	public ICalendarComponent parseComponent(BufferedReader is) {
		// Always init rhe component maps
		initComponents();

		ICalendarComponent result = null;

		final int eLookForVCalendar = 0;
		final int eGetPropertyOrComponent = 1;
		final int eGetComponentProperty = 2;
		final int eGetSubComponentProperty = 3;
		final int eGotVCalendar = 4;

		int state = eLookForVCalendar;

		// Get lines looking for start of calendar
		String lines[] = new String[2];
		lines[0] = new String();
		lines[1] = new String();
		ICalendarComponent comp = null;
		ICalendarComponent prevcomp = null;
		ICalendarComponentDB compmap = null;
		boolean got_timezone = false;

		while (ICalendarUtils.readFoldedLine(is, lines)) {
			switch (state) {
			case eLookForVCalendar:
				// Look for start
				if (lines[0]
						.equals(ICalendarDefinitions.cICalComponent_BEGINVCALENDAR)) {
					// Next state
					state = eGetPropertyOrComponent;
				}
				break;
			case eGetPropertyOrComponent: {
				// Parse property or look for start of component
				SComponentRegister found = (SComponentRegister) sComponents
						.get(lines[0]);
				if (found != null) {
					// Start a new component
					comp = makeComponent(found.mID, getRef());

					// Cache as result - but only the first one, we ignore the
					// rest
					if (result == null)
						result = comp;

					// Set the marker for the end of this component and the map
					// to store it in
					compmap = getComponents(found.mType);

					// Look for timezone component to trigger timezone merge
					// only if one is present
					if (found.mType == ICalendarComponent.eVTIMEZONE)
						got_timezone = true;

					// Change state
					state = eGetComponentProperty;
				} else if (lines[0]
						.equals(ICalendarDefinitions.cICalComponent_ENDVCALENDAR)) {
					// Change state
					state = eGotVCalendar;
				} else {
					// Ignore top-level items
				}
				break;
			}
			case eGetComponentProperty:
			case eGetSubComponentProperty:
				// Look for end of current component
				if (lines[0].equals(comp.getEndDelimiter())) {
					// Finalise the component (this caches data from the
					// properties)
					comp.finalise();

					// Check whether this is embedded
					if (prevcomp != null) {
						// Embed component in parent and reset to use parent
						if (!prevcomp.addComponent(comp))
							comp = null;
						comp = prevcomp;
						prevcomp = null;

						// Reset state
						state = eGetComponentProperty;
					} else {
						// Check for valid component
						if (!compmap.addComponent(comp)) {
							if (result == comp)
								result = null;
							comp = null;
						}
						comp = null;
						compmap = null;

						// Reset state
						state = eGetPropertyOrComponent;
					}
				} else {
					// Look for start of embedded component (can only do once)
					SComponentRegister found = (SComponentRegister) sEmbeddedComponents
							.get(lines[0]);
					if ((state != eGetSubComponentProperty) && (found != null)) {
						// Start a new component (saving off the current one)
						prevcomp = comp;
						comp = makeComponent(found.mID, getRef());

						// Reset state
						state = eGetSubComponentProperty;
					} else {
						// Parse attribute/value and store in component
						ICalendarProperty prop = new ICalendarProperty();
						if (prop.parse(lines[0]))
							comp.addProperty(prop);
					}
				}
				break;
			}

			// Exit if we have one - ignore all the rest
			if (state == eGotVCalendar)
				break;
		}

		// We need to store all timezones in the static object so they can be
		// accessed by any date object
		// Only do this if we read in a timezone
		if (got_timezone && (this != sICalendar)) {
			sICalendar.mergeTimezones(this);
		}

		return result;
	}

	public void generate(BufferedWriter os, boolean for_cache) {
		try {
			// Make sure all required timezones are in this object
			includeTimezones();

			// Write header
			os.write(ICalendarDefinitions.cICalComponent_BEGINVCALENDAR);
			os.write("\n");

			// Write properties (we always handle PRODID & VERSION)
			writeProperties(os);

			// Write out each type of component (not VALARMS which are embedded
			// in others)
			// Do VTIMEZONES at the start
			generate(os, mV[eVTIMEZONE], for_cache);
			for (int i = 0; i < eMaxV; i++) {
				if (i != eVTIMEZONE)
					generate(os, mV[i], for_cache);
			}

			// Write trailer
			os.write(ICalendarDefinitions.cICalComponent_ENDVCALENDAR);
			os.write("\n");
		} catch (IOException e) {
		}
	}

	public void generateOne(BufferedWriter os, ICalendarComponent comp) {
		try {
			// Write header
			os.write(ICalendarDefinitions.cICalComponent_BEGINVCALENDAR);
			os.write("\n");

			// Write properties (we always handle PRODID & VERSION)
			writeProperties(os);

			// Make sure each timezone is written out
			HashSet tzids = new HashSet();
			comp.getTimezones(tzids);
			for (Iterator iter = tzids.iterator(); iter.hasNext();) {
				String txt = (String) iter.next();
				ICalendarVTimezone tz = getTimezone((String) txt);
				if (tz == null) {
					// Find it in the static object
					tz = sICalendar.getTimezone((String) txt);
				}
				if (tz != null) {
					tz.generate(os, false);
				}
			}

			// Check for recurring component and potentially write out all
			// instances
			if (comp instanceof ICalendarComponentRecur) {
				// Write this one out first
				comp.generate(os, false);

				// Get list of all instances
				Vector instances = new Vector();
				getRecurrenceInstances(comp.getType(), comp.getUID(), instances);

				// Write each instance out
				for (Enumeration e = instances.elements(); e.hasMoreElements();) {
					ICalendarComponentRecur iter = (ICalendarComponentRecur) e
							.nextElement();

					// Write the component
					iter.generate(os, false);
				}
			} else
				// Write the component
				comp.generate(os, false);

			// Write trailer
			os.write(ICalendarDefinitions.cICalComponent_ENDVCALENDAR);
			os.write("\n");
		} catch (IOException e) {
		}
	}

	public void generateFiltered(BufferedWriter os, ICalendarOutputFilter filter) {

		// First check whether this component matches the filter
		if (!filter.testComponent(ICalendarComponent.eVCALENDAR))
			return;

		try {
			// Make sure all required timezones are in this object
			includeTimezones();

			// Write header
			os.write(ICalendarDefinitions.cICalComponent_BEGINVCALENDAR);
			os.write("\n");

			// Write properties (we always handle PRODID & VERSION)
			writePropertiesFiltered(os, filter);

			// Write out each type of component (not VALARMS which are embedded
			// in others)
			// Do VTIMEZONES at the start
			if (filter.isAllSubComponents()) {
				// Do VTIMEZONES at the start
				generate(os, mV[eVTIMEZONE], false);
				for (int i = 0; i < eMaxV; i++) {
					if (i != eVTIMEZONE)
						generate(os, mV[i], false);
				}
			} else if (filter.hasSubComponentFilters()) {
				// Look for matching filter for each type of sub component and
				// use that
				ICalendarOutputFilter subfilter = filter
						.getSubComponentFilter(ICalendarComponent.eVTIMEZONE);
				if (subfilter != null)
					generateFiltered(os, mV[eVTIMEZONE], subfilter);
				subfilter = filter
						.getSubComponentFilter(ICalendarComponent.eVEVENT);
				if (subfilter != null)
					generateFiltered(os, mV[eVEVENT], subfilter);
				subfilter = filter
						.getSubComponentFilter(ICalendarComponent.eVTODO);
				if (subfilter != null)
					generateFiltered(os, mV[eVTODO], subfilter);
				subfilter = filter
						.getSubComponentFilter(ICalendarComponent.eVJOURNAL);
				if (subfilter != null)
					generateFiltered(os, mV[eVJOURNAL], subfilter);
				subfilter = filter
						.getSubComponentFilter(ICalendarComponent.eVFREEBUSY);
				if (subfilter != null)
					generateFiltered(os, mV[eVFREEBUSY], subfilter);
			}
			// Write trailer
			os.write(ICalendarDefinitions.cICalComponent_ENDVCALENDAR);
			os.write("\n");
		} catch (IOException e) {
		}
	}

	// Get components
	public ICalendarComponentDB getVEvents() {
		return mV[eVEVENT];
	}

	public ICalendarComponentDB getVToDos() {
		return mV[eVTODO];
	}

	public ICalendarComponentDB getVJournals() {
		return mV[eVJOURNAL];
	}

	public ICalendarComponentDB getVFreeBusy() {
		return mV[eVFREEBUSY];
	}

	public ICalendarComponentDB getVTimezone() {
		return mV[eVTIMEZONE];
	}

	public void getAllDBs(Vector list) {
		for (int i = 0; i < eMaxV; i++)
			list.addElement(mV[i]);
	}

	// Disconnected support
	public String getETag() {
		return mETag;
	}

	public void setETag(String etag) {
		mETag = etag;
	}

	/*
	 * ICalendarComponentRecordDB GetRecording() { return mRecordDB; } void
	 * ClearRecording() { mRecordDB.clear(); } boolean NeedsSync() { return
	 * !mRecordDB.empty(); }
	 * 
	 * void ParseCache(istream is); void GenerateCache(ostream os);
	 */
	// Get expanded components
	public void getVEvents(ICalendarPeriod period, Vector list,
			boolean all_day_at_top) {
		// Look at each VEvent
		for (Enumeration e = mV[eVEVENT].elements(); e.hasMoreElements();) {
			ICalendarVEvent vevent = (ICalendarVEvent) e.nextElement();
			vevent.expandPeriod(period, list);
		}

		if (all_day_at_top)
			VectorUtils.sort(list,
					new ICalendarComponentExpanded.sort_by_dtstart_allday());
		else
			VectorUtils.sort(list,
					new ICalendarComponentExpanded.sort_by_dtstart());
	}

	public void getVToDos(boolean only_due, boolean all_dates,
			ICalendarDateTime upto_due_date, Vector list) {
		// Get current date-time less one day to test for completed events
		// during the last day
		ICalendarDateTime minusoneday = new ICalendarDateTime();
		minusoneday.setNowUTC();
		minusoneday.offsetDay(-1);

		ICalendarDateTime today = new ICalendarDateTime();
		today.setToday();

		// Look at each VToDo
		for (Enumeration e = mV[eVTODO].elements(); e.hasMoreElements();) {
			ICalendarVToDo vtodo = (ICalendarVToDo) e.nextElement();

			// Filter out done (that were complted more than a day ago) or
			// cancelled to dos if required
			if (only_due) {
				if (vtodo.getStatus() == ICalendarDefinitions.eStatus_VToDo_Cancelled)
					continue;
				else if ((vtodo.getStatus() == ICalendarDefinitions.eStatus_VToDo_Completed)
						&& (!vtodo.hasCompleted() || (vtodo.getCompleted()
								.lt(minusoneday))))
					continue;
			}

			// Filter out those with end after chosen date if required
			if (!all_dates) {
				if (vtodo.hasEnd() && (vtodo.getEnd().gt(upto_due_date)))
					continue;
				else if (!vtodo.hasEnd() && (today.gt(upto_due_date)))
					continue;
			}

			list.addElement(new ICalendarComponentExpanded(vtodo, null));
		}
	}

	public void getRecurrenceInstances(int type, String uid, Vector items) {
		// Get instances from list
		getComponents(type).getRecurrenceInstances(uid, items);
	}

	public void getRecurrenceInstances(int type, String uid,
			ICalendarDateTimeList ids) {
		// Get instances from list
		getComponents(type).getRecurrenceInstances(uid, ids);
	}

	// Freebusy generation
	public void getVFreeBusy(ICalendarPeriod period, Vector list) {

		// Look at each VFreeBusy
		for(Enumeration e = mV[eVFREEBUSY].elements(); e.hasMoreElements(); )
		{
			ICalendarVFreeBusy vfreebusy = (ICalendarVFreeBusy)e.nextElement();
			vfreebusy.expandPeriodComp(period, list);
		}
	}

	// Freebusy generation
	public ICalendarComponent getFreeBusyComp(Vector periods) {
		// Create new VFREEBUSY component
		ICalendarComponent fb = new ICalendarVFreeBusy(mICalendarRef);

		for (Enumeration e1 = periods.elements(); e1.hasMoreElements();) {
			ICalendarPeriod period = (ICalendarPeriod) e1.nextElement();

			// First create expanded set
			Vector list = new Vector();
			getVEvents(period, list, false);
			if (list.size() == 0)
				continue;

			// Get start/end list for each non-all-day expanded components
			ICalendarDateTimeList dtstart = new ICalendarDateTimeList();
			ICalendarDateTimeList dtend = new ICalendarDateTimeList();
			for (Enumeration e2 = list.elements(); e2.hasMoreElements();) {
				ICalendarComponentExpanded iter = (ICalendarComponentExpanded) e2
						.nextElement();

				// Ignore if all-day
				if (iter.getInstanceStart().isDateOnly())
					continue;

				// Ignore if transparent to free-busy
				String transp = iter.getOwner().getPropertyString(
						ICalendarDefinitions.cICalProperty_TRANSP);
				if ((transp != null)
						&& (transp
								.equals(ICalendarDefinitions.cICalProperty_TRANSPARENT)))
					continue;

				// Add start/end to list
				dtstart.addElement(iter.getInstanceStart());
				dtend.addElement(iter.getInstanceEnd());
			}

			// No longer need the expanded items
			// for(Enumeration e = list.elements(); e.hasMoreElements(); )
			// {
			// ICalendarComponentExpanded iter =
			// (ICalendarComponentExpanded)e.nextElement();
			// iter.close();
			// }
			list.removeAllElements();

			// Create non-overlapping periods as properties in the freebusy
			// component
			Enumeration dtstart_e = dtstart.elements();
			Enumeration dtend_e = dtend.elements();
			ICalendarPeriod temp = new ICalendarPeriod(
					(ICalendarDateTime) dtstart_e.nextElement(),
					(ICalendarDateTime) dtend_e.nextElement());
			for (; dtstart_e.hasMoreElements();) {
				ICalendarDateTime dtstart_iter = (ICalendarDateTime) dtstart_e
						.nextElement();
				ICalendarDateTime dtend_iter = (ICalendarDateTime) dtend_e
						.nextElement();

				// Check for non-overlap
				if (dtstart_iter.gt(temp.getEnd())) {
					// Current period is complete
					fb.addProperty(new ICalendarProperty(
							ICalendarDefinitions.cICalProperty_FREEBUSY, temp));

					// Reset period to new range
					temp = new ICalendarPeriod(dtstart_iter, dtend_iter);
				}

				// They overlap - check for extended end
				if (dtend_iter.gt(temp.getEnd())) {
					// Extend the end
					temp = new ICalendarPeriod(temp.getStart(), dtend_iter);
				}
			}

			// Add remaining period as property
			fb.addProperty(new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_FREEBUSY, temp));
		}

		return fb;
	}

	// Freebusy generation
	public Vector getFreeBusyInfo(ICalendarPeriod period) {
		Vector fb = new Vector();
		// First create expanded set
		{
			Vector list = new Vector();
			getVEvents(period, list, true);
			
			// Get start/end list for each non-all-day expanded components
			for(Enumeration e = list.elements(); e.hasMoreElements(); )
			{
				ICalendarComponentExpanded iter = (ICalendarComponentExpanded)e.nextElement();

				// Ignore if all-day
				if (iter.getInstanceStart().isDateOnly())
					continue;
				
				// Ignore if transparent to free-busy
				String transp = iter.getOwner().getPropertyString(
						ICalendarDefinitions.cICalProperty_TRANSP);
				if ((transp != null) && transp.equals(ICalendarDefinitions.cICalProperty_TRANSPARENT))
					continue;
				
				// Add free busy item to list
				switch(((ICalendarVEvent)iter.getMaster()).getStatus())
				{
				case ICalendarDefinitions.eStatus_VEvent_None:
				case ICalendarDefinitions.eStatus_VEvent_Confirmed:
					fb.add(new ICalendarFreeBusy(ICalendarFreeBusy.eBusy, new ICalendarPeriod(iter.getInstanceStart(), iter.getInstanceEnd())));
					break;
				case ICalendarDefinitions.eStatus_VEvent_Tentative:
					fb.add(new ICalendarFreeBusy(ICalendarFreeBusy.eBusyTentative, new ICalendarPeriod(iter.getInstanceStart(), iter.getInstanceEnd())));
					break;
				case ICalendarDefinitions.eStatus_VEvent_Cancelled:
					// Cancelled => does not contribute to busy time
					break;
				}
			}
		}

		// Now get the VFREEBUSY info
		{
			Vector list2 = new Vector();
			getVFreeBusy(period, list2);
			
			// Get start/end list for each free-busy
			for(Enumeration e = list2.elements(); e.hasMoreElements(); )
			{
				// Expand component and add free busy info to list
				ICalendarVFreeBusy vfreebusy = (ICalendarVFreeBusy)e.nextElement();
				vfreebusy.expandPeriodFB(period, fb);
			}
		}
			
		// Add remaining period as property
		ICalendarFreeBusy.resolveOverlaps(fb);

		return fb;
	}

	// Freebusy generation
	public Vector getFreeBusy(ICalendarPeriod period) {
		Vector fb = new Vector();

		// First create expanded set
		Vector list = new Vector();
		getVEvents(period, list, false);
		if (list.size() == 0)
			return fb;

		// Get start/end list for each non-all-day expanded components
		ICalendarDateTimeList dtstart = new ICalendarDateTimeList();
		ICalendarDateTimeList dtend = new ICalendarDateTimeList();
		for (Enumeration e = list.elements(); e.hasMoreElements();) {
			ICalendarComponentExpanded iter = (ICalendarComponentExpanded) e
					.nextElement();

			// Ignore if all-day
			if (iter.getInstanceStart().isDateOnly())
				continue;

			// Ignore if transparent to free-busy
			String transp = iter.getOwner().getPropertyString(
					ICalendarDefinitions.cICalProperty_TRANSP);
			if ((transp != null)
					&& (transp
							.equals(ICalendarDefinitions.cICalProperty_TRANSPARENT)))
				continue;

			// Add start/end to list
			dtstart.addElement(iter.getInstanceStart());
			dtend.addElement(iter.getInstanceEnd());
		}

		// No longer need the expanded items
		// No longer need the expanded items
		// for(Enumeration e = list.elements(); e.hasMoreElements(); )
		// {
		// ICalendarComponentExpanded iter =
		// (ICalendarComponentExpanded)e.nextElement();
		// iter.close();
		// }
		list.removeAllElements();

		// Create non-overlapping periods as properties in the freebusy
		// component
		Enumeration dtstart_e = dtstart.elements();
		Enumeration dtend_e = dtend.elements();
		ICalendarPeriod temp = new ICalendarPeriod(
				(ICalendarDateTime) dtstart_e.nextElement(),
				(ICalendarDateTime) dtend_e.nextElement());
		for (; dtstart_e.hasMoreElements();) {
			ICalendarDateTime dtstart_iter = (ICalendarDateTime) dtstart_e
					.nextElement();
			ICalendarDateTime dtend_iter = (ICalendarDateTime) dtend_e
					.nextElement();

			// Check for non-overlap
			if (dtstart_iter.gt(temp.getEnd())) {
				// Current period is complete
				fb.addElement(temp);

				// Reset period to new range
				temp = new ICalendarPeriod(dtstart_iter, dtend_iter);
			}

			// They overlap - check for extended end
			if (dtend_iter.gt(temp.getEnd())) {
				// Extend the end
				temp = new ICalendarPeriod(temp.getStart(), dtend_iter);
			}
		}

		// Add remaining period as property
		fb.addElement(temp);

		return fb;
	}

	// Timezone lookups
	public void mergeTimezones(ICalendar cal) {
		// Merge each timezone from other calendar
		for (Enumeration e = cal.mV[eVTIMEZONE].elements(); e.hasMoreElements();) {
			ICalendarComponent iter = (ICalendarComponent) e.nextElement();

			// See whether matching item is already installed
			ICalendarComponent found = (ICalendarComponent) mV[eVTIMEZONE]
					.get(iter.getMapKey());
			if (found == null) {
				// Item does not already exist - so copy and add it
				ICalendarVTimezone copy = new ICalendarVTimezone(
						(ICalendarVTimezone) iter);
				mV[eVTIMEZONE].addComponent(copy);
			} else
				// Merge similar items
				((ICalendarVTimezone) found)
						.mergeTimezone((ICalendarVTimezone) iter);
		}
	}

	public int getTimezoneOffsetSeconds(String timezone, ICalendarDateTime dt) {
		// Find timezone that matches the name (which is the same as the map
		// key)
		ICalendarComponent found = (timezone != null) ? (ICalendarComponent) mV[eVTIMEZONE]
				.get(timezone)
				: null;
		if (found != null) {
			return ((ICalendarVTimezone) found).getTimezoneOffsetSeconds(dt);
		} else
			return 0;
	}

	public String getTimezoneDescriptor(String timezone, ICalendarDateTime dt) {
		// Find timezone that matches the name (which is the same as the map
		// key)
		ICalendarComponent found = (timezone != null) ? (ICalendarComponent) mV[eVTIMEZONE]
				.get(timezone)
				: null;
		if (found != null) {
			return ((ICalendarVTimezone) found).getTimezoneDescriptor(dt);
		} else
			return new String();
	}

	public void getTimezones(Vector tzids) {

		// Get all timezones in a list for sorting
		TreeSet sorted = new TreeSet(new ICalendarVTimezone.SortComparator());
		for (Enumeration e = mV[eVTIMEZONE].elements(); e.hasMoreElements();) {
			ICalendarVTimezone tz = (ICalendarVTimezone) e.nextElement();
			sorted.add(tz);
		}

		// Now add to list in sorted order
		for (Iterator i = sorted.iterator(); i.hasNext();) {
			ICalendarVTimezone tz = (ICalendarVTimezone) i.next();
			tzids.add(tz.getID());
		}
	}

	public ICalendarVTimezone getTimezone(String tzid) {
		// Find timezone that matches the name (which is the same as the map
		// key)
		ICalendarComponent found = (tzid != null) ? (ICalendarComponent) mV[eVTIMEZONE]
				.get(tzid)
				: null;
		if (found != null) {
			return (ICalendarVTimezone) found;
		} else
			return null;
	}

	public void changedComponent(ICalendarComponent comp) {
		// Calendar has changed
		setDirty(true);

		/*
		 * // Record change ICalendarComponentRecord.RecordAction(mRecordDB,
		 * comp, ICalendarComponentRecord.eChanged); // Broadcast change
		 * CComponentAction action(CComponentAction.eChanged, *this, *comp);
		 * Broadcast_Message(eBroadcast_ChangedComponent, &action);
		 */}

	public void addNewVEvent(ICalendarVEvent vevent, boolean moved) {
		// Do not init props if moving
		if (!moved) {
			// Make sure UID is set and unique
			String uid = new String();
			vevent.setUID(uid);

			// Init DTSTAMP to now
			vevent.initDTSTAMP();
		}

		mV[eVEVENT].addComponent(vevent);

		// Calendar has changed
		setDirty(true);

		/*
		 * // Record change ICalendarComponentRecord.RecordAction(mRecordDB,
		 * vevent, ICalendarComponentRecord.eAdded); // Broadcast change
		 * CComponentAction action(CComponentAction.eAdded, *this, *vevent);
		 * Broadcast_Message(eBroadcast_AddedComponent, &action);
		 */}

	public void removeVEvent(ICalendarVEvent vevent) {
		/*
		 * // Record change before delete occurs
		 * ICalendarComponentRecord.RecordAction(mRecordDB, vevent,
		 * ICalendarComponentRecord.eRemoved);
		 */
		// Remove from the map (do not delete here - wait until after broadcast)
		mV[eVEVENT].removeComponent(vevent);

		// Calendar has changed
		setDirty(true);

		/*
		 * // Broadcast change CComponentAction
		 * action(CComponentAction.eRemoved, *this, *vevent);
		 * Broadcast_Message(eBroadcast_RemovedComponent, &action);
		 */
		vevent.close();
	}

	public void removeRecurringVEvent(ICalendarComponentExpanded vevent,
			int recur) {
		// Determine how to delete
		switch (recur) {
		case eRemoveAll:
			// Remove the top-level master event
			removeVEvent((ICalendarVEvent) vevent.getTrueMaster());
			break;
		case eRemoveOnlyThis: {
			// Simply exclude this instance from the top-level master vevent -
			// this works even if this instance is the top-level (first) one
			ICalendarVEvent master = (ICalendarVEvent) vevent.getTrueMaster();

			// NB the vevent will be deleted as part of this so cache the
			// instance start before
			ICalendarDateTime exclude = new ICalendarDateTime(vevent
					.getInstanceStart());

			// The start instance is the RECURRENCE-ID to exclude
			master.excludeRecurrence(exclude);

			// Tell it it has changed (i.e. bump sequence)
			master.changed();
			break;
		}
		case eRemoveThisFuture: {
			// Simply exclude this instance from the master vevent
			ICalendarVEvent master = (ICalendarVEvent) vevent.getTrueMaster();

			// NB the vevent will be deleted as part of this so cache the
			// instance start before
			ICalendarDateTime exclude = new ICalendarDateTime(vevent
					.getInstanceStart());

			// The DTSTART specifies the recurrence that we exclude
			master.excludeFutureRecurrence(exclude);

			// Tell it it has changed (i.e. bump sequence)
			master.changed();
		}
			break;
		}

		// Calendar has changed
		setDirty(true);

		/*
		 * // Broadcast change Broadcast_Message(eBroadcast_Edit, this);
		 */}

	public void addNewVToDo(ICalendarVToDo vtodo, boolean moved) {
		// Do not init props if moving
		if (!moved) {
			// Make sure UID is set and unique
			String uid = new String();
			vtodo.setUID(uid);

			// Init DTSTAMP to now
			vtodo.initDTSTAMP();
		}

		mV[eVTODO].addComponent(vtodo);

		// Calendar has changed
		setDirty(true);

		/*
		 * // Record change ICalendarComponentRecord.RecordAction(mRecordDB,
		 * vtodo, ICalendarComponentRecord.eAdded); // Broadcast change
		 * CComponentAction action(CComponentAction.eAdded, *this, *vtodo);
		 * Broadcast_Message(eBroadcast_AddedComponent, &action);
		 */}

	public void removeVToDo(ICalendarVToDo vtodo, boolean delete_it) {
		/*
		 * // Record change before delete occurs
		 * ICalendarComponentRecord.RecordAction(mRecordDB, vtodo,
		 * ICalendarComponentRecord.eRemoved);
		 */
		// Remove from the map (do not delete here - wait until after broadcast)
		mV[eVTODO].removeComponent(vtodo);

		// Calendar has changed
		setDirty(true);

		/*
		 * // Broadcast change CComponentAction
		 * action(CComponentAction.eRemoved, *this, *vtodo);
		 * Broadcast_Message(eBroadcast_RemovedComponent, &action);
		 */
		// Delete it here after all changes
		if (delete_it) {
			vtodo.close();
			vtodo = null;
		}
	}

	public void changedToDo(ICalendarVToDo vtodo) {
	}

	public ICalendarComponent findComponent(ICalendarComponent orig) {
		return findComponent(orig, eFindExact);
	}

	public ICalendarComponent findComponent(ICalendarComponent orig, int find) {
		// Based on original component type. If we have a component of one type
		// with the same UID
		// as a component of another type something is really f*cked up!
		int index = getComponentIndex(orig.getType());
		if (index != -1)
			return findComponent(mV[index], orig, find);
		else
			return null;
	}

	public void addComponent(ICalendarComponent comp) {
		// Based on original component type. If we have a component of one type
		// with the same UID
		// as a component of another type something is really f*cked up!
		int index = getComponentIndex(comp.getType());
		if (index != -1)
			addComponent(mV[index], comp);
		else
			comp = null;

	}

	public ICalendarComponent getComponentByKey(String mapkey) {

		for (int i = 0; i < eMaxV; i++) {
			ICalendarComponent result = getComponentByKey(mV[i], mapkey);
			if (result != null)
				return result;
		}

		return null;
	}

	public void removeComponentByKey(String mapkey) {
		for (int i = 0; i < eMaxV; i++) {
			if (removeComponentByKey(mV[i], mapkey))
				return;
		}
	}

	public boolean isReadOnly() {
		return mReadOnly;
	}

	public void setReadOnly(boolean ro) {
		mReadOnly = ro;
	}

	// Change state
	public boolean isDirty() {
		return mDirty;
	}

	public void setDirty(boolean dirty) {
		mDirty = dirty;
	}

	public boolean isTotalReplace() {
		return mTotalReplace;
	}

	public void setTotalReplace(boolean replace) {
		mTotalReplace = replace;
	}

	protected ICalendarComponentDB getComponents(int type) {

		return mV[getComponentIndex(type)];
	}

	protected int getComponentIndex(int type) {
		switch (type) {
		case ICalendarComponent.eVEVENT:
			return eVEVENT;
		case ICalendarComponent.eVTODO:
			return eVTODO;
		case ICalendarComponent.eVJOURNAL:
			return eVJOURNAL;
		case ICalendarComponent.eVFREEBUSY:
			return eVFREEBUSY;
		case ICalendarComponent.eVTIMEZONE:
			return eVTIMEZONE;
		default:
			return -1;
		}
	}

	protected void addDefaultProperties() {
		addProperty(new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_PRODID,
				"-//Cyrusoft International, Inc.//Mulberry v4.0//EN"));
		addProperty(new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_VERSION, "2.0"));
		addProperty(new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_CALSCALE, "GREGORIAN"));
	}

	protected void generate(BufferedWriter os, ICalendarComponentDB components,
			boolean for_cache) {
		for (Enumeration e = components.elements(); e.hasMoreElements();) {
			ICalendarComponent iter = (ICalendarComponent) e.nextElement();
			iter.generate(os, for_cache);
		}
	}

	protected void generateFiltered(BufferedWriter os,
			ICalendarComponentDB components, ICalendarOutputFilter filter) {
		for (Enumeration e = components.elements(); e.hasMoreElements();) {
			ICalendarComponent iter = (ICalendarComponent) e.nextElement();
			iter.generateFiltered(os, filter);
		}
	}

	protected boolean validProperty(ICalendarProperty prop) {
		if (prop.getName() == ICalendarDefinitions.cICalProperty_VERSION) {
			ICalendarPlainTextValue tvalue = prop.getTextValue();
			if ((tvalue == null) || !tvalue.getValue().equals("2.0"))
				return false;
		} else if (prop.getName() == ICalendarDefinitions.cICalProperty_CALSCALE) {
			ICalendarPlainTextValue tvalue = prop.getTextValue();
			if ((tvalue == null) || !tvalue.getValue().equals("GREGORIAN"))
				return false;
		}

		return true;
	}

	protected boolean ignoreProperty(ICalendarProperty prop) {
		return (prop.getName()
				.equals(ICalendarDefinitions.cICalProperty_VERSION))
				|| (prop.getName()
						.equals(ICalendarDefinitions.cICalProperty_CALSCALE))
				|| (prop.getName()
						.equals(ICalendarDefinitions.cICalProperty_PRODID));
	}

	protected void includeTimezones() {
		// Get timezone names from each component
		HashSet tzids = new HashSet();
		for (int i = 0; i < eMaxV; i++) {
			if (i != eVTIMEZONE)
				includeTimezones(mV[i], tzids);
		}

		// Make sure each timezone is in current calendar
		for (Iterator iter = tzids.iterator(); iter.hasNext();) {
			String txt = (String) iter.next();
			ICalendarVTimezone tz = getTimezone((String) txt);
			if (tz == null) {
				// Find it in the static object
				tz = sICalendar.getTimezone((String) txt);
				if (tz != null) {
					ICalendarVTimezone dup = new ICalendarVTimezone(tz);
					mV[eVTIMEZONE].addComponent(dup);
				}
			}
		}
	}

	protected void includeTimezones(ICalendarComponentDB components,
			HashSet tzids) {
		for (Enumeration e = components.elements(); e.hasMoreElements();) {
			ICalendarComponent iter = (ICalendarComponent) e.nextElement();
			iter.getTimezones(tzids);
		}
	}

	protected ICalendarComponent findComponent(ICalendarComponentDB db,
			ICalendarComponent orig, int find) {
		ICalendarComponent found = (ICalendarComponent) db
				.get((find == eFindExact) ? orig.getMapKey() : orig.getMasterKey());
		return found;
	}

	protected void addComponent(ICalendarComponentDB db, ICalendarComponent comp) {
		// Just add it without doing anything as this is a copy being made
		// during sync'ing
		if (!db.addComponent(comp))
			comp = null;
	}

	protected ICalendarComponent getComponentByKey(ICalendarComponentDB db,
			String mapkey) {
		ICalendarComponent found = (ICalendarComponent) db.get(mapkey);
		return found;
	}

	protected boolean removeComponentByKey(ICalendarComponentDB db,
			String mapkey) {
		ICalendarComponent result = getComponentByKey(db, mapkey);
		if (result != null) {
			db.removeComponent(result);
			result.close();
			return true;
		} else
			return false;
	}

	private static class SComponentRegister {
		public int mID;

		public int mType;

		public SComponentRegister(int id, int type) {
			mID = id;
			mType = type;
		}
	}

	private static final void initComponents() {
		if (sComponents == null) {
			sComponents = new SimpleMap();

			sComponents.put(ICalendarVEvent.getVBegin(),
					new SComponentRegister(0, ICalendarComponent.eVEVENT));
			sComponents.put(ICalendarVToDo.getVBegin(), new SComponentRegister(
					1, ICalendarComponent.eVTODO));
			sComponents.put(ICalendarVJournal.getVBegin(),
					new SComponentRegister(2, ICalendarComponent.eVJOURNAL));
			sComponents.put(ICalendarVFreeBusy.getVBegin(),
					new SComponentRegister(3, ICalendarComponent.eVFREEBUSY));
			sComponents.put(ICalendarVTimezone.getVBegin(),
					new SComponentRegister(4, ICalendarComponent.eVTIMEZONE));
		}
		if (sEmbeddedComponents == null) {
			sEmbeddedComponents = new SimpleMap();

			sEmbeddedComponents.put(ICalendarVAlarm.getVBegin(),
					new SComponentRegister(5, ICalendarComponent.eVALARM));
			sEmbeddedComponents.put(ICalendarVTimezoneStandard.getVBegin(),
					new SComponentRegister(6, ICalendarComponent.eVTIMEZONE));
			sEmbeddedComponents.put(ICalendarVTimezoneDaylight.getVBegin(),
					new SComponentRegister(7, ICalendarComponent.eVTIMEZONE));
		}
	}

	private ICalendarComponent makeComponent(int id, int calendar) {

		switch (id) {

		case 0:
			return new ICalendarVEvent(calendar);

		case 1:
			return new ICalendarVToDo(calendar);

		case 2:
			return new ICalendarVJournal(calendar);

		case 3:
			return new ICalendarVFreeBusy(calendar);

		case 4:
			return new ICalendarVTimezone(calendar);

		case 5:
			return new ICalendarVAlarm(calendar);

		case 6:
			return new ICalendarVTimezoneStandard(calendar);

		case 7:
			return new ICalendarVTimezoneDaylight(calendar);

		default:
			return null;
		}
	}

	private void initDefaultTimezones() {

	}

}