/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.*;
import java.util.*;

import com.mulberrymail.utils.CompareFunctor;
import com.mulberrymail.utils.IntegerCompare;
import com.mulberrymail.utils.IntegerSort;
import com.mulberrymail.utils.SortFunctor;
import com.mulberrymail.utils.StringUtils;
import com.mulberrymail.utils.VectorUtils;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarRecurrence {

	static final String cFreqMap[] = {
			ICalendarDefinitions.cICalValue_RECUR_SECONDLY,
			ICalendarDefinitions.cICalValue_RECUR_MINUTELY,
			ICalendarDefinitions.cICalValue_RECUR_HOURLY,
			ICalendarDefinitions.cICalValue_RECUR_DAILY,
			ICalendarDefinitions.cICalValue_RECUR_WEEKLY,
			ICalendarDefinitions.cICalValue_RECUR_MONTHLY,
			ICalendarDefinitions.cICalValue_RECUR_YEARLY, null };

	static final String cRecurMap[] = {
			ICalendarDefinitions.cICalValue_RECUR_UNTIL,
			ICalendarDefinitions.cICalValue_RECUR_COUNT,
			ICalendarDefinitions.cICalValue_RECUR_INTERVAL,
			ICalendarDefinitions.cICalValue_RECUR_BYSECOND,
			ICalendarDefinitions.cICalValue_RECUR_BYMINUTE,
			ICalendarDefinitions.cICalValue_RECUR_BYHOUR,
			ICalendarDefinitions.cICalValue_RECUR_BYDAY,
			ICalendarDefinitions.cICalValue_RECUR_BYMONTHDAY,
			ICalendarDefinitions.cICalValue_RECUR_BYYEARDAY,
			ICalendarDefinitions.cICalValue_RECUR_BYWEEKNO,
			ICalendarDefinitions.cICalValue_RECUR_BYMONTH,
			ICalendarDefinitions.cICalValue_RECUR_BYSETPOS,
			ICalendarDefinitions.cICalValue_RECUR_WKST, null };

	static final String cWeekdayMap[] = {
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_SU,
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_MO,
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_TU,
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_WE,
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_TH,
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_FR,
			ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_SA, null };

	static final int cUnknownIndex = 0xFFFFFFFF;

	protected int mFreq;

	protected boolean mUseUntil;

	protected ICalendarDateTime mUntil;

	protected boolean mUseCount;

	protected int mCount;

	protected int mInterval;

	protected Vector mBySeconds;

	protected Vector mByMinutes;

	protected Vector mByHours;

	protected Vector mByDay;

	protected Vector mByMonthDay;

	protected Vector mByYearDay;

	protected Vector mByWeekNo;

	protected Vector mByMonth;

	protected Vector mBySetPos;

	protected int mWeekstart;

	protected boolean mCached;

	protected ICalendarDateTime mCacheStart;

	protected ICalendarDateTime mCacheUpto;

	protected boolean mFullyCached;

	protected ICalendarDateTimeList mRecurrences;

	public class WeekDayNum {
		public int first;

		public int second;

		WeekDayNum(int f, int s) {
			first = f;
			second = s;
		}
	}

	private class WeekDayNumSort extends SortFunctor {
		public boolean less_than(Object o1, Object o2) {

			WeekDayNum w1 = (WeekDayNum) o1;
			WeekDayNum w2 = (WeekDayNum) o2;
			return (w1.first < w2.first) || (w1.first == w2.first)
					&& (w1.second < w2.second);
		}
	}

	private class WeekDayNumCompare extends CompareFunctor {
		public int compare(Object o1, Object o2) {

			WeekDayNum w1 = (WeekDayNum) o1;
			WeekDayNum w2 = (WeekDayNum) o2;
			
			if (w1.first < w2.first)
				return -1;
			else if (w1.first > w2.first)
				return 1;
			else if (w1.second < w2.second)
				return -1;
			else if (w1.second > w2.second)
				return 1;
			else
				return 0;
		}
		
		public Object cloneObject(Object o)
		{
			WeekDayNum w = (WeekDayNum) o;
			return new WeekDayNum(w.first, w.second);
		}
	}

	public ICalendarRecurrence() {
		_init_ICalendarRecurrence();
	}

	public ICalendarRecurrence(ICalendarRecurrence copy) {
		_copy_ICalendarRecurrence(copy);
	}

	private void _init_ICalendarRecurrence() {
		mFreq = ICalendarDefinitions.eRecurrence_YEARLY;

		mUseCount = false;
		mCount = 0;

		mUseUntil = false;
		mUntil = null;

		mInterval = 1;
		mBySeconds = null;
		mByMinutes = null;
		mByHours = null;
		mByDay = null;
		mByMonthDay = null;
		mByYearDay = null;
		mByWeekNo = null;
		mByMonth = null;
		mBySetPos = null;
		mWeekstart = ICalendarDefinitions.eRecurrence_WEEKDAY_MO;

		mCached = false;
		mFullyCached = false;
		mRecurrences = null;
	}

	private void _copy_ICalendarRecurrence(ICalendarRecurrence copy) {
		_init_ICalendarRecurrence();

		mFreq = copy.mFreq;

		mUseCount = copy.mUseCount;
		mCount = copy.mCount;
		mUseUntil = copy.mUseUntil;
		if (copy.mUntil != null)
			mUntil = new ICalendarDateTime(copy.mUntil);

		mInterval = copy.mInterval;
		if (copy.mBySeconds != null)
			mBySeconds = _copy_Integer_Vector(copy.mBySeconds);
		if (copy.mByMinutes != null)
			mByMinutes = _copy_Integer_Vector(copy.mByMinutes);
		if (copy.mByHours != null)
			mByHours = _copy_Integer_Vector(copy.mByHours);
		if (copy.mByDay != null)
			mByDay = _copy_Integer_Vector(copy.mByDay);
		if (copy.mByMonthDay != null)
			mByMonthDay = _copy_Integer_Vector(copy.mByMonthDay);
		if (copy.mByYearDay != null)
			mByYearDay = _copy_Integer_Vector(copy.mByYearDay);
		if (copy.mByWeekNo != null)
			mByWeekNo = _copy_Integer_Vector(copy.mByWeekNo);
		if (copy.mByMonth != null)
			mByMonth = _copy_Integer_Vector(copy.mByMonth);
		if (copy.mBySetPos != null)
			mBySetPos = _copy_Integer_Vector(copy.mBySetPos);
		mWeekstart = copy.mWeekstart;

		mCached = copy.mCached;
		if (copy.mCacheStart != null)
			mCacheStart = new ICalendarDateTime(copy.mCacheStart);
		if (copy.mCacheUpto != null)
			mCacheUpto = new ICalendarDateTime(copy.mCacheUpto);
		mFullyCached = copy.mFullyCached;
		if (copy.mRecurrences != null)
			mRecurrences = new ICalendarDateTimeList(copy.mRecurrences);
	}

	private Vector _copy_Integer_Vector(Vector from) {
		Vector result = new Vector();
		for (Enumeration e = from.elements(); e.hasMoreElements();) {
			result.addElement(new Integer(((Integer) e.nextElement())
					.intValue()));
		}

		return result;
	}

	public boolean equals(ICalendarRecurrence comp) {
		return (mFreq == comp.mFreq) && (mCount == comp.mCount)
				&& (mUseUntil == comp.mUseUntil) && (mUntil == comp.mUntil)
				&& (mInterval == comp.mInterval)
				&& equalsNum(mBySeconds, comp.mBySeconds)
				&& equalsNum(mByMinutes, comp.mByMinutes)
				&& equalsNum(mByHours, comp.mByHours)
				&& equalsDayNum(mByDay, comp.mByDay)
				&& equalsNum(mByMonthDay, comp.mByMonthDay)
				&& equalsNum(mByYearDay, comp.mByYearDay)
				&& equalsNum(mByWeekNo, comp.mByWeekNo)
				&& equalsNum(mByMonth, comp.mByMonth)
				&& equalsNum(mBySetPos, comp.mBySetPos)
				&& (mWeekstart == comp.mWeekstart);
	}

	protected boolean equalsNum(Vector items1, Vector items2) {
		// Check sizes first
		if (items1.size() != items2.size())
			return false;
		else if (items1.size() == 0)
			return true;

		// Copy and sort each one for comparison
		Vector temp1 = new Vector(items1);
		Vector temp2 = new Vector(items2);
		VectorUtils.sort(temp1, new IntegerSort());
		VectorUtils.sort(temp2, new IntegerSort());

		return VectorUtils.equal(temp1, temp2, new IntegerCompare());
	}

	protected boolean equalsDayNum(Vector items1, Vector items2) {
		// Check sizes first
		if (items1.size() != items2.size())
			return false;
		else if (items1.size() == 0)
			return true;

		// Copy and sort each one for comparison
		Vector temp1 = new Vector(items1);
		Vector temp2 = new Vector(items2);
		VectorUtils.sort(temp1, new WeekDayNumSort());
		VectorUtils.sort(temp2, new WeekDayNumSort());

		return VectorUtils.equal(temp1, temp2, new WeekDayNumCompare());
	}

	public int getFreq() {
		return mFreq;
	}

	public void setFreq(int freq) {
		mFreq = freq;
	}

	public boolean getUseUntil() {
		return mUseUntil;
	}

	public void setUseUntil(boolean use_until) {
		mUseUntil = use_until;
	}

	public ICalendarDateTime getUntil() {
		return mUntil;
	}

	public void setUntil(ICalendarDateTime until) {
		mUntil = until;
	}

	public boolean getUseCount() {
		return mUseCount;
	}

	public void setUseCount(boolean use_count) {
		mUseCount = use_count;
	}

	public int getCount() {
		return mCount;
	}

	public void setCount(int count) {
		mCount = count;
	}

	public int getInterval() {
		return mInterval;
	}

	public void setInterval(int interval) {
		mInterval = interval;
	}

	public Vector getByMonth() {
		return mByMonth;
	}

	public void setByMonth(Vector by) {
		mByMonth = _copy_Integer_Vector(by);
	}

	public Vector getByMonthDay() {
		return mByMonthDay;
	}

	public void setByMonthDay(Vector by) {
		mByMonthDay = _copy_Integer_Vector(by);
	}

	public Vector getByDay() {
		return mByDay;
	}

	public void setByDay(Vector by) {
		mByDay = _copy_Integer_Vector(by);
	}

	public Vector getBySetPos() {
		return mBySetPos;
	}

	public void setBySetPos(Vector by) {
		mBySetPos = by;
	}

	public void parse(String data) {
		_init_ICalendarRecurrence();

		// Tokenise using ';'
		StringTokenizer tokens = new StringTokenizer(data, ";");
		String token = null;

		// Look for FREQ= with delimiter
		{
			if (!tokens.hasMoreTokens())
				return;
			token = tokens.nextToken();

			// Make sure it is the token we expect
			if (!token.startsWith(ICalendarDefinitions.cICalValue_RECUR_FREQ))
				return;
			String q = token
					.substring(ICalendarDefinitions.cICalValue_RECUR_FREQ_LEN);

			// Get the FREQ value
			int index = StringUtils.strindexfind(q, cFreqMap, cUnknownIndex);
			if (index == cUnknownIndex)
				return;
			mFreq = index;
		}

		while (tokens.hasMoreTokens()) {
			// Get next token
			token = tokens.nextToken();

			// Determine token type
			int index = StringUtils.strnindexfind(token, cRecurMap,
					cUnknownIndex);
			if (index == cUnknownIndex)
				return;

			// Parse remainder based on index
			String q = token.substring(token.indexOf('=') + 1);

			switch (index) {
			case 0: // UNTIL
				if (mUseCount)
					return;
				mUseUntil = true;
				if (mUntil == null)
					mUntil = new ICalendarDateTime();
				mUntil.parse(q);
				break;
			case 1: // COUNT
				if (mUseUntil)
					return;
				mUseCount = true;
				mCount = Integer.parseInt(q);

				// Must not be less than one
				if (mCount < 1)
					mCount = 1;
				break;
			case 2: // INTERVAL
				mInterval = Integer.parseInt(q);

				// Must NOT be less than one
				if (mInterval < 1)
					mInterval = 1;
				break;
			case 3: // BYSECOND
				if (mBySeconds != null)
					return;
				mBySeconds = new Vector();
				parseList(q, mBySeconds);
				break;
			case 4: // BYMINUTE
				if (mByMinutes != null)
					return;
				mByMinutes = new Vector();
				parseList(q, mByMinutes);
				break;
			case 5: // BYHOUR
				if (mByHours != null)
					return;
				mByHours = new Vector();
				parseList(q, mByHours);
				break;
			case 6: // BYDAY
				if (mByDay != null)
					return;
				mByDay = new Vector();
				parseListDW(q, mByDay);
				break;
			case 7: // BYMONTHDAY
				if (mByMonthDay != null)
					return;
				mByMonthDay = new Vector();
				parseList(q, mByMonthDay);
				break;
			case 8: // BYYEARDAY
				if (mByYearDay != null)
					return;
				mByYearDay = new Vector();
				parseList(q, mByYearDay);
				break;
			case 9: // BYWEEKNO
				if (mByWeekNo != null)
					return;
				mByWeekNo = new Vector();
				parseList(q, mByWeekNo);
				break;
			case 10: // BYMONTH
				if (mByMonth != null)
					return;
				mByMonth = new Vector();
				parseList(q, mByMonth);
				break;
			case 11: // BYSETPOS
				if (mBySetPos != null)
					return;
				mBySetPos = new Vector();
				parseList(q, mBySetPos);
				break;
			case 12: // WKST
			{
				index = StringUtils.strindexfind(q, cWeekdayMap, cUnknownIndex);
				if (index == cUnknownIndex)
					return;
				mWeekstart = index;
			}
				break;
			}
		}
	}

	private void parseList(String txt, Vector list) {
		StringTokenizer tokens = new StringTokenizer(txt, ",");

		while (tokens.hasMoreTokens()) {
			int num = Integer.parseInt(tokens.nextToken());
			list.addElement(new Integer(num));
		}
	}

	private void parseListDW(String txt, Vector list) {
		StringTokenizer tokens = new StringTokenizer(txt, ",");

		while (tokens.hasMoreTokens()) {
			// Get number if present
			String token = tokens.nextToken();
			int num = 0;
			if (Character.isDigit(token.charAt(0)) || (token.charAt(0) == '+')
					|| (token.charAt(0) == '-')) {
				int offset = 0;
				while (Character.isDigit(token.charAt(offset))
						|| (token.charAt(offset) == '+')
						|| (token.charAt(offset) == '-')) {
					offset++;
				}
				num = Integer.parseInt(token.substring(0, offset));
				token = token.substring(offset);
			}

			// Get day
			int index = StringUtils.strnindexfind(token, cWeekdayMap,
					cUnknownIndex);
			if (index == cUnknownIndex)
				return;
			int wday = index;

			list.addElement(new WeekDayNum(num, wday));
		}

	}

	public void generate(BufferedWriter os) {
		try {
			os.write(ICalendarDefinitions.cICalValue_RECUR_FREQ);
			switch (mFreq) {
			case ICalendarDefinitions.eRecurrence_SECONDLY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_SECONDLY);
				break;
			case ICalendarDefinitions.eRecurrence_MINUTELY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_MINUTELY);
				break;
			case ICalendarDefinitions.eRecurrence_HOURLY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_HOURLY);
				break;
			case ICalendarDefinitions.eRecurrence_DAILY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_DAILY);
				break;
			case ICalendarDefinitions.eRecurrence_WEEKLY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKLY);
				break;
			case ICalendarDefinitions.eRecurrence_MONTHLY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_MONTHLY);
				break;
			case ICalendarDefinitions.eRecurrence_YEARLY:
				os.write(ICalendarDefinitions.cICalValue_RECUR_YEARLY);
				break;
			}

			if (mUseCount) {
				os.write(";");
				os.write(ICalendarDefinitions.cICalValue_RECUR_COUNT);
				os.write(Integer.toString(mCount));
			} else if (mUseUntil) {
				os.write(";");
				os.write(ICalendarDefinitions.cICalValue_RECUR_UNTIL);
				mUntil.generate(os);
			}

			if (mInterval > 1) {
				os.write(";");
				os.write(ICalendarDefinitions.cICalValue_RECUR_INTERVAL);
				os.write(Integer.toString(mInterval));
			}

			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYSECOND,
					mBySeconds);
			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYMINUTE,
					mByMinutes);
			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYHOUR,
					mByHours);

			if ((mByDay != null) && (mByDay.size() != 0)) {
				os.write(";");
				os.write(ICalendarDefinitions.cICalValue_RECUR_BYDAY);
				boolean comma = false;
				for (Enumeration e = mByDay.elements(); e.hasMoreElements();) {
					if (comma)
						os.write(",");
					comma = true;

					WeekDayNum iter = (WeekDayNum) e.nextElement();
					if (iter.first != 0)
						os.write(Integer.toString(iter.first));
					switch (iter.second) {
					case ICalendarDefinitions.eRecurrence_WEEKDAY_SU:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_SU);
						break;
					case ICalendarDefinitions.eRecurrence_WEEKDAY_MO:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_MO);
						break;
					case ICalendarDefinitions.eRecurrence_WEEKDAY_TU:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_TU);
						break;
					case ICalendarDefinitions.eRecurrence_WEEKDAY_WE:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_WE);
						break;
					case ICalendarDefinitions.eRecurrence_WEEKDAY_TH:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_TH);
						break;
					case ICalendarDefinitions.eRecurrence_WEEKDAY_FR:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_FR);
						break;
					case ICalendarDefinitions.eRecurrence_WEEKDAY_SA:
						os
								.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_SA);
						break;
					}
				}
			}

			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYMONTHDAY,
					mByMonthDay);
			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYYEARDAY,
					mByYearDay);
			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYWEEKNO,
					mByWeekNo);
			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYMONTH,
					mByMonth);
			generateList(os, ICalendarDefinitions.cICalValue_RECUR_BYSETPOS,
					mBySetPos);

			// MO is the default so we do not need it
			if (mWeekstart != ICalendarDefinitions.eRecurrence_WEEKDAY_MO) {
				os.write(";");
				os.write(ICalendarDefinitions.cICalValue_RECUR_WKST);
				switch (mWeekstart) {
				case ICalendarDefinitions.eRecurrence_WEEKDAY_SU:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_SU);
					break;
				case ICalendarDefinitions.eRecurrence_WEEKDAY_MO:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_MO);
					break;
				case ICalendarDefinitions.eRecurrence_WEEKDAY_TU:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_TU);
					break;
				case ICalendarDefinitions.eRecurrence_WEEKDAY_WE:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_WE);
					break;
				case ICalendarDefinitions.eRecurrence_WEEKDAY_TH:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_TH);
					break;
				case ICalendarDefinitions.eRecurrence_WEEKDAY_FR:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_FR);
					break;
				case ICalendarDefinitions.eRecurrence_WEEKDAY_SA:
					os.write(ICalendarDefinitions.cICalValue_RECUR_WEEKDAY_SA);
					break;
				}
			}
		} catch (IOException e1) {
		}
	}

	private void generateList(BufferedWriter os, String title, Vector list)
			throws IOException {
		if ((list != null) && (list.size() != 0)) {
			os.write(";");
			os.write(title);
			boolean comma = false;
			for (Enumeration e = list.elements(); e.hasMoreElements();) {
				if (comma)
					os.write(",");
				comma = true;
				os.write(((Integer) e.nextElement()).toString());
			}
		}
	}

	public boolean hasBy() {
		return (mBySeconds != null) && !mBySeconds.isEmpty()
				|| (mByMinutes != null) && !mByMinutes.isEmpty()
				|| (mByHours != null) && !mByHours.isEmpty()
				|| (mByDay != null) && !mByDay.isEmpty()
				|| (mByMonthDay != null) && !mByMonthDay.isEmpty()
				|| (mByYearDay != null) && !mByYearDay.isEmpty()
				|| (mByWeekNo != null) && !mByWeekNo.isEmpty()
				|| (mByMonth != null) && !mByMonth.isEmpty()
				|| (mBySetPos != null) && !mBySetPos.isEmpty();
	}

	public boolean isSimpleRule() {
		// One that has no BYxxx rules
		return !hasBy();
	}

	public boolean isAdvancedRule() {
		// One that has BYMONTH,
		// BYMONTHDAY (with no negative value),
		// BYDAY (with multiple unumbered, or numbered with all the same number
		// (1..4, -2, -1)
		// BYSETPOS with +1, or -1 only
		// no others

		// First checks the ones we do not handle at all
		if ((mBySeconds != null) && !mBySeconds.isEmpty()
				|| (mByMinutes != null) && !mByMinutes.isEmpty()
				|| (mByHours != null) && !mByHours.isEmpty()
				|| (mByYearDay != null) && !mByYearDay.isEmpty()
				|| (mByWeekNo != null) && !mByWeekNo.isEmpty())
			return false;

		// Check BYMONTHDAY numbers (we can handle -7...-1, 1..31)
		if (mByMonthDay != null) {
			for (Enumeration e = mByMonthDay.elements(); e.hasMoreElements();) {
				int iter = ((Integer) e.nextElement()).intValue();
				if ((iter < -7) || (iter > 31) || (iter == 0))
					return false;
			}
		}

		// Check BYDAY numbers
		if (mByDay != null) {
			int number = 0;
			for (Enumeration e = mByDay.elements(); e.hasMoreElements();) {
				WeekDayNum iter = (WeekDayNum) e.nextElement();

				// Get the first number
				if (iter == mByDay.firstElement()) {
					number = iter.first;

					// Check number range
					if ((number > 4) || (number < -2))
						return false;
				}

				// If current differs from last, then we have an error
				else if (number != iter.first)
					return false;
			}
		}

		// Check BYSETPOS numbers
		if (mBySetPos != null) {
			if (mBySetPos.size() > 1)
				return false;
			if ((mBySetPos.size() == 1)
					&& (((Integer) mBySetPos.firstElement()).intValue() != -1)
					&& (((Integer) mBySetPos.firstElement()).intValue() != 1))
				return false;
		}

		// If we get here it must be OK
		return true;
	}

	public String getUIDescription() {
		String result;
		try {
			// For now just use iCal item
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			BufferedWriter dout = new BufferedWriter(new OutputStreamWriter(
					bout, "UTF8"));
			generate(dout);
			bout.flush();
			result = bout.toString();
		} catch (IOException e) {
			result = new String();
		}
		return result;
	}

	public void expand(ICalendarDateTime start, ICalendarPeriod range,
			ICalendarDateTimeList items) {

		// Must have recurrence list at this point
		if (mRecurrences == null)
			mRecurrences = new ICalendarDateTimeList();

		// Wipe cache if start is different
		if (mCached && (start != mCacheStart)) {
			mCached = false;
			mFullyCached = false;
			mRecurrences.removeAllElements();
		}

		// Is the current cache complete or does it extaned past the requested
		// range end
		if (!mCached || !mFullyCached
				&& ((mCacheUpto == null) || mCacheUpto.lt(range.getEnd()))) {
			ICalendarPeriod cache_range = new ICalendarPeriod(range);

			// If partially cached just cache from previous cache end up to new
			// end
			if (mCached)
				cache_range = new ICalendarPeriod(mCacheUpto, range.getEnd());

			// Simple expansion is one where there is no BYXXX rule part
			if (!hasBy())
				mFullyCached = simpleExpand(start, cache_range, mRecurrences);
			else
				mFullyCached = complexExpand(start, cache_range, mRecurrences);

			// Set cache values
			mCached = true;
			mCacheStart = start;
			mCacheUpto = range.getEnd();
		}

		// Just return the cached items in the requested range
		for (Enumeration e = mRecurrences.elements(); e.hasMoreElements();) {
			ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();
			if (range.isDateWithinPeriod(iter))
				items.addElement(iter);
		}
	}

	private boolean simpleExpand(ICalendarDateTime start,
			ICalendarPeriod range, ICalendarDateTimeList items) {
		ICalendarDateTime start_iter = new ICalendarDateTime(start);
		int ctr = 0;

		while (true) {
			// Exit if after period we want
			if (range.isDateAfterPeriod(start_iter))
				return false;

			// Add current one to list
			items.addElement(new ICalendarDateTime(start_iter));

			// Get next item
			start_iter.recur(mFreq, mInterval);

			// Check limits
			if (mUseCount) {
				// Bump counter and exit if over
				ctr++;
				if (ctr >= mCount)
					return true;
			} else if (mUseUntil) {
				// Exit if next item is after until (its OK if its the same as
				// UNTIL as
				// UNTIL is inclusive)
				if (start_iter.gt(mUntil))
					return true;
			}
		}
	}

	private boolean complexExpand(ICalendarDateTime start,
			ICalendarPeriod range, ICalendarDateTimeList items) {
		ICalendarDateTime start_iter = new ICalendarDateTime(start);
		int ctr = 0;

		// Always add the initial instance DTSTART
		items.add(new ICalendarDateTime(start));
		if (mUseCount) {
			// Bump counter and exit if over
			ctr++;
			if (ctr >= mCount)
				return true;
		}

		// Need to re-initialise start based on BYxxx rules
		while (true) {
			// Behaviour is based on frequency
			ICalendarDateTimeList set_items = new ICalendarDateTimeList();
			switch (mFreq) {
			case ICalendarDefinitions.eRecurrence_SECONDLY:
				generateSecondlySet(start_iter, set_items);
				break;
			case ICalendarDefinitions.eRecurrence_MINUTELY:
				generateMinutelySet(start_iter, set_items);
				break;
			case ICalendarDefinitions.eRecurrence_HOURLY:
				generateHourlySet(start_iter, set_items);
				break;
			case ICalendarDefinitions.eRecurrence_DAILY:
				generateDailySet(start_iter, set_items);
				break;
			case ICalendarDefinitions.eRecurrence_WEEKLY:
				generateWeeklySet(start_iter, set_items);
				break;
			case ICalendarDefinitions.eRecurrence_MONTHLY:
				generateMonthlySet(start_iter, set_items);
				break;
			case ICalendarDefinitions.eRecurrence_YEARLY:
				generateYearlySet(start_iter, set_items);
				break;
			}

			// Always sort the set as BYxxx rules may not be sorted
			set_items.sort();

			// Process each one in the generated set
			for (Enumeration e = set_items.elements(); e.hasMoreElements();) {
				ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();

				// Ignore if it is before the actual start - we need this
				// because the expansion
				// can go back in time from the real start, but we must exclude
				// those when counting
				// even if they are not within the requested range
				if (iter.lt(start))
					continue;

				// Exit if after period we want
				if (range.isDateAfterPeriod(iter))
					return false;

				// Exit if beyond the UNTIL limit
				if (mUseUntil) {
					// Exit if next item is after until (its OK if its the same
					// as UNTIL as
					// UNTIL is inclusive)
					if (iter.gt(mUntil))
						return true;
				}

				// Add current one to list
				items.addElement(new ICalendarDateTime(iter));

				// Check limits
				if (mUseCount) {
					// Bump counter and exit if over
					ctr++;
					if (ctr >= mCount)
						return true;
				}
			}

			// Exit if after period we want
			if (range.isDateAfterPeriod(start_iter))
				return false;

			// Get next item
			start_iter.recur(mFreq, mInterval);
		}
	}

	public void clear() {
		mCached = false;
		mFullyCached = false;
		if (mRecurrences != null)
			mRecurrences.removeAllElements();
	}

	// IMPORTANT ExcludeFutureRecurrence assumes mCacheStart is setup with the
	// owning VEVENT's DTSTART
	// Currently this method is only called when a recurrence is being removed
	// so
	// the recurrence data should be cached

	// Exclude dates on or after the chosen one
	public void excludeFutureRecurrence(ICalendarDateTime exclude) {
		// Expand the rule upto the exclude date
		ICalendarDateTimeList items = new ICalendarDateTimeList();
		expand(mCacheStart, new ICalendarPeriod(mCacheStart, exclude), items);

		// Adjust UNTIL or add one if no COUNT
		if (getUseUntil() || !getUseCount()) {
			// The last one is just less than the exclude date
			if (items.size() != 0) {
				// Now use the data as the UNTIL
				mUseUntil = true;
				mUntil = (ICalendarDateTime) items.lastElement();
			}
		}

		// Adjust COUNT
		else if (getUseCount()) {
			// The last one is just less than the exclude date
			mUseCount = true;
			mCount = items.size();
		}

		// Now clear out the cached set after making changes
		clear();
	}

	private void generateYearlySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// All possible BYxxx are valid, though some combinations are not

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			byMonthExpand(items);
		}

		if ((mByWeekNo != null) && !mByWeekNo.isEmpty()) {
			byWeekNoExpand(items);
		}

		if ((mByYearDay != null) && !mByYearDay.isEmpty()) {
			byYearDayExpand(items);
		}

		if ((mByMonthDay != null) && !mByMonthDay.isEmpty()) {
			byMonthDayExpand(items);
		}

		if ((mByDay != null) && !mByDay.isEmpty()) {
			// BYDAY is complicated:
			// if BYDAY is included with BYYEARDAY or BYMONTHDAY then it
			// contracts the recurrence set
			// else it expands it, but the expansion depends on the frequency
			// and other BYxxx periodicities

			if (((mByYearDay != null) && !mByYearDay.isEmpty())
					|| ((mByMonthDay != null) && !mByMonthDay.isEmpty()))
				byDayLimit(items);
			else if ((mByWeekNo != null) && !mByWeekNo.isEmpty())
				byDayExpandWeekly(items);
			else if ((mByMonth != null) && !mByMonth.isEmpty())
				byDayExpandMonthly(items);
			else
				byDayExpandYearly(items);
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourExpand(items);
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteExpand(items);
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondExpand(items);
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void generateMonthlySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// Cannot have BYYEARDAY and BYWEEKNO

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			// BYMONTH limits the range of possible values
			byMonthLimit(items);
			if (items.size() == 0)
				return;
		}

		// No BYWEEKNO

		// No BYYEARDAY

		if ((mByMonthDay != null) && !mByMonthDay.isEmpty()) {
			byMonthDayExpand(items);
		}

		if ((mByDay != null) && !mByDay.isEmpty()) {
			// BYDAY is complicated:
			// if BYDAY is included with BYYEARDAY or BYMONTHDAY then it
			// contracts the recurrence set
			// else it expands it, but the expansion depends on the frequency
			// and other BYxxx periodicities

			if (((mByYearDay != null) && !mByYearDay.isEmpty())
					|| ((mByMonthDay != null) && !mByMonthDay.isEmpty()))
				byDayLimit(items);
			else
				byDayExpandMonthly(items);
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourExpand(items);
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteExpand(items);
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondExpand(items);
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void generateWeeklySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// Cannot have BYYEARDAY and BYMONTHDAY

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			// BYMONTH limits the range of possible values
			byMonthLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByWeekNo != null) && !mByWeekNo.isEmpty()) {
			byWeekNoLimit(items);
			if (items.size() == 0)
				return;
		}

		// No BYYEARDAY

		// No BYMONTHDAY

		if ((mByDay != null) && !mByDay.isEmpty()) {
			byDayExpandWeekly(items);
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourExpand(items);
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteExpand(items);
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondExpand(items);
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void generateDailySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// Cannot have BYYEARDAY

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			// BYMONTH limits the range of possible values
			byMonthLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByWeekNo != null) && !mByWeekNo.isEmpty()) {
			byWeekNoLimit(items);
			if (items.size() == 0)
				return;
		}

		// No BYYEARDAY

		if ((mByMonthDay != null) && !mByMonthDay.isEmpty()) {
			byMonthDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByDay != null) && !mByDay.isEmpty()) {
			byDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourExpand(items);
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteExpand(items);
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondExpand(items);
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void generateHourlySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// Cannot have BYYEARDAY

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			// BYMONTH limits the range of possible values
			byMonthLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByWeekNo != null) && !mByWeekNo.isEmpty()) {
			byWeekNoLimit(items);
			if (items.size() == 0)
				return;
		}

		// No BYYEARDAY

		if ((mByMonthDay != null) && !mByMonthDay.isEmpty()) {
			byMonthDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByDay != null) && !mByDay.isEmpty()) {
			byDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteExpand(items);
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondExpand(items);
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void generateMinutelySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// Cannot have BYYEARDAY

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			// BYMONTH limits the range of possible values
			byMonthLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByWeekNo != null) && !mByWeekNo.isEmpty()) {
			byWeekNoLimit(items);
			if (items.size() == 0)
				return;
		}

		// No BYYEARDAY

		if ((mByMonthDay != null) && !mByMonthDay.isEmpty()) {
			byMonthDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByDay != null) && !mByDay.isEmpty()) {
			byDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondExpand(items);
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void generateSecondlySet(ICalendarDateTime start,
			ICalendarDateTimeList items) {
		// Cannot have BYYEARDAY

		// Start with initial date-time
		items.addElement(new ICalendarDateTime(start));

		if ((mByMonth != null) && !mByMonth.isEmpty()) {
			// BYMONTH limits the range of possible values
			byMonthLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByWeekNo != null) && !mByWeekNo.isEmpty()) {
			byWeekNoLimit(items);
			if (items.size() == 0)
				return;
		}

		// No BYYEARDAY

		if ((mByMonthDay != null) && !mByMonthDay.isEmpty()) {
			byMonthDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByDay != null) && !mByDay.isEmpty()) {
			byDayLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByHours != null) && !mByHours.isEmpty()) {
			byHourLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mByMinutes != null) && !mByMinutes.isEmpty()) {
			byMinuteLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mBySeconds != null) && !mBySeconds.isEmpty()) {
			bySecondLimit(items);
			if (items.size() == 0)
				return;
		}

		if ((mBySetPos != null) && !mBySetPos.isEmpty()) {
			bySetPosLimit(items);
		}
	}

	private void byMonthExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYMONTH and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByMonth.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setMonth(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void byWeekNoExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYWEEKNO and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByWeekNo.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setWeekNo(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void byYearDayExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYYEARDAY and generating a new date-time for it
			// and insert into output
			for (Enumeration e2 = mByYearDay.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setYearDay(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void byMonthDayExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYMONTHDAY and generating a new date-time for it
			// and insert into output
			for (Enumeration e2 = mByMonthDay.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setMonthDay(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void byDayExpandYearly(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYDAY and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByDay.elements(); e2.hasMoreElements();) {
				WeekDayNum iter2 = (WeekDayNum) e2.nextElement();

				// Numeric value means specific instance
				if (iter2.first != 0) {
					ICalendarDateTime temp = new ICalendarDateTime(iter1);
					temp.setDayOfWeekInYear(iter2.first, iter2.second);
					output.addElement(temp);
				} else {
					// Every matching day in the year
					for (int i = 1; i < 54; i++) {
						ICalendarDateTime temp = new ICalendarDateTime(iter1);
						temp.setDayOfWeekInYear(i, iter2.second);
						if (temp.getYear() == (iter1).getYear())
							output.addElement(temp);
					}
				}
			}
		}

		dates.copy(output);
	}

	private void byDayExpandMonthly(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYDAY and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByDay.elements(); e2.hasMoreElements();) {
				WeekDayNum iter2 = (WeekDayNum) e2.nextElement();

				// Numeric value means specific instance
				if (iter2.first != 0) {
					ICalendarDateTime temp = new ICalendarDateTime(iter1);
					temp.setDayOfWeekInMonth(iter2.first, iter2.second);
					output.addElement(temp);
				} else {
					// Every matching day in the month
					for (int i = 1; i < 6; i++) {
						ICalendarDateTime temp = new ICalendarDateTime(iter1);
						temp.setDayOfWeekInMonth(i, iter2.second);
						if (temp.getMonth() == iter1.getMonth())
							output.addElement(temp);
					}
				}
			}
		}

		dates.copy(output);
	}

	private void byDayExpandWeekly(ICalendarDateTimeList dates) {
		// Must take into account the WKST value

		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYDAY and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByDay.elements(); e2.hasMoreElements();) {
				WeekDayNum iter2 = (WeekDayNum) e2.nextElement();

				// Numeric values are meaningless so ignore them
				if (iter2.first == 0) {
					ICalendarDateTime temp = new ICalendarDateTime(iter1);

					// Determine amount of offset to apply to temp to shift it
					// to the start of the week (backwards)
					int week_start_offset = mWeekstart - temp.getDayOfWeek();
					if (week_start_offset > 0)
						week_start_offset -= 7;

					// Determine amount of offset from the start of the week to
					// the day we want (forwards)
					int day_in_week_offset = (iter2).second - mWeekstart;
					if (day_in_week_offset < 0)
						day_in_week_offset += 7;

					// Apply offsets
					temp.offsetDay(week_start_offset + day_in_week_offset);
					output.addElement(temp);
				}
			}
		}

		dates.copy(output);
	}

	private void byHourExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYHOUR and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByHours.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setHours(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void byMinuteExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYMINUTE and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mByMinutes.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setMinutes(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void bySecondExpand(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYSECOND and generating a new date-time for it and
			// insert into output
			for (Enumeration e2 = mBySeconds.elements(); e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				ICalendarDateTime temp = new ICalendarDateTime(iter1);
				temp.setSeconds(iter2);
				output.addElement(temp);
			}
		}

		dates.copy(output);
	}

	private void byMonthLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYMONTH and indicate keep if input month matches
			boolean keep = false;
			for (Enumeration e2 = mByMonth.elements(); !keep
					&& e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				keep = (iter1.getMonth() == iter2);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void byWeekNoLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYWEEKNO and indicate keep if input month matches
			boolean keep = false;
			for (Enumeration e2 = mByWeekNo.elements(); !keep
					&& e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				keep = iter1.isWeekNo(iter2);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void byMonthDayLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYMONTHDAY and indicate keep if input month
			// matches
			boolean keep = false;
			for (Enumeration e2 = mByMonthDay.elements(); !keep
					&& e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				keep = iter1.isMonthDay(iter2);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void byDayLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYDAY and indicate keep if input month matches
			boolean keep = false;
			for (Enumeration e2 = mByDay.elements(); !keep
					&& e2.hasMoreElements();) {
				WeekDayNum iter2 = (WeekDayNum) e2.nextElement();
				keep = iter1.isDayOfWeekInMonth(iter2.first, iter2.second);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void byHourLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYHOUR and indicate keep if input hour matches
			boolean keep = false;
			for (Enumeration e2 = mByHours.elements(); !keep
					&& e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				keep = (iter1.getHours() == iter2);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void byMinuteLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYMINUTE and indicate keep if input minute matches
			boolean keep = false;
			for (Enumeration e2 = mByMinutes.elements(); !keep
					&& e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				keep = (iter1.getMinutes() == iter2);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void bySecondLimit(ICalendarDateTimeList dates) {
		// Loop over all input items
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		for (Enumeration e1 = dates.elements(); e1.hasMoreElements();) {
			ICalendarDateTime iter1 = (ICalendarDateTime) e1.nextElement();

			// Loop over each BYSECOND and indicate keep if input second matches
			boolean keep = false;
			for (Enumeration e2 = mBySeconds.elements(); !keep
					&& e2.hasMoreElements();) {
				int iter2 = ((Integer) e2.nextElement()).intValue();
				keep = (iter1.getSeconds() == iter2);
			}
			if (keep)
				output.addElement(iter1);
		}

		dates.copy(output);
	}

	private void bySetPosLimit(ICalendarDateTimeList dates) {
		// The input dates MUST be sorted in order for this to work properly
		dates.sort();

		// Loop over each BYSETPOS and extract the relevant component from the
		// input array and add to the output
		ICalendarDateTimeList output = new ICalendarDateTimeList();
		int input_size = dates.size();
		for (Enumeration e = mBySetPos.elements(); e.hasMoreElements();) {
			int iter = ((Integer) e.nextElement()).intValue();

			if (iter > 0) {
				// Positive values are offset from the start
				if ((iter) <= input_size)
					output.addElement(dates.elementAt(iter - 1));
			} else if (iter < 0) {
				// Negative values are offset from the end
				if (-(iter) <= input_size)
					output.addElement(dates.elementAt(input_size + iter));
			}
		}

		dates.copy(output);
	}

}