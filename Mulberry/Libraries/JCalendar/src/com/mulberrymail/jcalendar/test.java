/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * Created on Dec 6, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.mulberrymail.jcalendar;


/**
 * @author cyrusdaboo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class test {

	public static void main(String[] args) {
		
/*		try {
			// Read in some data
	        BufferedReader in = new BufferedReader(
	            new InputStreamReader(new FileInputStream("/users/cyrusdaboo/Documents/US.ics"), "UTF8"));
	        
	        ICalendar cal = new ICalendar();
	        cal.parse(in);

	        // Write out the data
	        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
	                new FileOutputStream("/users/cyrusdaboo/Documents/USNew.ics"), "UTF8"));
	        	cal.generate(out, false);
	            out.close();	        
	    } catch (IOException e) {
	    }
*/		
/*		ICalendarDateTimeList dtl1 = new ICalendarDateTimeList();
		ICalendarDateTimeList dtl2 = new ICalendarDateTimeList();

		dtl1.addElement(new ICalendarDateTime(2004, 12, 13, null));
		dtl1.addElement(new ICalendarDateTime(2004, 12, 12, null));
		dtl1.addElement(new ICalendarDateTime(2004, 12, 11, null));
		dtl1.addElement(new ICalendarDateTime(2004, 12, 10, null));
		dtl1.sort();

		dtl2.addElement(new ICalendarDateTime(2004, 12, 12, null));
		dtl2.addElement(new ICalendarDateTime(2004, 12, 10, null));
		dtl2.sort();

		ICalendarDateTimeList dtl3 = new ICalendarDateTimeList();
		ICalendarDateTimeList.set_difference(dtl1, dtl2, dtl3);
		for(int i = 0; i  < dtl3.size(); i++)
			System.out.println(((ICalendarDateTime)dtl3.elementAt(i)).getText());
*/
		ICalendarRecurrence r = new ICalendarRecurrence();
		r.setFreq(ICalendarDefinitions.eRecurrence_DAILY);
		r.setCount(1000);
		
		ICalendarDateTime start = new ICalendarDateTime(2006, 1, 1, 17, 0, 0, null);
		ICalendarDateTime end = new ICalendarDateTime(2050, 1, 1, 0, 0, 0, null);
		ICalendarPeriod p = new ICalendarPeriod(start, end);
		ICalendarDateTimeList dtl = new ICalendarDateTimeList();
		r.expand(start, p, dtl);
		int ctr = dtl.size();
		for(int i = 0; i < dtl.size(); i++)
			System.out.println(((ICalendarDateTime)dtl.elementAt(i)).getLocaleDate(ICalendarDateTime.eAbbrevDate));
	}
}
