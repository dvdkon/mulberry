/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarVTimezoneDaylight extends ICalendarVTimezoneElement {

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINDAYLIGHT;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDDAYLIGHT;

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String getVEnd() {
		return sEndDelimiter;
	}

	public ICalendarVTimezoneDaylight(int calendar) {
		super(calendar);
	}

	public ICalendarVTimezoneDaylight(ICalendarVTimezoneDaylight copy) {
		super(copy);
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVTimezoneDaylight(this);
	}

	public int getType() {
		return ICalendarComponent.eVTIMEZONEDAYLIGHT;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

}