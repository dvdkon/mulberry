/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarUtils {

	public static boolean readFoldedLine(BufferedReader is, String[] lines) {
		boolean fail = false;

		// If line2 already has data, transfer that into line1
		if (lines[1].length() != 0)
			lines[0] = lines[1];
		else {
			// Fill first line
			try {
				lines[0] = is.readLine();
			} catch (IOException ex) {
				fail = true;
			}
			if (fail || (lines[0] == null) || (lines[0].length() == 0))
				return false;
		}

		// Now loop looking ahead at the next line to see if it is folded
		while (true) {
			// Get next line
			try {
				lines[1] = is.readLine();
			} catch (IOException ex) {
				fail = true;
			}
			if (fail || (lines[1] == null))
				return true;

			// Does it start with a space => folded
			if ((lines[1].length() != 0)
					&& Character.isWhitespace(lines[1].charAt(0)))
				// Copy folded line (without space) to current line and cycle
				// for more
				lines[0] = lines[0].concat(lines[1].substring(1));
			else
				// Not folded - just exit loop
				break;
		}

		return true;
	}

	public static int find_first_of(String text, String tokens, int offset) {
		for (int i = offset; i < text.length(); i++) {
			if (tokens.indexOf(text.charAt(i)) != -1)
				return i;
		}
		return -1;
	}

	public static void writeTextValue(BufferedWriter os, String value) {
		try {
			int start_pos = 0;
			int end_pos = find_first_of(value, "\r\n;\\,", start_pos);
			if (end_pos != -1) {
				while (true) {
					// Write current segment
					os.write(value.substring(start_pos, end_pos));

					// Write escape
					os.write("\\");
					switch (value.charAt(end_pos)) {
					case '\r':
						os.write("r");
						break;
					case '\n':
						os.write("n");
						break;
					case ';':
						os.write(";");
						break;
					case '\\':
						os.write("\\");
						break;
					case ',':
						os.write(",");
						break;
					}

					// Bump past escapee and look for next segment
					start_pos = end_pos + 1;

					end_pos = find_first_of(value, "\r\n;\\,", start_pos);
					if (end_pos == -1) {
						os.write(value.substring(start_pos));
						break;
					}
				}
			} else
				os.write(value);
		} catch (IOException e) {
		}
	}

	public static String decodeTextValue(String value) {
		StringBuffer os = new StringBuffer();

		int start_pos = 0;
		int end_pos = find_first_of(value, "\\", start_pos);
		int size_pos = value.length();
		if (end_pos != -1) {
			while (true) {
				// Write current segment upto but not including the escape char
				os.append(value.substring(start_pos, end_pos));

				// Bump to escapee char but not past the end
				end_pos++;
				if (end_pos >= size_pos)
					break;

				// Unescape
				switch (value.charAt(end_pos)) {
				case 'r':
					os.append('\r');
					break;
				case 'n':
					os.append('\n');
					break;
				case ';':
					os.append(';');
					break;
				case '\\':
					os.append('\\');
					break;
				case ',':
					os.append(',');
					break;
				}

				// Bump past escapee and look for next segment (not past the
				// end)
				start_pos = end_pos + 1;
				if (start_pos >= size_pos)
					break;

				end_pos = find_first_of(value, "\\", start_pos);
				if (end_pos == -1) {
					os.append(value.substring(start_pos));
					break;
				}
			}
		} else
			os.append(value);

		return os.toString();
	}

	// Date/time calcs
	public static int daysInMonth(int month, int year) {
		// NB month is 1..12 so use dummy value at start of array to avoid index
		// adjustment
		int days_in_month[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,
				31 };
		int days_in_month_leap[] = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31,
				30, 31 };

		return isLeapYear(year) ? days_in_month_leap[month]
				: days_in_month[month];
	}

	public static int daysUptoMonth(int month, int year) {
		// NB month is 1..12 so use dummy value at start of array to avoid index
		// adjustment
		int days_upto_month[] = { 0, 0, 31, 59, 90, 120, 151, 181, 212, 243,
				273, 304, 334 };
		int days_upto_month_leap[] = { 0, 0, 31, 60, 91, 121, 152, 182, 213,
				244, 274, 305, 335 };

		return isLeapYear(year) ? days_upto_month_leap[month]
				: days_upto_month[month];
	}

	public static boolean isLeapYear(int year) {
		if (year <= 1752)
			return (year % 4 == 0);
		else
			return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
	}

	public static int leapDaysSince1970(int year_offset) {
		if (year_offset > 2)
			return (year_offset + 1) / 4;
		else if (year_offset < -1)
			return (year_offset - 2) / 4;
		else
			return 0;
	}

	public static int getLocalTimezoneOffsetSeconds() {
		Calendar rightNow = Calendar.getInstance();
		int tzoffset = rightNow.get(Calendar.ZONE_OFFSET);
		int dstoffset = rightNow.get(Calendar.DST_OFFSET);
		return -(tzoffset + dstoffset) / 1000;
	}

	// Packed date
	public static int packDate(int year, int month, int day) {
		return (year << 16) | (month << 8) | (day + 128);
	}

	public static void unpackDate(int data, int[] unpacked) {
		unpacked[0] = (data & 0xFFFF0000) >> 16;
		unpacked[1] = (data & 0x0000FF00) >> 8;
		unpacked[2] = (data & 0xFF) - 128;
	}

	public static int unpackDateYear(int data) {
		return (data & 0xFFFF0000) >> 16;
	}

	public static int unpackDateMonth(int data) {
		return (data & 0x0000FF00) >> 8;
	}

	public static int unpackDateDay(int data) {
		return (data & 0xFF) - 128;
	}

	// Display elements
	public static class int_pair {
		public int first;

		public int second;

		public int_pair(int f, int s) {
			first = f;
			second = s;
		}

		public void make_pair(int f, int s) {
			first = f;
			second = s;
		}
	}

	public static void getMonthTable(int month, int year, int weekstart,
			Vector table, int_pair today_index) {
		// Get today
		ICalendarDateTime today = ICalendarDateTime.getToday(null);
		today_index.make_pair(-1, -1);

		// Start with empty table
		table.removeAllElements();

		// Determine first weekday in month
		ICalendarDateTime temp = new ICalendarDateTime(year, month, 1, null);
		int row = -1;
		int initial_col = temp.getDayOfWeek() - weekstart;
		if (initial_col < 0)
			initial_col += 7;
		int col = initial_col;

		// Counters
		int max_day = daysInMonth(month, year);

		// Fill up each row
		for (int day = 1; day <= max_day; day++) {
			// Insert new row if we are at the start of a row
			if ((col == 0) || (day == 1)) {
				Vector row_data = new Vector();
				for (int i = 0; i < 7; i++)
					row_data.addElement(new Integer(0));
				table.addElement(row_data);
				row++;
			}

			// Set the table item to the curret day
			((Vector) table.elementAt(row)).setElementAt(new Integer(packDate(
					temp.getYear(), temp.getMonth(), day)), col);

			// Check on today
			if ((temp.getYear() == today.getYear())
					&& (temp.getMonth() == today.getMonth())
					&& (day == today.getDay())) {
				today_index.make_pair(row, col);
			}

			// Bump column (modulo 7)
			col++;
			if (col > 6)
				col = 0;
		}

		// Add next month to remainder
		temp.offsetMonth(1);
		if (col != 0) {
			for (int day = 1; col < 7; day++, col++) {
				((Vector) table.elementAt(row)).setElementAt(new Integer(
						packDate(temp.getYear(), temp.getMonth(), -day)), col);

				// Check on today
				if ((temp.getYear() == today.getYear())
						&& (temp.getMonth() == today.getMonth())
						&& (day == today.getDay())) {
					today_index.make_pair(row, col);
				}
			}
		}

		// Add previous month to start
		temp.offsetMonth(-2);
		if (initial_col != 0) {
			int day = daysInMonth(temp.getMonth(), temp.getYear());
			for (int back_col = initial_col - 1; back_col >= 0; back_col--, day--) {
				((Vector) table.elementAt(0)).setElementAt(new Integer(
						packDate(temp.getYear(), temp.getMonth(), -day)),
						back_col);

				// Check on today
				if ((temp.getYear() == today.getYear())
						&& (temp.getMonth() == today.getMonth())
						&& (day == today.getDay())) {
					today_index.make_pair(0, back_col);
				}
			}
		}
	}
}