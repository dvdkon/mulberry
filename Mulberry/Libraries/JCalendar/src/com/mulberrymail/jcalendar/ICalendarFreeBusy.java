/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Vector;

public class ICalendarFreeBusy {

	public static final int eFree = 0;

	public static final int eBusyTentative = 1;

	public static final int eBusyUnavailable = 2;

	public static final int eBusy = 3;

	protected int mType; // Type of busy

	protected ICalendarPeriod mPeriod; // Period for

	// busy time

	public ICalendarFreeBusy() {
		mType = eFree;
	}

	public ICalendarFreeBusy(int type) {
		mType = type;
	}

	public ICalendarFreeBusy(int type, ICalendarPeriod period) {
		mType = type;
		mPeriod = new ICalendarPeriod(period);
	}

	public ICalendarFreeBusy(ICalendarFreeBusy copy) {
		_copy_ICalendarFreeBusy(copy);
	}

	private void _copy_ICalendarFreeBusy(ICalendarFreeBusy copy) {
		mType = copy.mType;
		mPeriod = new ICalendarPeriod(copy.mPeriod);
	}

	public void setType(int type) // Set type
	{
		mType = type;
	}

	public int getType() // Get type
	{
		return mType;
	}

	public void setPeriod(ICalendarPeriod period) // Set period
	{
		mPeriod = new ICalendarPeriod(period);
	}

	public ICalendarPeriod getPeriod() // Get periods
	{
		return mPeriod;
	}

	public boolean isPeriodOverlap(ICalendarPeriod period) {
		return mPeriod.isPeriodOverlap(period);
	}

	public static void resolveOverlaps(Vector fb) {
		// TO DO
	}

}
