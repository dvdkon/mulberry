/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import com.mulberrymail.utils.SortFunctor;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarComponentExpanded {

	protected ICalendarComponentRecur mOwner;

	protected ICalendarDateTime mInstanceStart;

	protected ICalendarDateTime mInstanceEnd;

	protected boolean mRecurring;

	public static class sort_by_dtstart_allday extends SortFunctor {
		public boolean less_than(Object o1, Object o2) {
			ICalendarComponentExpanded e1 = (ICalendarComponentExpanded) o1;
			ICalendarComponentExpanded e2 = (ICalendarComponentExpanded) o2;

			if (e1.mInstanceStart.isDateOnly()
					&& e2.mInstanceStart.isDateOnly())
				return e1.mInstanceStart.lt(e2.mInstanceStart);
			else if (e1.mInstanceStart.isDateOnly())
				return true;
			else if (e2.mInstanceStart.isDateOnly())
				return false;
			else if (e1.mInstanceStart.eq(e2.mInstanceStart)) {
				if (e1.mInstanceEnd.eq(e2.mInstanceEnd))
					// Put ones created earlier in earlier columns in day view
					return e1.getOwner().getStamp()
							.lt(e2.getOwner().getStamp());
				else
					// Put ones that end later in earlier columns in day view
					return e1.mInstanceEnd.gt(e2.mInstanceEnd);
			} else
				return e1.mInstanceStart.lt(e2.mInstanceStart);
		}
	}

	public static class sort_by_dtstart extends SortFunctor {
		public boolean less_than(Object o1, Object o2) {
			ICalendarComponentExpanded e1 = (ICalendarComponentExpanded) o1;
			ICalendarComponentExpanded e2 = (ICalendarComponentExpanded) o2;

			if (e1.mInstanceStart.eq(e2.mInstanceStart)) {
				if (e1.mInstanceStart.isDateOnly()
						^ e2.mInstanceStart.isDateOnly())
					return e1.mInstanceStart.isDateOnly();
				else
					return false;
			} else
				return e1.mInstanceStart.lt(e2.mInstanceStart);
		}
	}

	public ICalendarComponentExpanded(ICalendarComponentRecur owner,
			ICalendarDateTime rid) {
		mOwner = owner;
		initFromOwner(rid);
	}

	public ICalendarComponentExpanded(ICalendarComponentExpanded copy) {
		_copy_ICalendarComponentExpanded(copy);
	}

	public void close()
	{
		// Clean-up
		mOwner = null;
	}

	public ICalendarComponentRecur getOwner() {
		return mOwner;
	}

	public ICalendarComponentRecur getMaster() {
		return mOwner;
	}

	public ICalendarComponentRecur getTrueMaster() {
		return mOwner.getMaster();
	}

	public ICalendarDateTime getInstanceStart() {
		return mInstanceStart;
	}

	public ICalendarDateTime getInstanceEnd() {
		return mInstanceEnd;
	}

	public boolean recurring() {
		return mRecurring;
	}

	public boolean isNow() {
		// Check instance start/end against current date-time
		ICalendarDateTime now = ICalendarDateTime.getNowUTC();
		return mInstanceStart.le(now) && mInstanceEnd.gt(now);
	}

	protected void initFromOwner(ICalendarDateTime rid) {
		// There are four possibilities here:
		//
		// 1: this instance is the instance for the master component
		//
		// 2: this instance is an expanded instance derived directly from the
		// master component
		//
		// 3: This instance is the instance for a slave (overridden recurrence
		// instance)
		//
		// 4: This instance is the expanded instance for a slave with a RANGE
		// parameter
		//

		// rid is not set if the owner is the master (case 1)
		if (rid == null) {
			// Just get start/end from owner
			mInstanceStart = mOwner.getStart();
			mInstanceEnd = mOwner.getEnd();
			mRecurring = false;
		}

		// If the owner is not a recurrence instance then it is case 2
		else if (!mOwner.isRecurrenceInstance()) {
			// Derive start/end from rid and duration of master

			// Start of the recurrence instance is the recurrence id
			mInstanceStart = rid;

			// End is based on original events settings
			mInstanceEnd = mInstanceStart.add(mOwner.getEnd().subtract(
					mOwner.getStart()));

			mRecurring = true;
		}

		// If the owner is a recurrence item and the passed in rid is the same
		// as the component rid we have case 3
		else if (rid.eq(mOwner.getRecurrenceID())) {
			// Derive start/end directly from the owner
			mInstanceStart = mOwner.getStart();
			mInstanceEnd = mOwner.getEnd();

			mRecurring = true;
		}

		// case 4 - the complicated one!
		else {
			// We need to use the rid as the starting point, but adjust it by
			// the offset between the slave's
			// rid and its start
			mInstanceStart = rid.add(mOwner.getStart().subtract(
					mOwner.getRecurrenceID()));

			// End is based on duration of owner
			mInstanceEnd = mInstanceStart.add(mOwner.getEnd().subtract(
					mOwner.getStart()));

			mRecurring = true;
		}
	}

	private void _copy_ICalendarComponentExpanded(
			ICalendarComponentExpanded copy) {
		mOwner = copy.mOwner;
		mInstanceStart = new ICalendarDateTime(copy.mInstanceStart);
		mInstanceEnd = new ICalendarDateTime(copy.mInstanceEnd);
		mRecurring = copy.mRecurring;
	}

}