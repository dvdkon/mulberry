/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarMultiValue extends ICalendarValue {

	protected int mType;

	protected Vector mValues;

	/**
	 *  
	 */
	public ICalendarMultiValue(int type) {
		super();
		mType = type;
		mValues = new Vector();
	}

	public ICalendarMultiValue(ICalendarMultiValue copy) {
		super();
		mType = copy.mType;
		mValues = new Vector();
		for (Enumeration e = copy.mValues.elements(); e.hasMoreElements();) {
			ICalendarValue iter = (ICalendarValue) e.nextElement();
			mValues.addElement(iter.clone_it());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyrusoft.jcalendar.ICalendarValue#clone_it()
	 */
	public ICalendarValue clone_it() {
		return new ICalendarMultiValue(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyrusoft.jcalendar.ICalendarValue#getType()
	 */
	public int getType() {
		return mType;
	}

	/**
	 * @return Returns the values.
	 */
	public Vector getValues() {
		return mValues;
	}

	public void addValue(ICalendarValue value) {
		mValues.addElement(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyrusoft.jcalendar.ICalendarValue#parse(java.lang.String)
	 */
	public void parse(String data) {
		// Tokenize on comma
		StringTokenizer tokens = new StringTokenizer(data, ",");
		while (tokens.hasMoreTokens()) {
			// Find next token separator
			String token = tokens.nextToken();

			// Create single value, and parse data
			ICalendarValue value = ICalendarValue.createFromType(mType);
			value.parse(token);
			mValues.addElement(value);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyrusoft.jcalendar.ICalendarValue#generate(java.io.DataOutput)
	 */
	public void generate(BufferedWriter os) {
		try {
			boolean first = true;
			for (Enumeration e = mValues.elements(); e.hasMoreElements();) {
				ICalendarValue iter = (ICalendarValue) e.nextElement();
				if (first)
					first = false;
				else
					os.write(",");
				iter.generate(os);
			}
		} catch (IOException e1) {
		}
	}

}