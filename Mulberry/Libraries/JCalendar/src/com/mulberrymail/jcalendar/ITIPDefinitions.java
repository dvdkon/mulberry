/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

public class ITIPDefinitions {

//	 2446 �3

	static final String cICalMethod_PUBLISH = "PUBLISH";
	static final String cICalMethod_REQUEST = "REQUEST";
	static final String cICalMethod_REFRESH = "REFRESH";
	static final String cICalMethod_CANCEL = "CANCEL";
	static final String cICalMethod_ADD = "ADD";
	static final String cICalMethod_REPLY = "REPLY";
	static final String cICalMethod_COUNTER = "COUNTER";
	static final String cICalMethod_DECLINECOUNTER = "DECLINECOUNTER";

//	 2447 �2.4
	static final String cICalMIMEMethod_PUBLISH = "publish";
	static final String cICalMIMEMethod_REQUEST = "request";
	static final String cICalMIMEMethod_REFRESH = "refresh";
	static final String cICalMIMEMethod_CANCEL = "cancel";
	static final String cICalMIMEMethod_ADD = "add";
	static final String cICalMIMEMethod_REPLY = "reply";
	static final String cICalMIMEMethod_COUNTER = "counter";
	static final String cICalMIMEMethod_DECLINECOUNTER = "declinecounter";

	static final String cICalMIMEComponent_VEVENT = "vevent";
	static final String cICalMIMEComponent_VTODO = "vtodo";
	static final String cICalMIMEComponent_VJOURNAL = "vjournal";
	static final String cICalMIMEComponent_VFREEBUSY = "vfreebusy";

}
