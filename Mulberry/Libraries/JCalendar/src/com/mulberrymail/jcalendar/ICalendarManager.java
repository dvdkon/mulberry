/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarManager {

	public static ICalendarManager sICalendarManager = null;

	protected ICalendarTimezone mDefaultTimezone;

	public static void loadStatics()
	{
		ICalendar.loadStatics();
		ICalendarProperty.loadStatics();
	}

	public ICalendarManager() {
		mDefaultTimezone = new ICalendarTimezone();
		sICalendarManager = this;
	}

	public void initManager() {
		/**
		 * TODO - read in timezones from vtimezones.ics file
		 */
		// Eventually we need to read these from prefs - for now they are
		// hard-coded to my personal prefs!
		setDefaultTimezone(new ICalendarTimezone(false, "US/Eastern"));

	}

	public void setDefaultTimezoneID(String tzid) {
		// Check for UTC
		if (tzid.equals("UTC")) {
			ICalendarTimezone temp = new ICalendarTimezone(true);
			setDefaultTimezone(temp);
		} else {
			ICalendarTimezone temp = new ICalendarTimezone(false, tzid);
			setDefaultTimezone(temp);
		}
	}

	public void setDefaultTimezone(ICalendarTimezone tzid) {
		mDefaultTimezone = tzid;
	}

	public String getDefaultTimezoneID() {
		if (mDefaultTimezone.getUTC())
			return "UTC";
		else
			return mDefaultTimezone.getTimezoneID();
	}

	public ICalendarTimezone getDefaultTimezone() {
		return mDefaultTimezone;
	}

}