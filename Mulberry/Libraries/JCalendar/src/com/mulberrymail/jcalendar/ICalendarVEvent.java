/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Vector;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarVEvent extends ICalendarComponentRecur {

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINVEVENT;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDVEVENT;

	protected int mStatus;

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String getVEnd() {
		return sEndDelimiter;
	}

	public ICalendarVEvent(int calendar) {
		super(calendar);
		mStatus = ICalendarDefinitions.eStatus_VEvent_None;
	}

	public ICalendarVEvent(ICalendarVEvent copy) {
		super(copy);
		mStatus = copy.mStatus;
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVEvent(this);
	}

	public int getType() {
		return ICalendarComponent.eVEVENT;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

	public String getMimeComponentName() {
		return ITIPDefinitions.cICalMIMEComponent_VEVENT;
	}

	public boolean addComponent(ICalendarComponent comp) {
		// We can embed the alarm components only
		if (comp.getType() == ICalendarComponent.eVALARM) {
			if (mEmbedded == null)
				mEmbedded = new Vector();
			mEmbedded.addElement(comp);
			comp.setEmbedder(this);
			return true;
		} else
			return false;
	}

	public int getStatus() {
		return mStatus;
	}

	public void setStatus(int status) {
		mStatus = status;
	}

	public void finalise() {
		// Do inherited
		super.finalise();

		// Get STATUS
		{
			String temp = loadValueString(ICalendarDefinitions.cICalProperty_STATUS);
			if (temp != null) {
				if (temp
						.equals(ICalendarDefinitions.cICalProperty_STATUS_TENTATIVE)) {
					mStatus = ICalendarDefinitions.eStatus_VEvent_Tentative;
				} else if (temp
						.equals(ICalendarDefinitions.cICalProperty_STATUS_CONFIRMED)) {
					mStatus = ICalendarDefinitions.eStatus_VEvent_Confirmed;
				} else if (temp
						.equals(ICalendarDefinitions.cICalProperty_STATUS_CANCELLED)) {
					mStatus = ICalendarDefinitions.eStatus_VEvent_Cancelled;
				}
			}
		}
	}

	// Editing
	public void editStatus(int status) {
		// Only if it is different
		if (mStatus != status) {
			// Updated cached values
			mStatus = status;

			// Remove existing STATUS items
			removeProperties(ICalendarDefinitions.cICalProperty_STATUS);

			// Now create properties
			{
				String value = null;
				switch (status) {
				case ICalendarDefinitions.eStatus_VEvent_None:
				default:
					break;
				case ICalendarDefinitions.eStatus_VEvent_Tentative:
					value = ICalendarDefinitions.cICalProperty_STATUS_TENTATIVE;
					break;
				case ICalendarDefinitions.eStatus_VEvent_Confirmed:
					value = ICalendarDefinitions.cICalProperty_STATUS_CONFIRMED;
					break;
				case ICalendarDefinitions.eStatus_VEvent_Cancelled:
					value = ICalendarDefinitions.cICalProperty_STATUS_CANCELLED;
					break;
				}
				if (value != null) {
					ICalendarProperty prop = new ICalendarProperty(
							ICalendarDefinitions.cICalProperty_STATUS, value);
					addProperty(prop);
				}
			}
		}
	}
}