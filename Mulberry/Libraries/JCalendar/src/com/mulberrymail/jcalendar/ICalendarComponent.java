/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Vector;

import com.mulberrymail.utils.StringUtils;

/**
 * @author cyrusdaboo
 *  
 */
public abstract class ICalendarComponent extends ICalendarComponentBase {

	private static int uid_ctr = 1;

	public static final int eVCALENDAR = -1;

	public static final int eVEVENT = 0;

	public static final int eVTODO = 1;

	public static final int eVJOURNAL = 2;

	public static final int eVFREEBUSY = 3;

	public static final int eVTIMEZONE = 4;

	public static final int eVALARM = 5;

	// Pseudo components
	public static final int eVTIMEZONESTANDARD = 6;

	public static final int eVTIMEZONEDAYLIGHT = 7;

	protected int mCalendarRef;

	protected String mUID;

	protected int mSeq;

	protected int mOriginalSeq;

	protected ICalendarComponent mEmbedder;

	protected Vector mEmbedded;

	// CalDAV stuff
	protected String mRURL;

	protected String mETag;

	protected boolean mChanged;

	public ICalendarComponent(int calendar) {
		mCalendarRef = calendar;
		mUID = new String();
		mSeq = 0;
		mOriginalSeq = 0;
		mEmbedder = null;
		mEmbedded = null;
		mChanged = false;
	}

	public ICalendarComponent(ICalendarComponent copy) {
		super(copy);

		mCalendarRef = copy.mCalendarRef;
		mUID = copy.mUID;
		mSeq = copy.mSeq;
		mOriginalSeq = copy.mOriginalSeq;

		mEmbedder = null;
		mEmbedded = null;
		if (copy.mEmbedded != null) {
			// Do deep copy of element list
			mEmbedded = new Vector();
			for (Enumeration e = copy.mEmbedded.elements(); e.hasMoreElements();) {
				ICalendarComponent iter = (ICalendarComponent) e.nextElement();
				mEmbedded.addElement(iter.clone_it());
				((ICalendarComponent) mEmbedded.lastElement())
						.setEmbedder(this);
			}
		}

		mETag = copy.mETag;
		mRURL = copy.mRURL;
		mChanged = copy.mChanged;
	}

	public void close() {

		mEmbedder = null;

		// Also close sub-components if present
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();)
				((ICalendarComponent) e.nextElement()).close();
			mEmbedded.removeAllElements();
			mEmbedded = null;
		}
	}

	public abstract ICalendarComponent clone_it();

	public abstract int getType();

	public abstract String getBeginDelimiter();

	public abstract String getEndDelimiter();

	public abstract String getMimeComponentName();

	public boolean addComponent(ICalendarComponent comp) {
		// Sub-classes decide what can be embedded
		return false;
	}

	public void removeComponent(ICalendarComponent comp) {
		if (mEmbedded != null) {
			mEmbedded.removeElement(comp);
		}
	}

	public boolean hasEmbeddedComponent(int type) {
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();) {
				ICalendarComponent iter = (ICalendarComponent) e.nextElement();
				if (iter.getType() == type)
					return true;
			}
		}

		return false;
	}

	public ICalendarComponent getFirstEmbeddedComponent(int type) {
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();) {
				ICalendarComponent iter = (ICalendarComponent) e.nextElement();
				if (iter.getType() == type)
					return iter;
			}
		}

		return null;
	}

	public void setEmbedder(ICalendarComponent embedder) {
		mEmbedder = embedder;
	}

	public ICalendarComponent getEmbedder() {
		return mEmbedder;
	}

	public void setCalendar(int ref) {
		mCalendarRef = ref;
	}

	public int getCalendar() {
		return mCalendarRef;
	}

	public String getMapKey() {
		return mUID;
	}

	public String getMasterKey() {
		return mUID;
	}

	public String getUID() {
		return mUID;
	}

	public void setUID(String uid) {
		if (uid.length() != 0)
			mUID = uid;
		else {
			// Get left-side of UID (first 24 chars of MD5 digest of time, clock
			// and ctr)
			String lhs_txt = new String();
			lhs_txt = lhs_txt.concat(Long.toString(new Date().getTime()));
			lhs_txt = lhs_txt.concat(".");
			lhs_txt = lhs_txt.concat(Integer.toString(uid_ctr++));
			String lhs = StringUtils.md5(lhs_txt);

			// Get right side (domain) of message-id
			String rhs = null;
			{
				// Use app name
				String domain = new String("com.cyrusoft.mulberry");
				domain = domain.concat(Integer.toString(uid_ctr));

				// Use first 24 chars of MD5 digest of the domain as the
				// right-side of message-id
				rhs = StringUtils.md5(domain);
			}

			// Generate the UID string
			String new_uid = lhs;
			new_uid = new_uid.concat("@");
			new_uid = new_uid.concat(rhs);

			mUID = new_uid;
		}

		removeProperties(ICalendarDefinitions.cICalProperty_UID);

		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_UID, mUID);
		addProperty(prop);

	}

	public int getSeq() {
		return mSeq;
	}

	public void setSeq(int seq) {
		mSeq = seq;

		removeProperties(ICalendarDefinitions.cICalProperty_SEQUENCE);

		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_SEQUENCE, mSeq);
		addProperty(prop);
	}

	public int getOriginalSeq() {
		return mOriginalSeq;
	}

	public String getRURL() {
		return mRURL;
	}

	public void setRURL(String rurl) {
		mRURL = rurl;
	}

	public void generateRURL() {
		// Format is:
		//
		// <<hash code>> *("-0"> .ics
		if ((mRURL == null) || (mRURL.length() == 0)) {
			// Generate hash code
			String hash = new String();
			hash = hash.concat(getMapKey());
			hash = hash.concat(":");
			hash = hash.concat(Integer.toString(getSeq()));
			hash = hash.concat(":");

			ICalendarDateTime dt = new ICalendarDateTime();
			if (loadValue(ICalendarDefinitions.cICalProperty_DTSTAMP, dt)) {
				hash = hash.concat(dt.getText());
			}

			mRURL = StringUtils.md5(hash);
		} else {
			// Strip off .ics
			if (mRURL.endsWith(".ics"))
				mRURL = mRURL.substring(0, mRURL.length() - 4);
		}

		// Add trailer
		mRURL = mRURL.concat("-0.ics");
	}

	public String getETag() {
		return mETag;
	}

	public void setETag(String etag) {
		mETag = etag;
	}

	public boolean getChanged() {
		return mChanged;
	}

	public void setChanged(boolean changed) {
		mChanged = changed;
	}

	public void initDTSTAMP() {
		removeProperties(ICalendarDefinitions.cICalProperty_DTSTAMP);

		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_DTSTAMP, ICalendarDateTime
						.getNowUTC());
		addProperty(prop);
	}

	public void updateLastModified() {
		removeProperties(ICalendarDefinitions.cICalProperty_LAST_MODIFIED);

		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_LAST_MODIFIED,
				ICalendarDateTime.getNowUTC());
		addProperty(prop);
	}

	public void added() {
		// Also add sub-components if present
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();)
				((ICalendarComponent) e.nextElement()).added();
		}

		mChanged = true;
	}

	public void removed() {
		// Also remove sub-components if present
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();)
				((ICalendarComponent) e.nextElement()).removed();
		}

		mChanged = true;
	}

	public void duplicated() {
		// Remove SEQ, UID, DTSTAMP
		// These will be re-created when it is added to the calendar
		removeProperties(ICalendarDefinitions.cICalProperty_UID);
		removeProperties(ICalendarDefinitions.cICalProperty_SEQUENCE);
		removeProperties(ICalendarDefinitions.cICalProperty_DTSTAMP);

		// Remove the cached values as well
		mUID = new String();
		mSeq = 0;
		mOriginalSeq = 0;

		// Also duplicate sub-components if present
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();)
				((ICalendarComponent) e.nextElement()).duplicated();
		}

		// Reset CalDAV items
		mETag = null;
		mRURL = null;
		mChanged = true;
	}

	public void changed() {
		// Bump the sequence
		setSeq(getSeq() + 1);

		// Update last-modified
		updateLastModified();

		// Also change sub-components
		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();)
				((ICalendarComponent) e.nextElement()).changed();
		}

		mChanged = true;

		// Mark calendar as dirty
		ICalendar cal = ICalendar.getICalendar(getCalendar());
		if (cal != null)
			cal.changedComponent(this);
	}

	public void finalise() {
		// Get UID
		String temps = loadValueString(ICalendarDefinitions.cICalProperty_UID);
		if (temps != null)
			mUID = temps;

		// Get SEQ
		Integer temp = loadValueInteger(ICalendarDefinitions.cICalProperty_SEQUENCE);
		if (temp != null)
			mSeq = temp.intValue();

		// Cache the original sequence when the component is read in.
		// This will be used to synchronise changes between two instances of the
		// same calendar
		mOriginalSeq = mSeq;

		// Get CalDAV info if present
		temps = loadPrivateValue(ICalendarDefinitions.cICalProperty_X_PRIVATE_RURL);
		if (temps != null)
			mRURL = temps;
		temps = loadPrivateValue(ICalendarDefinitions.cICalProperty_X_PRIVATE_ETAG);
		if (temps != null)
			mETag = temps;
	}

	public boolean canGenerateInstance() {
		return true;
	}

	public void generate(BufferedWriter os, boolean for_cache) {
		try {
			// Header
			os.write(getBeginDelimiter());
			os.write("\n");

			// Write each property
			writeProperties(os);

			// Do private properties if caching
			if (for_cache) {
				if ((mRURL != null) && (mRURL.length() != 0))
					writePrivateProperty(os,
							ICalendarDefinitions.cICalProperty_X_PRIVATE_RURL,
							mRURL);
				if ((mETag != null) && (mETag.length() != 0))
					writePrivateProperty(os,
							ICalendarDefinitions.cICalProperty_X_PRIVATE_ETAG,
							mETag);
			}

			// Write each embedded component
			if (mEmbedded != null) {
				for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();)
					((ICalendarComponent) e.nextElement()).generate(os,
							for_cache);
			}

			// Footer
			os.write(getEndDelimiter());
			os.write("\n");
		} catch (IOException e1) {
		}
	}

	public void generateFiltered(BufferedWriter os, ICalendarOutputFilter filter) {
		try {
			// Header
			os.write(getBeginDelimiter());
			os.write("\n");

			// Write each property
			writePropertiesFiltered(os, filter);

			// Write each embedded component
			if (mEmbedded != null) {
				// Shortcut for alll sub-components
				if (filter.isAllSubComponents()) {
					for (Enumeration e = mEmbedded.elements(); e
							.hasMoreElements();)
						((ICalendarComponent) e.nextElement()).generate(os,
								false);
				} else if (filter.hasSubComponentFilters()) {
					for (Enumeration e = mEmbedded.elements(); e
							.hasMoreElements();) {

						ICalendarComponent subcomp = (ICalendarComponent) e
								.nextElement();
						ICalendarOutputFilter subfilter = filter
								.getSubComponentFilter(getType());
						if (subfilter != null)
							subcomp.generateFiltered(os, subfilter);
					}
				}
			}

			// Footer
			os.write(getEndDelimiter());
			os.write("\n");
		} catch (IOException e1) {
		}
	}

	public void getTimezones(HashSet tzids) {
		// Look for all date-time properties
		for (Enumeration iter = mProperties.iterator(); iter.hasMoreElements();) {
			// Try to get a date-time value from the property
			ICalendarDateTimeValue dtv = ((ICalendarProperty) iter
					.nextElement()).getDateTimeValue();
			if (dtv != null) {
				// Add timezone id if appropriate
				if (dtv.getValue().getTimezoneID() != null)
					tzids.add(dtv.getValue().getTimezoneID());
			}
		}
	}
}