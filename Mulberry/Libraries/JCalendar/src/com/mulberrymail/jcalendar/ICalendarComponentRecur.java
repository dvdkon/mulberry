/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Enumeration;
import java.util.Vector;

import com.mulberrymail.utils.MultiMap;
import com.mulberrymail.utils.SortFunctor;
import com.mulberrymail.utils.VectorUtils;

/**
 * @author cyrusdaboo
 * 
 */
public abstract class ICalendarComponentRecur extends ICalendarComponent {

	protected ICalendarComponentRecur mMaster; // Component to inherit

	// properties from if none exist
	// in this component

	protected String mMapKey;

	protected String mSummary;

	protected ICalendarDateTime mStamp;

	protected boolean mHasStamp;

	protected ICalendarDateTime mStart;

	protected boolean mHasStart;

	protected ICalendarDateTime mEnd;

	protected boolean mHasEnd;

	protected boolean mDuration;

	protected boolean mHasRecurrenceID;

	protected boolean mAdjustFuture;

	protected boolean mAdjustPrior;

	protected ICalendarDateTime mRecurrenceID;

	protected ICalendarRecurrenceSet mRecurrences;

	public static String mapKey(String uid) {
		return new String("u:").concat(uid);
	}

	public static String mapKey(String uid, String rid) {
		return new String("u:").concat(uid).concat(rid);
	}

	public static class sort_by_dtstart_allday extends SortFunctor {

		public boolean less_than(Object o1, Object o2) {
			ICalendarComponentRecur e1 = (ICalendarComponentRecur) o1;
			ICalendarComponentRecur e2 = (ICalendarComponentRecur) o2;

			if (e1.mStart.isDateOnly() && e2.mStart.isDateOnly())
				return e1.mStart.lt(e2.mStart);
			else if (e1.mStart.isDateOnly())
				return true;
			else if (e2.mStart.isDateOnly())
				return false;
			else if (e1.mStart.eq(e2.mStart)) {
				if (e1.mEnd.eq(e2.mEnd))
					// Put ones created earlier in earlier columns in day view
					return e1.mStamp.lt(e2.mStamp);
				else
					// Put ones that end later in earlier columns in day view
					return e1.mEnd.gt(e2.mEnd);
			} else
				return e1.mStart.lt(e2.mStart);
		}
	}

	public static class sort_by_dtstart extends SortFunctor {

		public boolean less_than(Object o1, Object o2) {
			ICalendarComponentRecur e1 = (ICalendarComponentRecur) o1;
			ICalendarComponentRecur e2 = (ICalendarComponentRecur) o2;

			if (e1.mStart.eq(e2.mStart)) {
				if (e1.mStart.isDateOnly() ^ e2.mStart.isDateOnly())
					return e1.mStart.isDateOnly();
				else
					return false;
			} else
				return e1.mStart.lt(e2.mStart);
		}

	}

	public ICalendarComponentRecur(int calendar) {
		super(calendar);
		mMaster = this;
		mMapKey = null;
		mSummary = null;
		mStamp = new ICalendarDateTime();
		mHasStamp = false;
		mStart = new ICalendarDateTime();
		mHasStart = false;
		mEnd = new ICalendarDateTime();
		mHasEnd = false;
		mDuration = false;
		mHasRecurrenceID = false;
		mAdjustFuture = false;
		mAdjustPrior = false;
		mRecurrenceID = null;
		mRecurrences = null;
	}

	public ICalendarComponentRecur(ICalendarComponentRecur copy) {
		super(copy);

		// Special determination of master
		if (copy.recurring())
			mMaster = copy.mMaster;
		else
			mMaster = this;

		mMapKey = copy.mMapKey;

		mSummary = copy.mSummary;

		if (copy.mStamp != null)
			mStamp = new ICalendarDateTime(copy.mStamp);
		mHasStamp = copy.mHasStamp;

		mStart = new ICalendarDateTime(copy.mStart);
		mHasStart = copy.mHasStart;
		mEnd = new ICalendarDateTime(copy.mEnd);
		mHasEnd = copy.mHasEnd;
		mDuration = copy.mDuration;

		mHasRecurrenceID = copy.mHasRecurrenceID;
		mAdjustFuture = copy.mAdjustFuture;
		mAdjustPrior = copy.mAdjustPrior;
		if (copy.mRecurrenceID != null)
			mRecurrenceID = new ICalendarDateTime(copy.mRecurrenceID);

		if (copy.mRecurrences != null)
			mRecurrences = new ICalendarRecurrenceSet(copy.mRecurrences);
	}

	public boolean canGenerateInstance() {
		return !mHasRecurrenceID;
	}

	public boolean recurring() {
		return (mMaster != null) && (mMaster != this);
	}

	public void setMaster(ICalendarComponentRecur master) {
		mMaster = master;
		initFromMaster();
	}

	public ICalendarComponentRecur getMaster() {
		return mMaster;
	}

	public String getMapKey() {
		return mMapKey;
	}

	public String getMasterKey() {
		return mapKey(mUID);
	}

	public void initDTSTAMP() {
		// Save new one
		super.initDTSTAMP();

		// Get the new one
		mHasStamp = loadValue(ICalendarDefinitions.cICalProperty_DTSTAMP,
				mStamp);
	}

	public ICalendarDateTime getStamp() {
		return mStamp;
	}

	public boolean hasStamp() {
		return mHasStamp;
	}

	public ICalendarDateTime getStart() {
		return mStart;
	}

	public boolean hasStart() {
		return mHasStart;
	}

	public ICalendarDateTime getEnd() {
		return mEnd;
	}

	public boolean hasEnd() {
		return mHasEnd;
	}

	public boolean useDuration() {
		return mDuration;
	}

	public boolean isRecurrenceInstance() {
		return mHasRecurrenceID;
	}

	public boolean isAdjustFuture() {
		return mAdjustFuture;
	}

	public boolean isAdjustPrior() {
		return mAdjustPrior;
	}

	public ICalendarDateTime getRecurrenceID() {
		return mRecurrenceID;
	}

	public boolean isRecurring() {
		return (mRecurrences != null) && mRecurrences.hasRecurrence();
	}

	public ICalendarRecurrenceSet getRecurrenceSet() {
		return mRecurrences;
	}

	public void setUID(String uid) {
		super.setUID(uid);

		// Update the map key
		if (mHasRecurrenceID) {
			mMapKey = mapKey(mUID, mRecurrenceID.getText());
		} else {
			mMapKey = mapKey(mUID);
		}
	}

	public String getSummary() {
		return mSummary;
	}

	public void setSummary(String summary) {
		mSummary = summary;
	}

	public String getDescription() {
		// Get DESCRIPTION
		String txt = loadValueString(ICalendarDefinitions.cICalProperty_DESCRIPTION);
		return txt != null ? txt : new String();
	}

	public String getLocation() {
		// Get LOCATION
		String txt = loadValueString(ICalendarDefinitions.cICalProperty_LOCATION);
		return txt != null ? txt : new String();
	}

	public void finalise() {
		super.finalise();

		// Get DTSTAMP
		mHasStamp = loadValue(ICalendarDefinitions.cICalProperty_DTSTAMP,
				mStamp);

		// Get DTSTART
		mHasStart = loadValue(ICalendarDefinitions.cICalProperty_DTSTART,
				mStart);

		// Get DTEND
		if (!loadValue(ICalendarDefinitions.cICalProperty_DTEND, mEnd)) {
			// Try DURATION instead
			ICalendarDuration temp = new ICalendarDuration();
			if (loadValue(ICalendarDefinitions.cICalProperty_DURATION, temp)) {
				mEnd = mStart.add(temp);
				mDuration = true;
			} else {
				// If no end or duration then use the start
				mEnd = new ICalendarDateTime(mStart);
				mDuration = false;
			}
		} else {
			mHasEnd = true;
			mDuration = false;
		}

		// Make sure start/end values are sensible
		FixStartEnd();

		// Get SUMMARY
		String temp = loadValueString(ICalendarDefinitions.cICalProperty_SUMMARY);
		if (temp != null)
			mSummary = temp;

		// Get RECURRENCE-ID
		mHasRecurrenceID = (getProperties().count(
				ICalendarDefinitions.cICalProperty_RECURRENCE_ID) != 0);
		if (mHasRecurrenceID) {
			if (mRecurrenceID == null)
				mRecurrenceID = new ICalendarDateTime();
			loadValue(ICalendarDefinitions.cICalProperty_RECURRENCE_ID,
					mRecurrenceID);
		}

		// Update the map key
		if (mHasRecurrenceID) {
			mMapKey = mapKey(mUID, mRecurrenceID.getText());

			// Also get the RANGE attribute
			MultiMap attrs = ((ICalendarProperty) getProperties().findFirst(
					ICalendarDefinitions.cICalProperty_RECURRENCE_ID))
					.getAttributes();
			Vector found = attrs
					.findItems(ICalendarDefinitions.cICalAttribute_RANGE);
			if ((found != null) && !found.isEmpty()) {
				mAdjustFuture = ((String) ((ICalendarAttribute) found
						.firstElement()).getFirstValue())
						.equals(ICalendarDefinitions.cICalAttribute_RANGE_THISANDFUTURE);
				mAdjustPrior = ((String) ((ICalendarAttribute) found
						.firstElement()).getFirstValue())
						.equals(ICalendarDefinitions.cICalAttribute_RANGE_THISANDPRIOR);
			} else {
				mAdjustFuture = false;
				mAdjustPrior = false;
			}
		} else
			mMapKey = mapKey(mUID);

		// May need to create items
		if ((getProperties().count(ICalendarDefinitions.cICalProperty_RRULE) != 0)
				|| (getProperties().count(
						ICalendarDefinitions.cICalProperty_RDATE) != 0)
				|| (getProperties().count(
						ICalendarDefinitions.cICalProperty_EXRULE) != 0)
				|| (getProperties().count(
						ICalendarDefinitions.cICalProperty_EXDATE) != 0)) {
			if (mRecurrences == null)
				mRecurrences = new ICalendarRecurrenceSet();

			// Get RRULEs
			loadValueRRULE(ICalendarDefinitions.cICalProperty_RRULE,
					mRecurrences, true);

			// Get RDATEs
			loadValueRDATE(ICalendarDefinitions.cICalProperty_RDATE,
					mRecurrences, true);

			// Get EXRULEs
			loadValueRRULE(ICalendarDefinitions.cICalProperty_EXRULE,
					mRecurrences, false);

			// Get EXDATEs
			loadValueRDATE(ICalendarDefinitions.cICalProperty_EXDATE,
					mRecurrences, false);
		}
	}

	private void FixStartEnd() {
		// End is always greater than start if start exists
		if (mHasStart && mEnd.le(mStart)) {
			// Use the start
			mEnd = new ICalendarDateTime(mStart);
			mDuration = false;

			// Adjust to approriate non-inclusive end point
			if (mStart.isDateOnly()) {
				mEnd.offsetDay(1);

				// For all day events it makes sense to use duration
				mDuration = true;
			} else {
				// Use end of current day
				mEnd.offsetDay(1);
				mEnd.setHHMMSS(0, 0, 0);
			}
		}
	}

	public void expandPeriod(ICalendarPeriod period, Vector list) {
		// Check for recurrence and true master
		if ((mRecurrences != null) && mRecurrences.hasRecurrence()
				&& !isRecurrenceInstance()) {
			// Expand recurrences within the range
			ICalendarDateTimeList items = new ICalendarDateTimeList();
			mRecurrences.expand(mStart, period, items);

			// Look for overridden recurrence items
			ICalendar cal = ICalendar.getICalendar(getCalendar());
			if (cal != null) {
				// Remove recurrence instances from the list of items
				ICalendarDateTimeList recurs = new ICalendarDateTimeList();
				cal.getRecurrenceInstances(ICalendarComponent.eVEVENT,
						getUID(), recurs);
				if (recurs.size() != 0) {
					ICalendarDateTimeList temp = new ICalendarDateTimeList();
					ICalendarDateTimeList.set_difference(items, recurs, temp);
					items = temp;

					// Now get actual instances
					Vector instances = new Vector();
					cal.getRecurrenceInstances(ICalendarComponent.eVEVENT,
							getUID(), instances);

					// Get list of each ones with RANGE
					Vector prior = new Vector();
					Vector future = new Vector();
					for (Enumeration e = instances.elements(); e
							.hasMoreElements();) {
						ICalendarComponentRecur iter = (ICalendarComponentRecur) e
								.nextElement();
						if (iter.isAdjustPrior())
							prior.addElement(iter);
						if (iter.isAdjustFuture())
							future.addElement(iter);
					}

					// Check for special behaviour
					if (prior.isEmpty() && future.isEmpty()) {
						// Add each expanded item
						for (Enumeration e = items.elements(); e
								.hasMoreElements();) {
							ICalendarDateTime iter = (ICalendarDateTime) e
									.nextElement();
							list.addElement(createExpanded(this, iter));
						}
					} else {
						// Sort each list first
						VectorUtils.sort(prior, new sort_by_dtstart());
						VectorUtils.sort(future, new sort_by_dtstart());

						// Add each expanded item
						for (Enumeration e = items.elements(); e
								.hasMoreElements();) {
							ICalendarDateTime iter1 = (ICalendarDateTime) e
									.nextElement();

							// Now step through each using the slave item
							// instead of the master as appropriate
							ICalendarComponentRecur slave = null;

							// Find most appropriate THISANDPRIOR item
							for (int i = prior.size() - 1; i >= 0; i--) {
								ICalendarComponentRecur riter2 = (ICalendarComponentRecur) prior
										.elementAt(i);
								if (riter2.getStart().gt(iter1)) {
									slave = riter2;
									break;
								}
							}

							// Find most appropriate THISANDFUTURE item
							for (int i = future.size() - 1; i >= 0; i--) {
								ICalendarComponentRecur riter2 = (ICalendarComponentRecur) prior
										.elementAt(i);
								if (riter2.getStart().lt(iter1)) {
									slave = riter2;
									break;
								}
							}

							list.addElement(createExpanded(
									slave != null ? slave : this, iter1));
						}
					}
				} else {
					// Add each expanded item
					for (Enumeration e = items.elements(); e.hasMoreElements();) {
						ICalendarDateTime iter = (ICalendarDateTime) e
								.nextElement();
						list.addElement(createExpanded(this, iter));
					}
				}
			}
		}

		else if (withinPeriod(period))
			list.addElement(new ICalendarComponentExpanded(this,
					isRecurrenceInstance() ? mRecurrenceID : null));
	}

	public boolean withinPeriod(ICalendarPeriod period) {
		// Check for recurrence
		if ((mRecurrences != null) && mRecurrences.hasRecurrence()) {
			ICalendarDateTimeList items = new ICalendarDateTimeList();
			mRecurrences.expand(mStart, period, items);
			return !items.isEmpty();
		} else {
			// Does event span the period (assume mEnd > mStart)
			// Check start (inclusive) and end (exclusive)
			if (mEnd.le(period.getStart()) || mStart.ge(period.getEnd()))
				return false;
			else
				return true;
		}
	}

	public void changedRecurrence() {
		// Clear cached values
		if (mRecurrences != null)
			mRecurrences.changed();
	}

	// Editing
	public void editSummary(String summary) {
		// Updated cached value
		mSummary = summary;

		// Remove existing items
		editProperty(ICalendarDefinitions.cICalProperty_SUMMARY, summary);
	}

	public void editDetails(String description, String location) {

		// Edit existing items
		editProperty(ICalendarDefinitions.cICalProperty_DESCRIPTION,
				description);
		editProperty(ICalendarDefinitions.cICalProperty_LOCATION, location);

	}

	public void editTiming() {
		// Updated cached values
		mHasStart = false;
		mHasEnd = false;
		mDuration = false;
		mStart.setToday();
		mEnd.setToday();

		// Remove existing DTSTART & DTEND & DURATION & DUE items
		removeProperties(ICalendarDefinitions.cICalProperty_DTSTART);
		removeProperties(ICalendarDefinitions.cICalProperty_DTEND);
		removeProperties(ICalendarDefinitions.cICalProperty_DURATION);
		removeProperties(ICalendarDefinitions.cICalProperty_DUE);
	}

	public void editTiming(ICalendarDateTime due) {
		// Updated cached values
		mHasStart = false;
		mHasEnd = true;
		mDuration = false;
		mStart = due;
		mEnd = due;

		// Remove existing DUE & DTSTART & DTEND & DURATION items
		removeProperties(ICalendarDefinitions.cICalProperty_DUE);
		removeProperties(ICalendarDefinitions.cICalProperty_DTSTART);
		removeProperties(ICalendarDefinitions.cICalProperty_DTEND);
		removeProperties(ICalendarDefinitions.cICalProperty_DURATION);

		// Now create properties
		{
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DUE, due);
			addProperty(prop);
		}
	}

	public void editTiming(ICalendarDateTime start, ICalendarDateTime end) {
		// Updated cached values
		mHasStart = mHasEnd = true;
		mStart = start;
		mEnd = end;
		mDuration = false;
		FixStartEnd();

		// Remove existing DTSTART & DTEND & DURATION & DUE items
		removeProperties(ICalendarDefinitions.cICalProperty_DTSTART);
		removeProperties(ICalendarDefinitions.cICalProperty_DTEND);
		removeProperties(ICalendarDefinitions.cICalProperty_DURATION);
		removeProperties(ICalendarDefinitions.cICalProperty_DUE);

		// Now create properties
		{
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DTSTART, start);
			addProperty(prop);
		}

		// If its an all day event and the end one day after the start, ignore
		// it
		ICalendarDateTime temp = new ICalendarDateTime(start);
		temp.offsetDay(1);
		if (!start.isDateOnly() || end.ne(temp)) {
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DTEND, end);
			addProperty(prop);
		}
	}

	public void editTiming(ICalendarDateTime start, ICalendarDuration duration) {
		// Updated cached values
		mHasStart = true;
		mHasEnd = false;
		mStart = start;
		mEnd = start.add(duration);
		mDuration = true;

		// Remove existing DTSTART & DTEND & DURATION & DUE items
		removeProperties(ICalendarDefinitions.cICalProperty_DTSTART);
		removeProperties(ICalendarDefinitions.cICalProperty_DTEND);
		removeProperties(ICalendarDefinitions.cICalProperty_DURATION);
		removeProperties(ICalendarDefinitions.cICalProperty_DUE);

		// Now create properties
		{
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DTSTART, start);
			addProperty(prop);
		}

		// If its an all day event and the duration is one day, ignore it
		if (!start.isDateOnly() || (duration.getWeeks() != 0)
				|| (duration.getDays() > 1)) {
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DURATION, duration);
			addProperty(prop);
		}
	}

	public void editRecurrenceSet(ICalendarRecurrenceSet recurs) {
		// Must have items
		if (mRecurrences == null)
			mRecurrences = new ICalendarRecurrenceSet();

		// Updated cached values
		mRecurrences = recurs;

		// Remove existing RRULE, EXRULE, RDATE & EXDATE
		removeProperties(ICalendarDefinitions.cICalProperty_RRULE);
		removeProperties(ICalendarDefinitions.cICalProperty_EXRULE);
		removeProperties(ICalendarDefinitions.cICalProperty_RDATE);
		removeProperties(ICalendarDefinitions.cICalProperty_EXDATE);

		// Now create properties
		for (Enumeration e = mRecurrences.getRules().elements(); e
				.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_RRULE, iter);
			addProperty(prop);
		}
		for (Enumeration e = mRecurrences.getExrules().elements(); e
				.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_EXRULE, iter);
			addProperty(prop);
		}
		for (Enumeration e = mRecurrences.getDates().elements(); e
				.hasMoreElements();) {
			ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_RDATE, iter);
			addProperty(prop);
		}
		for (Enumeration e = mRecurrences.getExdates().elements(); e
				.hasMoreElements();) {
			ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_EXDATE, iter);
			addProperty(prop);
		}
	}

	public void excludeRecurrence(ICalendarDateTime start) {
		// Must have items
		if (mRecurrences == null)
			return;

		// Add to recurrence set and clear cache
		mRecurrences.subtract(start);

		// Add property
		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_EXDATE, start);
		addProperty(prop);
	}

	public void excludeFutureRecurrence(ICalendarDateTime start) {
		// Must have items
		if (mRecurrences == null)
			return;

		// Adjust RRULES to end before start
		mRecurrences.excludeFutureRecurrence(start);

		// Remove existing RRULE & RDATE
		removeProperties(ICalendarDefinitions.cICalProperty_RRULE);
		removeProperties(ICalendarDefinitions.cICalProperty_RDATE);

		// Now create properties
		for (Enumeration e = mRecurrences.getRules().elements(); e
				.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_RRULE, iter);
			addProperty(prop);
		}
		for (Enumeration e = mRecurrences.getDates().elements(); e
				.hasMoreElements();) {
			ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();
			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_RDATE, iter);
			addProperty(prop);
		}
	}

	// These are overridden to allow missing properties to come from the master
	// component
	protected Integer loadValueInteger(String value_name) {
		return loadValueInteger(value_name, ICalendarValue.eValueType_Integer);
	}

	protected Integer loadValue(String value_name, int type) {
		// Try to load from this component
		Integer result = super.loadValueInteger(value_name, type);

		// Try to load from master if we didn't get it from this component
		if ((result == null) && (mMaster != null) && (mMaster != this))
			result = mMaster.loadValueInteger(value_name, type);

		return result;
	}

	protected String loadValueString(String value_name) {
		// Try to load from this component
		String result = super.loadValueString(value_name);

		// Try to load from master if we didn't get it from this component
		if ((result == null) && (mMaster != null) && (mMaster != this))
			result = mMaster.loadValueString(value_name);

		return result;
	}

	protected boolean loadValue(String value_name, ICalendarDateTime value) {
		// Try to load from this component
		boolean result = super.loadValue(value_name, value);

		// Try to load from master if we didn't get it from this component
		if (!result && (mMaster != null) && (mMaster != this))
			result = mMaster.loadValue(value_name, value);

		return result;
	}

	protected boolean loadValue(String value_name, ICalendarDuration value) {
		// Try to load from this component
		boolean result = super.loadValue(value_name, value);

		// Try to load from master if we didn't get it from this component
		if (!result && (mMaster != null) && (mMaster != this))

			result = mMaster.loadValue(value_name, value);
		return result;
	}

	protected boolean loadValue(String value_name, ICalendarPeriod value) {
		// Try to load from this component
		boolean result = super.loadValue(value_name, value);

		// Try to load from master if we didn't get it from this component
		if (!result && (mMaster != null) && (mMaster != this))
			result = mMaster.loadValue(value_name, value);

		return result;
	}

	protected boolean loadValueRRULE(String value_name,
			ICalendarRecurrenceSet value, boolean add) {
		// Try to load from this component
		boolean result = super.loadValueRRULE(value_name, value, add);

		// Try to load from master if we didn't get it from this component
		if (!result && (mMaster != null) && (mMaster != this))
			result = mMaster.loadValueRRULE(value_name, value, add);

		return result;
	}

	protected boolean loadValueRDATE(String value_name,
			ICalendarRecurrenceSet value, boolean add) {
		// Try to load from this component
		boolean result = super.loadValueRDATE(value_name, value, add);

		// Try to load from master if we didn't get it from this component
		if (!result && (mMaster != null) && (mMaster != this))
			result = mMaster.loadValueRDATE(value_name, value, add);

		return result;
	}

	protected void initFromMaster() {
		// Only if not master
		if (recurring()) {
			// Redo this to get cached values from master
			finalise();

			// If this component does not have its own start property, use the
			// recurrence id
			// i.e. the start time of this instance has not changed - something
			// else has
			if (getProperties().count(
					ICalendarDefinitions.cICalProperty_DTSTART) == 0)
				mStart = mRecurrenceID;

			// If this component does not have its own end/duration property,
			// the determine
			// the end from the master duration
			if ((getProperties()
					.count(ICalendarDefinitions.cICalProperty_DTEND) == 0)
					&& (getProperties().count(
							ICalendarDefinitions.cICalProperty_DURATION) == 0)) {
				// End is based on original events settings
				mEnd = mStart
						.add(mMaster.getEnd().subtract(mMaster.getStart()));
			}

			// If this instance has a duration, but no start of its own, then we
			// need to readjust the end
			// to account for the start being changed to the recurrence id
			else if ((getProperties().count(
					ICalendarDefinitions.cICalProperty_DURATION) != 0)
					&& (getProperties().count(
							ICalendarDefinitions.cICalProperty_DTSTART) == 0)) {
				ICalendarDuration temp = new ICalendarDuration();
				loadValue(ICalendarDefinitions.cICalProperty_DURATION, temp);
				mEnd = mStart.add(temp);
			}
		}
	}

	protected ICalendarComponentExpanded createExpanded(
			ICalendarComponentRecur master, ICalendarDateTime recurid) {
		return new ICalendarComponentExpanded(master, recurid);
	}

}