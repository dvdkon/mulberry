/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

import com.mulberrymail.utils.VectorUtils;

/**
 * @author cyrusdaboo
 */

public class ICalendarVTimezone extends ICalendarComponent {

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINVTIMEZONE;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDVTIMEZONE;

	protected String mID;

	protected int mSortKey;

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String getVEnd() {
		return sEndDelimiter;
	}

	public ICalendarVTimezone(int calendar) {
		super(calendar);
		mID = new String();
		mSortKey = 1;
	}

	public ICalendarVTimezone(ICalendarVTimezone copy) {
		super(copy);
		mID = copy.mID;
		mSortKey = copy.mSortKey;
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVTimezone(this);
	}

	public int getType() {
		return ICalendarComponent.eVTIMEZONE;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

	public String getMimeComponentName() {
		// Cannot be sent as a separate MIME object
		return null;
	}

	public boolean addComponent(ICalendarComponent comp) {
		// We can embed the timezone components only
		if ((comp.getType() == ICalendarComponent.eVTIMEZONESTANDARD)
				|| (comp.getType() == ICalendarComponent.eVTIMEZONEDAYLIGHT)) {
			if (mEmbedded == null)
				mEmbedded = new Vector();
			mEmbedded.addElement(comp);
			comp.setEmbedder(this);
			return true;
		} else
			return false;
	}

	public String getMapKey() {
		return mID;
	}

	public void finalise() {
		// Get TZID
		String temp = loadValueString(ICalendarDefinitions.cICalProperty_TZID);
		if (temp != null)
			mID = temp;

		// Sort sub-components by DTSTART
		if (mEmbedded != null)
			VectorUtils.sort(mEmbedded,
					new ICalendarVTimezoneElement.sort_dtstart());

		// Do inherited
		super.finalise();
	}

	public String getID() {
		return mID;
	}

	public int getSortKey() {
		if (mSortKey == 1) {
			// Take time from first element
			if ((mEmbedded != null) && (mEmbedded.size() > 0)) {
				// Initial offset provides the primary key
				int utc_offset1 = ((ICalendarVTimezoneElement) mEmbedded
						.elementAt(0)).getUTCOffset();

				// Presence of secondary is the next key
				int utc_offset2 = utc_offset1;
				if (mEmbedded.size() > 1) {
					utc_offset2 = ((ICalendarVTimezoneElement) mEmbedded
							.elementAt(1)).getUTCOffset();
				}

				// Create key
				mSortKey = (utc_offset1 + utc_offset2) / 2;
			} else
				mSortKey = 0;
		}

		return mSortKey;
	}

	public int getTimezoneOffsetSeconds(ICalendarDateTime dt) {
		// Get the closet matching element to the time
		ICalendarVTimezoneElement found = findTimezoneElement(dt);

		// Return it
		if (found == null)
			return 0;
		else {
			// Get its offset
			return found.getUTCOffset();
		}
	}

	public String getTimezoneDescriptor(ICalendarDateTime dt) {
		String result = new String();

		// Get the closet matching element to the time
		ICalendarVTimezoneElement found = findTimezoneElement(dt);

		// Get it
		if (found != null) {
			if (found.getTZName().length() == 0) {
				int tzoffset = found.getUTCOffset();
				boolean negative = false;
				if (tzoffset < 0) {
					tzoffset = -tzoffset;
					negative = true;
				}
				result = negative ? "-" : "+";
				int hours_offset = tzoffset / (60 * 60);
				if (hours_offset < 10)
					result += "0";
				result += Integer.toString(hours_offset);
				int mins_offset = (tzoffset / 60) % 60;
				if (mins_offset < 10)
					result += "0";
				result += Integer.toString(mins_offset);
			} else {
				result = "(";
				result += found.getTZName();
				result += ")";
			}
		}

		return result;
	}

	public void mergeTimezone(ICalendarVTimezone tz) {
	}

	protected ICalendarVTimezoneElement findTimezoneElement(ICalendarDateTime dt) {
		// Need to make the incoming date-time relative to the DTSTART in the
		// timezone component for proper comparison.
		// This means making the incoming date-time a floating (no timezone)
		// item
		ICalendarDateTime temp = new ICalendarDateTime(dt);
		temp.setTimezoneID(null);

		// New scheme to avoid unneccessary timezone recurrence expansion:

		// Look for the standard and daylight components with a start time just
		// below the requested date-time
		ICalendarVTimezoneElement found_std = null;
		ICalendarVTimezoneElement found_day = null;

		if (mEmbedded != null) {
			for (Enumeration e = mEmbedded.elements(); e.hasMoreElements();) {
				ICalendarComponent iter = (ICalendarComponent) e.nextElement();
				ICalendarVTimezoneElement item = (ICalendarVTimezoneElement) iter;
				if (item.getStart().lt(temp)) {
					if (item.getType() == ICalendarComponent.eVTIMEZONESTANDARD)
						found_std = item;
					else
						found_day = item;
				}
			}
		}

		// Now do the expansion for each one found and pick the lowest
		ICalendarVTimezoneElement found = null;
		ICalendarDateTime dt_found = new ICalendarDateTime();

		if (found_std != null) {
			ICalendarDateTime dt_item = found_std.expandBelow(temp);
			if (temp.ge(dt_item)) {
				found = found_std;
				dt_found = dt_item;
			}
		}

		if (found_day != null) {
			ICalendarDateTime dt_item = found_day.expandBelow(temp);
			if (temp.ge(dt_item)) {
				if (found != null) {
					// Compare with the one previously cached and switch to this
					// one if newer
					if (dt_item.gt(dt_found)) {
						found = found_day;
						dt_found = dt_item;
					}
				} else {
					found = found_day;
					dt_found = dt_item;
				}
			}
		}

		return found;
	}

	public static class SortComparator implements Comparator {

		public int compare(Object arg0, Object arg1) {
			ICalendarVTimezone tz1 = (ICalendarVTimezone) arg0;
			ICalendarVTimezone tz2 = (ICalendarVTimezone) arg1;
			int sort1 = tz1.getSortKey();
			int sort2 = tz2.getSortKey();
			if (sort1 == sort2)
				return tz1.getID().compareToIgnoreCase(tz2.getID());
			else
				return (sort1 < sort2) ? -1 : 1;
		}
	}
}