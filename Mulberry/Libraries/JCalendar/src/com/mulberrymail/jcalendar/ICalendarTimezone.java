/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import com.mulberrymail.utils.StringUtils;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarTimezone {

	protected boolean mUTC;

	protected String mTimezone;

	/**
	 *  
	 */
public ICalendarTimezone() {
		super();

		mUTC = true;
		mTimezone = null;

		// Copy defauilt timezone if it exists
		if (ICalendarManager.sICalendarManager != null)
		{
			_copy_ICalendarTimezone(ICalendarManager.sICalendarManager.getDefaultTimezone());
		}
	}

	public ICalendarTimezone(boolean utc) {
		mUTC = utc;
		mTimezone = null;
	}

	public ICalendarTimezone(boolean utc, String tzid) {
		mUTC = utc;
		mTimezone = tzid;
	}

	public ICalendarTimezone(ICalendarTimezone copy) {
		_copy_ICalendarTimezone(copy);
	}

	public boolean equals(ICalendarTimezone comp) {
		// Always match if any one of them is 'floating'
		if (floating() || comp.floating())
			return true;
		else if (mUTC ^ comp.mUTC)
			return false;
		else
			return mUTC || StringUtils.compareStringsSafe(mTimezone, comp.mTimezone);
	}

	static public boolean same(boolean utc1, String tzid1, boolean utc2, String tzid2)
	{
		// Always match if any one of them is 'floating'
		if (is_float(utc1, tzid1) || is_float(utc2, tzid2))
			return true;
		else if (utc1 ^ utc2)
			return false;
		else
			return utc1 || StringUtils.compareStringsSafe(tzid1, tzid2);
	}

	static private boolean is_float(boolean utc, String tzid)
	{
		return !utc && ((tzid == null) || (tzid.length() == 0));
	}

	public boolean getUTC() {
		return mUTC;
	}

	public void setUTC(boolean utc) {
		mUTC = utc;
	}

	public String getTimezoneID() {
		return mTimezone;
	}

	public void setTimezoneID(String tzid) {
		mTimezone = tzid;
	}

	public boolean floating() {
		return !mUTC && ((mTimezone == null) || (mTimezone.length() == 0));
	}

	public boolean hasTZID() {
		return !mUTC && (mTimezone != null) && (mTimezone.length() != 0);
	}

	public long timeZoneSecondsOffset(ICalendarDateTime dt) {
		if (mUTC)
			return 0;
		else if ((mTimezone == null) || (mTimezone.length() == 0))
			return ICalendar.sICalendar.getTimezoneOffsetSeconds(
					ICalendarManager.sICalendarManager.getDefaultTimezone()
							.getTimezoneID(), dt);

		// Look up timezone and resolve date using default timezones
		return ICalendar.sICalendar.getTimezoneOffsetSeconds(mTimezone, dt);
	}

	public String timeZoneDescriptor(ICalendarDateTime dt) {
		if (mUTC)
			return "(UTC)";
		else if ((mTimezone == null) || (mTimezone.length() == 0))
			return ICalendar.sICalendar.getTimezoneDescriptor(
					ICalendarManager.sICalendarManager.getDefaultTimezone()
							.getTimezoneID(), dt);

		// Look up timezone and resolve date using default timezones
		return ICalendar.sICalendar.getTimezoneDescriptor(mTimezone, dt);
	}

	private void _copy_ICalendarTimezone(ICalendarTimezone copy) {
		mUTC = copy.mUTC;
		mTimezone = copy.mTimezone;
	}
}