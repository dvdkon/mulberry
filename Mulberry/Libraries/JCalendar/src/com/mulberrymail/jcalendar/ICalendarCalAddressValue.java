/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarCalAddressValue extends ICalendarPlainTextValue {

	/**
	 * 
	 */
	public ICalendarCalAddressValue() {
		super();
	}

	/**
	 * @param copy
	 */
	public ICalendarCalAddressValue(ICalendarPlainTextValue copy) {
		super(copy);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#clone_it()
	 */
	public ICalendarValue clone_it() {
		return new ICalendarCalAddressValue(this);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#getType()
	 */
	public int getType() {
		return eValueType_CalAddress;
	}

}
