/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarDateTimeValue extends ICalendarValue {

	protected ICalendarDateTime mValue;
	/**
	 * 
	 */
	public ICalendarDateTimeValue() {
		super();
		mValue = new ICalendarDateTime();
	}

	public ICalendarDateTimeValue(ICalendarDateTime value) {
		super();
		mValue = value;
	}

	public ICalendarDateTimeValue(ICalendarDateTimeValue copy) {
		super();
		mValue = new ICalendarDateTime(copy.mValue);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#clone_it()
	 */
	public ICalendarValue clone_it() {
		return new ICalendarDateTimeValue(this);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#getType()
	 */
	public int getType() {
		return mValue.isDateOnly() ? eValueType_Date : eValueType_DateTime;
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#parse(java.lang.String)
	 */
	public void parse(String data) {
		mValue.parse(data);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#generate(java.io.DataOutput)
	 */
	public void generate(BufferedWriter os) {
		mValue.generate(os);

	}

	/**
	 * @return Returns the value.
	 */
	public ICalendarDateTime getValue() {
		return mValue;
	}
	/**
	 * @param value The value to set.
	 */
	public void setValue(ICalendarDateTime value) {
		mValue = value;
	}
}
