/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarDefinitions {

//	 2445 Component Header/Footer

	static final String cICalComponent_BEGINVCALENDAR = "BEGIN:VCALENDAR";
	static final String cICalComponent_ENDVCALENDAR = "END:VCALENDAR";
	static final String cICalComponent_BEGINVEVENT = "BEGIN:VEVENT";
	static final String cICalComponent_ENDVEVENT = "END:VEVENT";
	static final String cICalComponent_BEGINVTODO = "BEGIN:VTODO";
	static final String cICalComponent_ENDVTODO = "END:VTODO";
	static final String cICalComponent_BEGINVJOURNAL = "BEGIN:VJOURNAL";
	static final String cICalComponent_ENDVJOURNAL = "END:VJOURNAL";
	static final String cICalComponent_BEGINVFREEBUSY = "BEGIN:VFREEBUSY";
	static final String cICalComponent_ENDVFREEBUSY = "END:VFREEBUSY";
	static final String cICalComponent_BEGINVTIMEZONE = "BEGIN:VTIMEZONE";
	static final String cICalComponent_ENDVTIMEZONE = "END:VTIMEZONE";
	static final String cICalComponent_BEGINVALARM = "BEGIN:VALARM";
	static final String cICalComponent_ENDVALARM = "END:VALARM";

//	 Pseudo components
	static final String cICalComponent_BEGINSTANDARD = "BEGIN:STANDARD";
	static final String cICalComponent_ENDSTANDARD = "END:STANDARD";
	static final String cICalComponent_BEGINDAYLIGHT = "BEGIN:DAYLIGHT";
	static final String cICalComponent_ENDDAYLIGHT = "END:DAYLIGHT";

//	 2445 Calendar Property Atrributes

//	 2445 �4.2
	static final String cICalAttribute_ALTREP = "ALTREP";
	static final String cICalAttribute_CN = "CN";
	static final String cICalAttribute_CUTYPE = "CUTYPE";
	static final String cICalAttribute_DELEGATED_FROM = "DELEGATED-FROM";
	static final String cICalAttribute_DELEGATED_TO = "DELEGATED-TO";
	static final String cICalAttribute_DIR = "DIR";
	static final String cICalAttribute_ENCODING = "ENCODING";
	static final String cICalAttribute_FMTTYPE = "FMTTYPE";
	static final String cICalAttribute_FBTYPE = "FBTYPE";
	static final String cICalAttribute_LANGUAGE = "LANGUAGE";
	static final String cICalAttribute_MEMBER = "MEMBER";
	static final String cICalAttribute_PARTSTAT = "PARTSTAT";
	static final String cICalAttribute_RANGE = "RANGE";
	static final String cICalAttribute_RELATED = "RELATED";
	static final String cICalAttribute_RELTYPE = "RELTYPE";
	static final String cICalAttribute_ROLE = "ROLE";
	static final String cICalAttribute_RSVP = "RSVP";
	static final String cICalAttribute_RSVP_TRUE = "TRUE";
	static final String cICalAttribute_RSVP_FALSE = "FALSE";
	static final String cICalAttribute_SENT_BY = "SENT-BY";
	static final String cICalAttribute_TZID = "TZID";
	static final String cICalAttribute_VALUE = "VALUE";

//	 2445 �4.2.9
	static final String cICalAttribute_FBTYPE_FREE = "FREE";
	static final String cICalAttribute_FBTYPE_BUSY = "BUSY";
	static final String cICalAttribute_FBTYPE_BUSYUNAVAILABLE = "BUSY-UNAVAILABLE";
	static final String cICalAttribute_FBTYPE_BUSYTENTATIVE = "BUSY-TENTATIVE";

//	 2445 �4.2.12
	static final int ePartStat_NeedsAction = 0;
	static final int ePartStat_Accepted = 1;
	static final int ePartStat_Declined = 2;
	static final int ePartStat_Tentative = 3;
	static final int ePartStat_Delegated = 4;
	static final int ePartStat_Completed = 5;
	static final int ePartStat_InProcess = 6;

	static final String cICalAttribute_PARTSTAT_NEEDSACTION = "NEEDS-ACTION";
	static final String cICalAttribute_PARTSTAT_ACCEPTED = "ACCEPTED";
	static final String cICalAttribute_PARTSTAT_DECLINED = "DECLINED";
	static final String cICalAttribute_PARTSTAT_TENTATIVE = "TENTATIVE";
	static final String cICalAttribute_PARTSTAT_DELEGATED = "DELEGATED";
	static final String cICalAttribute_PARTSTAT_COMPLETED = "COMPLETE";
	static final String cICalAttribute_PARTSTAT_INPROCESS = "IN-PROCESS";

//	 2445 �4.2.13
	static final String cICalAttribute_RANGE_THISANDFUTURE = "THISANDFUTURE";
	static final String cICalAttribute_RANGE_THISANDPRIOR = "THISANDPRIOR";

//	 2445 �4.2.14
	static final String cICalAttribute_RELATED_START = "START";
	static final String cICalAttribute_RELATED_END = "END";

//	 2445 �4.2.16
	static final int ePartRole_Chair = 0;
	static final int ePartRole_Required = 1;
	static final int ePartRole_Optional = 2;
	static final int ePartRole_Non = 3;

	static final String cICalAttribute_ROLE_CHAIR = "CHAIR";
	static final String cICalAttribute_ROLE_REQ_PART = "REQ-PARTICIPANT";
	static final String cICalAttribute_ROLE_OPT_PART = "OPT-PARTICIPANT";
	static final String cICalAttribute_ROLE_NON_PART = "NON-PARTICIPANT";

//	 2445 Value types

//	 2445 �4.3
	static final String cICalValue_BINARY = "BINARY";
	static final String cICalValue_BOOLEAN = "BOOLEAN";
	static final String cICalValue_CAL_ADDRESS = "CAL-ADDRESS";
	static final String cICalValue_DATE = "DATE";
	static final String cICalValue_DATE_TIME = "DATE-TIME";
	static final String cICalValue_DURATION = "DURATION";
	static final String cICalValue_FLOAT = "FLOAT";
	static final String cICalValue_INTEGER = "INTEGER";
	static final String cICalValue_PERIOD = "PERIOD";
	static final String cICalValue_RECUR = "RECUR";
	static final String cICalValue_TEXT = "TEXT";
	static final String cICalValue_TIME = "TIME";
	static final String cICalValue_URI = "URI";
	static final String cICalValue_UTC_OFFSET = "UTC-OFFSET";

//	 2445 Calendar Properties

//	 2445 � 4.7

	static final String cICalProperty_CALSCALE = "CALSCALE";
	static final String cICalProperty_METHOD = "METHOD";
	static final String cICalProperty_PRODID = "PRODID";
	static final String cICalProperty_VERSION = "VERSION";

//	 Apple Extensions
	static final String cICalProperty_XWRCALNAME = "X-WR-CALNAME";
	static final String cICalProperty_XWRCALDESC = "X-WR-CALDESC";

//	 2445 Componenty Property names

//	 2445 �4.8.1
	static final String cICalProperty_ATTACH = "ATTACH";
	static final String cICalProperty_CATEGORIES = "CATEGORIES";
	static final String cICalProperty_CLASS = "CLASS";
	static final String cICalProperty_COMMENT = "COMMENT";
	static final String cICalProperty_DESCRIPTION = "DESCRIPTION";
	static final String cICalProperty_GEO = "GEO";
	static final String cICalProperty_LOCATION = "LOCATION";
	static final String cICalProperty_PERCENT_COMPLETE = "PERCENT-COMPLETE";
	static final String cICalProperty_PRIORITY = "PRIORITY";
	static final String cICalProperty_RESOURCES = "RESOURCES";
	static final String cICalProperty_STATUS = "STATUS";
	static final String cICalProperty_SUMMARY = "SUMMARY";

//	 2445 �4.8.2
	static final String cICalProperty_COMPLETED = "COMPLETED";
	static final String cICalProperty_DTEND = "DTEND";
	static final String cICalProperty_DUE = "DUE";
	static final String cICalProperty_DTSTART = "DTSTART";
	static final String cICalProperty_DURATION = "DURATION";
	static final String cICalProperty_FREEBUSY = "FREEBUSY";
	static final String cICalProperty_TRANSP = "TRANSP";
	static final String cICalProperty_OPAQUE = "OPAQUE";
	static final String cICalProperty_TRANSPARENT = "TRANSPARENT";

//	 2445 �4.8.3
	static final String cICalProperty_TZID = "TZID";
	static final String cICalProperty_TZNAME = "TZNAME";
	static final String cICalProperty_TZOFFSETFROM = "TZOFFSETFROM";
	static final String cICalProperty_TZOFFSETTO = "TZOFFSETTO";
	static final String cICalProperty_TZURL = "TZURL";

//	 2445 �4.8.4
	static final String cICalProperty_ATTENDEE = "ATTENDEE";
	static final String cICalProperty_CONTACT = "CONTACT";
	static final String cICalProperty_ORGANIZER = "ORGANIZER";
	static final String cICalProperty_RECURRENCE_ID = "RECURRENCE-ID";
	static final String cICalProperty_RELATED_TO = "RELATED-TO";
	static final String cICalProperty_URL = "URL";
	static final String cICalProperty_UID = "UID";

//	 2445 �4.8.5
	static final String cICalProperty_EXDATE = "EXDATE";
	static final String cICalProperty_EXRULE = "EXRULE";
	static final String cICalProperty_RDATE = "RDATE";
	static final String cICalProperty_RRULE = "RRULE";

//	 2445 �4.8.6
	static final String cICalProperty_ACTION = "ACTION";
	static final String cICalProperty_REPEAT = "REPEAT";
	static final String cICalProperty_TRIGGER = "TRIGGER";

//	 2445 �4.8.7
	static final String cICalProperty_CREATED = "CREATED";
	static final String cICalProperty_DTSTAMP = "DTSTAMP";
	static final String cICalProperty_LAST_MODIFIED = "LAST-MODIFIED";
	static final String cICalProperty_SEQUENCE = "SEQUENCE";

//	 2445 �4.8.8.2
	static final String cICalProperty_REQUEST_STATUS = "REQUEST-STATUS";

//	 Enums
//	 Use ascending order for sensible sorting

//	 2445 �4.3.10

	static final int eRecurrence_SECONDLY = 0;
	static final int eRecurrence_MINUTELY = 1;
	static final int eRecurrence_HOURLY = 2;
	static final int eRecurrence_DAILY = 3;
	static final int eRecurrence_WEEKLY = 4;
	static final int eRecurrence_MONTHLY = 5;
	static final int eRecurrence_YEARLY = 6;

	static final String cICalValue_RECUR_FREQ = "FREQ=";
	static final int cICalValue_RECUR_FREQ_LEN = 5;

	static final String cICalValue_RECUR_SECONDLY = "SECONDLY";
	static final String cICalValue_RECUR_MINUTELY = "MINUTELY";
	static final String cICalValue_RECUR_HOURLY = "HOURLY";
	static final String cICalValue_RECUR_DAILY = "DAILY";
	static final String cICalValue_RECUR_WEEKLY = "WEEKLY";
	static final String cICalValue_RECUR_MONTHLY = "MONTHLY";
	static final String cICalValue_RECUR_YEARLY = "YEARLY";

	static final String cICalValue_RECUR_UNTIL = "UNTIL=";
	static final String cICalValue_RECUR_COUNT = "COUNT=";

	static final String cICalValue_RECUR_INTERVAL = "INTERVAL=";
	static final String cICalValue_RECUR_BYSECOND = "BYSECOND=";
	static final String cICalValue_RECUR_BYMINUTE = "BYMINUTE=";
	static final String cICalValue_RECUR_BYHOUR = "BYHOUR=";
	static final String cICalValue_RECUR_BYDAY = "BYDAY=";
	static final String cICalValue_RECUR_BYMONTHDAY = "BYMONTHDAY=";
	static final String cICalValue_RECUR_BYYEARDAY = "BYYEARDAY=";
	static final String cICalValue_RECUR_BYWEEKNO = "BYWEEKNO=";
	static final String cICalValue_RECUR_BYMONTH = "BYMONTH=";
	static final String cICalValue_RECUR_BYSETPOS = "BYSETPOS=";
	static final String cICalValue_RECUR_WKST = "WKST=";

	static final int eRecurrence_WEEKDAY_SU = 0;
	static final int eRecurrence_WEEKDAY_MO = 1;
	static final int eRecurrence_WEEKDAY_TU = 2;
	static final int eRecurrence_WEEKDAY_WE = 3;
	static final int eRecurrence_WEEKDAY_TH = 4;
	static final int eRecurrence_WEEKDAY_FR = 5;
	static final int eRecurrence_WEEKDAY_SA = 6;

	static final String cICalValue_RECUR_WEEKDAY_SU = "SU";
	static final String cICalValue_RECUR_WEEKDAY_MO = "MO";
	static final String cICalValue_RECUR_WEEKDAY_TU = "TU";
	static final String cICalValue_RECUR_WEEKDAY_WE = "WE";
	static final String cICalValue_RECUR_WEEKDAY_TH = "TH";
	static final String cICalValue_RECUR_WEEKDAY_FR = "FR";
	static final String cICalValue_RECUR_WEEKDAY_SA = "SA";

//	 2445 �4.8.1.11
	static final int eStatus_VEvent_None = 0;
	static final int eStatus_VEvent_Confirmed = 1;
	static final int eStatus_VEvent_Tentative = 2;
	static final int eStatus_VEvent_Cancelled = 3;

	static final int eStatus_VToDo_None = 0;
	static final int eStatus_VToDo_NeedsAction = 1;
	static final int eStatus_VToDo_InProcess = 2;
	static final int eStatus_VToDo_Completed = 3;
	static final int eStatus_VToDo_Cancelled = 4;

	static final int eStatus_VJournal_None = 0;
	static final int eStatus_VJournal_Final = 1;
	static final int eStatus_VJournal_Draft = 2;
	static final int eStatus_VJournal_Cancelled = 3;

	static final String cICalProperty_STATUS_TENTATIVE = "TENTATIVE";
	static final String cICalProperty_STATUS_CONFIRMED = "CONFIRMED";
	static final String cICalProperty_STATUS_CANCELLED = "CANCELLED";
	static final String cICalProperty_STATUS_NEEDS_ACTION = "NEEDS-ACTION";
	static final String cICalProperty_STATUS_COMPLETED = "COMPLETED";
	static final String cICalProperty_STATUS_IN_PROCESS = "IN-PROCESS";
	static final String cICalProperty_STATUS_DRAFT = "DRAFT";
	static final String cICalProperty_STATUS_FINAL = "FINAL";

//	 2445 �4.8.6.1
	static final int eAction_VAlarm_Audio = 0;
	static final int eAction_VAlarm_Display = 1;
	static final int eAction_VAlarm_Email = 2;
	static final int eAction_VAlarm_Procedure = 3;
	static final int eAction_VAlarm_Unknown = 4;

	static final String cICalProperty_ACTION_AUDIO = "AUDIO";
	static final String cICalProperty_ACTION_DISPLAY = "DISPLAY";
	static final String cICalProperty_ACTION_EMAIL = "EMAIL";
	static final String cICalProperty_ACTION_PROCEDURE = "PROCEDURE";


//	 Mulberry extensions
	static final String cICalProperty_X_PRIVATE_RURL = "X-MULBERRY-PRIVATE-RURL";
	static final String cICalProperty_X_PRIVATE_ETAG = "X-MULBERRY-PRIVATE-ETAG";

	static final String cICalProperty_ACTION_X_SPEAKTEXT = "X-MULBERRY-SPEAK-TEXT";
	static final String cICalProperty_ALARM_X_LASTTRIGGER = "X-MULBERRY-LAST-TRIGGER";

	static final String cICalProperty_ALARM_X_ALARMSTATUS = "X-MULBERRY-ALARM-STATUS";

	static final int eAlarm_Status_Pending = 0;
	static final int eAlarm_Status_Completed = 1;
	static final int eAlarm_Status_Disabled = 2;

	static final String cICalProperty_ALARM_X_ALARMSTATUS_PENDING = "PENDING";
	static final String cICalProperty_ALARM_X_ALARMSTATUS_COMPLETED = "COMPLETED";
	static final String cICalProperty_ALARM_X_ALARMSTATUS_DISABLED = "DISABLED";

	static final String cICalAttribute_ORGANIZER_X_IDENTITY = "X-MULBERRY-IDENTITY";
	static final String cICalAttribute_ATTENDEE_X_NEEDS_ITIP = "X-MULBERRY-NEEDS-ITIP";

}
