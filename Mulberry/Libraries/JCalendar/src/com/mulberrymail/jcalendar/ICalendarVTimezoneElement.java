/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import com.mulberrymail.utils.SortFunctor;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarVTimezoneElement extends ICalendarVTimezone {

	protected ICalendarDateTime mStart;

	protected int mUTCOffset;

	protected String mTZName;

	protected ICalendarRecurrenceSet mRecurrences;

	protected ICalendarDateTime		mCachedExpandBelow;
	protected ICalendarDateTimeList	mCachedExpandBelowItems;

	public static class sort_dtstart extends SortFunctor {
		public sort_dtstart() {
			super();
		}

		public boolean less_than(Object o1, Object o2) {
			ICalendarComponent s1 = (ICalendarComponent) o1;
			ICalendarComponent s2 = (ICalendarComponent) o2;

			ICalendarVTimezoneElement e1 = (ICalendarVTimezoneElement) s1;
			ICalendarVTimezoneElement e2 = (ICalendarVTimezoneElement) s2;
			return e1.mStart.lt(e2.mStart);
		}
	}

	public ICalendarVTimezoneElement(int calendar) {
		super(calendar);
		mStart = new ICalendarDateTime();
		mTZName = new String();
		mUTCOffset = 0;
		mRecurrences = new ICalendarRecurrenceSet();
		mCachedExpandBelow = null;
		mCachedExpandBelowItems = null;
	}

	public ICalendarVTimezoneElement(int calendar, ICalendarDateTime dt,
			int offset) {
		super(calendar);
		mStart = dt;
		mTZName = new String();
		mUTCOffset = offset;
		mRecurrences = new ICalendarRecurrenceSet();
		mCachedExpandBelow = null;
		mCachedExpandBelowItems = null;
	}

	public ICalendarVTimezoneElement(ICalendarVTimezoneElement copy) {
		super(copy);
		mStart = new ICalendarDateTime(copy.mStart);
		mTZName = copy.mTZName;
		mUTCOffset = copy.mUTCOffset;
		mRecurrences = copy.mRecurrences;
		mCachedExpandBelow = null;
		mCachedExpandBelowItems = null;
	}

	public void finalise() {
		// Get DTSTART
		loadValue(ICalendarDefinitions.cICalProperty_DTSTART, mStart);

		// Get TZOFFSETTO
		Integer temp = loadValueInteger(ICalendarDefinitions.cICalProperty_TZOFFSETTO,
				ICalendarValue.eValueType_UTC_Offset);
		if (temp != null)
			mUTCOffset = temp.intValue();

		// Get TZNAME
		String temps = loadValueString(ICalendarDefinitions.cICalProperty_TZNAME);
		if (temps != null)
			mTZName = temps;

		// Get RRULEs
		loadValueRRULE(ICalendarDefinitions.cICalProperty_RRULE, mRecurrences,
				true);

		// Get RDATEs
		loadValueRDATE(ICalendarDefinitions.cICalProperty_RDATE, mRecurrences,
				true);

		// Do inherited
		super.finalise();
	}

	public ICalendarDateTime getStart() {
		return mStart;
	}

	public int getUTCOffset() {
		return mUTCOffset;
	}

	public String getTZName() {
		return mTZName;
	}

	public ICalendarDateTime expandBelow(ICalendarDateTime below) {
		// Look for recurrences
		if (!mRecurrences.hasRecurrence() || (mStart.gt(below)))
			// Return DTSTART even if it is newer
			return mStart;
		else {
			// We want to allow recurrence calculation caching to help us here
			// as this method
			// gets called a lot - most likely for ever increasing dt values
			// (which will therefore
			// invalidate the recurrence cache).
			//
			// What we will do is round up the date-time to the next year so
			// that the recurrence
			// cache is invalidated less frequently

			ICalendarDateTime temp = new ICalendarDateTime(below);
			temp.setMonth(1);
			temp.setDay(1);
			temp.setHHMMSS(0, 0, 0);
			temp.offsetYear(1);

			// Use cache of expansion
			if (mCachedExpandBelowItems == null)
				mCachedExpandBelowItems = new ICalendarDateTimeList();
			if (mCachedExpandBelow == null)
				mCachedExpandBelow = new ICalendarDateTime(mStart);
			if (temp.gt(mCachedExpandBelow))
			{
				mCachedExpandBelowItems.removeAllElements();
				ICalendarPeriod period = new ICalendarPeriod(mStart, temp);
				mRecurrences.expand(mStart, period, mCachedExpandBelowItems);
				mCachedExpandBelow = temp;
			}
			
			if (mCachedExpandBelowItems.size() != 0) {
				// List comes back sorted so we pick the element just less than
				// the dt value we want
				for (int i = 0; i < mCachedExpandBelowItems.size(); i++) {
					if (((ICalendarDateTime) mCachedExpandBelowItems.elementAt(i)).gt(below)) {
						if (i != 0)
							return (ICalendarDateTime) mCachedExpandBelowItems.elementAt(i - 1);
						break;
					}
				}
				
				// The last one in the list is the one we want
				return (ICalendarDateTime) mCachedExpandBelowItems.elementAt(mCachedExpandBelowItems.size() - 1);
			}

			return mStart;
		}
	}
}