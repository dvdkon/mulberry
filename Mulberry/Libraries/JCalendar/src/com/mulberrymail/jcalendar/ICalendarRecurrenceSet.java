/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Enumeration;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarRecurrenceSet {

	protected ICalendarRecurrenceList mRrules;

	protected ICalendarRecurrenceList mExrules;

	protected ICalendarDateTimeList mRdates;

	protected ICalendarDateTimeList mExdates;

	protected ICalendarPeriodList mRperiods;

	protected ICalendarPeriodList mExperiods;

	public ICalendarRecurrenceSet() {
		super();
		mRrules = new ICalendarRecurrenceList();
		mExrules = new ICalendarRecurrenceList();
		mRdates = new ICalendarDateTimeList();
		mExdates = new ICalendarDateTimeList();
		mRperiods = new ICalendarPeriodList();
		mExperiods = new ICalendarPeriodList();
	}

	public ICalendarRecurrenceSet(ICalendarRecurrenceSet copy) {
		mRrules = new ICalendarRecurrenceList(copy.mRrules);
		mExrules = new ICalendarRecurrenceList(copy.mExrules);
		mRdates = new ICalendarDateTimeList(copy.mRdates);
		mExdates = new ICalendarDateTimeList(copy.mExdates);
		mRperiods = new ICalendarPeriodList(copy.mRperiods);
		mExperiods = new ICalendarPeriodList(copy.mExperiods);
	}

	public boolean hasRecurrence() {
		return !mRrules.isEmpty() || !mRdates.isEmpty() || !mRperiods.isEmpty()
				|| !mExrules.isEmpty() || !mExdates.isEmpty()
				|| !mExperiods.isEmpty();
	}

	public boolean equals(ICalendarRecurrenceSet comp) {
		// Look at RRULEs
		if (!equalsRules(mRrules, comp.mRrules))
			return false;

		// Look at EXRULEs
		if (!equalsRules(mExrules, comp.mExrules))
			return false;

		// Look at RDATEs
		if (!equalsDates(mRdates, comp.mRdates))
			return false;
		if (!equalsPeriods(mRperiods, comp.mRperiods))
			return false;

		// Look at EXDATEs
		if (!equalsDates(mExdates, comp.mExdates))
			return false;
		if (!equalsPeriods(mExperiods, comp.mExperiods))
			return false;

		// If we get here they match
		return true;
	}

	private boolean equalsRules(ICalendarRecurrenceList rules1,
			ICalendarRecurrenceList rules2) {
		// Check sizes first
		if (rules1.size() != rules2.size())
			return false;
		else if (rules1.size() == 0)
			return true;

		// Do sledge hammer O(n^2) approach as its not easy to sort these things
		// for a smarter test.
		// In most cases there will only be one rule anyway, so this should not
		// be too painful.

		ICalendarRecurrenceList temp2 = new ICalendarRecurrenceList(rules2);

		for (Enumeration iter1 = rules1.elements(); iter1.hasMoreElements();) {
			ICalendarRecurrence r1 = (ICalendarRecurrence) iter1.nextElement();
			boolean found = false;
			for (Enumeration iter2 = temp2.elements(); iter2.hasMoreElements();) {
				ICalendarRecurrence r2 = (ICalendarRecurrence) iter2
						.nextElement();
				if (r1.equals(r2)) {
					// Remove the one found so it is not tested again
					temp2.remove(r2);
					found = true;
					break;
				}
			}

			if (!found)
				return false;
		}

		return true;
	}

	private boolean equalsDates(ICalendarDateTimeList dates1,
			ICalendarDateTimeList dates2) {
		// Check sizes first
		if (dates1.size() != dates2.size())
			return false;
		else if (dates1.size() == 0)
			return true;

		// Copy each and sort for comparison
		ICalendarDateTimeList dt1 = new ICalendarDateTimeList(dates1);
		ICalendarDateTimeList dt2 = new ICalendarDateTimeList(dates2);

		dt1.sort();
		dt2.sort();

		return dt1.equal(dt2);
	}

	private boolean equalsPeriods(ICalendarPeriodList periods1,
			ICalendarPeriodList periods2) {
		// Check sizes first
		if (periods1.size() != periods2.size())
			return false;
		else if (periods1.size() == 0)
			return true;

		// Copy each and sort for comparison
		ICalendarPeriodList p1 = new ICalendarPeriodList(periods1);
		ICalendarPeriodList p2 = new ICalendarPeriodList(periods2);

		p1.sort();
		p2.sort();

		return p1.equal(p2);
	}

	public void add(ICalendarRecurrence rule) {
		mRrules.addElement(rule);
	}

	public void subtract(ICalendarRecurrence rule) {
		mExrules.addElement(rule);
	}

	public void add(ICalendarDateTime dt) {
		mRdates.addElement(dt);
	}

	public void subtract(ICalendarDateTime dt) {
		mExdates.addElement(dt);
	}

	public void add(ICalendarPeriod p) {
		mRperiods.addElement(p);
	}

	public void subtract(ICalendarPeriod p) {
		mExperiods.addElement(p);
	}

	public ICalendarRecurrenceList getRules() {
		return mRrules;
	}

	public ICalendarRecurrenceList getExrules() {
		return mExrules;
	}

	public ICalendarDateTimeList getDates() {
		return mRdates;
	}

	public ICalendarDateTimeList getExdates() {
		return mExdates;
	}

	public ICalendarPeriodList getPeriods() {
		return mRperiods;
	}

	public ICalendarPeriodList getExperiods() {
		return mExperiods;
	}

	public void expand(ICalendarDateTime start, ICalendarPeriod range,
			ICalendarDateTimeList items) {
		// Now create list of items to include
		ICalendarDateTimeList include = new ICalendarDateTimeList();

		// Always include the initial DTSTART if within the range
		if (range.isDateWithinPeriod(start))
			include.add(start);

		// RRULES
		for (Enumeration e = mRrules.elements(); e.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			iter.expand(start, range, include);
		}

		// RDATES
		for (Enumeration e = mRdates.elements(); e.hasMoreElements();) {
			ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();
			if (range.isDateWithinPeriod(iter))
				include.addElement(iter);
		}
		for (Enumeration e = mRperiods.elements(); e.hasMoreElements();) {
			ICalendarPeriod iter = (ICalendarPeriod) e.nextElement();
			if (range.isPeriodOverlap(iter))
				include.addElement(iter.getStart());
		}

		// Make sure the list is unique
		include.sort();
		include.unique();

		// Now create list of items to exclude
		ICalendarDateTimeList exclude = new ICalendarDateTimeList();

		// EXRULES
		for (Enumeration e = mExrules.elements(); e.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			iter.expand(start, range, exclude);
		}

		// EXDATES
		for (Enumeration e = mExdates.elements(); e.hasMoreElements();) {
			ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();
			if (range.isDateWithinPeriod(iter))
				exclude.addElement(iter);
		}
		for (Enumeration e = mExperiods.elements(); e.hasMoreElements();) {
			ICalendarPeriod iter = (ICalendarPeriod) e.nextElement();
			if (range.isPeriodOverlap(iter))
				exclude.addElement(iter.getStart());
		}

		// Make sure the list is unique
		exclude.sort();
		exclude.unique();

		// Add difference between to the two sets (include - exclude) to the
		// results
		ICalendarDateTimeList.set_difference(include, exclude, items);
	}

	public void changed() {
		// RRULES
		for (Enumeration e = mRrules.elements(); e.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			iter.clear();
		}

		// EXRULES
		for (Enumeration e = mExrules.elements(); e.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			iter.clear();
		}
	}

	public void excludeFutureRecurrence(ICalendarDateTime exclude) {
		// Adjust RRULES to end before start
		for (Enumeration e = mRrules.elements(); e.hasMoreElements();) {
			ICalendarRecurrence iter = (ICalendarRecurrence) e.nextElement();
			iter.excludeFutureRecurrence(exclude);
		}

		// Remove RDATES on or after start
		mRdates.removeOnOrAfter(exclude);
		for (Enumeration e = mRperiods.elements(); e.hasMoreElements();) {
			ICalendarPeriod iter = (ICalendarPeriod) e.nextElement();
			if (iter.gt(exclude))
				mRperiods.remove(iter);
		}
	}

	// UI operations
	public boolean isSimpleUI() {
		// Right now the Event dialog only handles a single RRULE (but we allow
		// any number of EXDATES as deleted
		// instances will appear as EXDATES)
		if ((mRrules.size() > 1) || (mExrules.size() > 0)
				|| (mRdates.size() > 0) || (mRperiods.size() > 0))
			return false;

		// Also, check the rule iteself
		else if (mRrules.size() == 1)
			return ((ICalendarRecurrence) mRrules.firstElement())
					.isSimpleRule();
		else
			return true;
	}

	public boolean isAdvancedUI() {
		// Right now the Event dialog only handles a single RRULE
		if ((mRrules.size() > 1) || (mExrules.size() > 0)
				|| (mRdates.size() > 0) || (mRperiods.size() > 0))
			return false;

		// Also, check the rule iteself
		else if (mRrules.size() == 1)
			return ((ICalendarRecurrence) mRrules.firstElement())
					.isAdvancedRule();
		else
			return true;
	}

	public ICalendarRecurrence getUIRecurrence() {
		return (mRrules.size() == 1) ? (ICalendarRecurrence) mRrules
				.firstElement() : null;
	}

	public String getUIDescription() {
		// Check for anything
		if (!hasRecurrence())
			return "No Recurrence";

		// Look for a single RRULE and return its descriptor
		if ((mRrules.size() == 1) && mExrules.isEmpty() && mRdates.isEmpty()
				&& mExdates.isEmpty() && mRperiods.isEmpty()
				&& mExperiods.isEmpty()) {
			return ((ICalendarRecurrence) mRrules.firstElement())
					.getUIDescription();
		}

		// Indicate some form of complex recurrence
		return "Multiple recurrence rules, dates or exclusions";
	}

}