/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarDuration {

	protected boolean mForward;

	protected int mWeeks;

	protected int mDays;

	protected int mHours;

	protected int mMinutes;

	protected int mSeconds;

	/**
	 *  
	 */
	public ICalendarDuration() {
		super();

		mForward = true;

		mWeeks = 0;
		mDays = 0;

		mHours = 0;
		mMinutes = 0;
		mSeconds = 0;
	}

	public ICalendarDuration(long seconds) {
		super();

		setDuration(seconds);
	}

	public ICalendarDuration(ICalendarDuration copy) {
		_copy_ICalendarDuration(copy);
	}

	public void _copy_ICalendarDuration(ICalendarDuration copy) {
		mForward = copy.mForward;

		mWeeks = copy.mWeeks;
		mDays = copy.mDays;

		mHours = copy.mHours;
		mMinutes = copy.mMinutes;
		mSeconds = copy.mSeconds;
	}

	public void copy(ICalendarDuration copy) {
		_copy_ICalendarDuration(copy);
	}

	public long getTotalSeconds() {
		return (mForward ? 1 : -1)
				* ((long) mSeconds + ((long) mMinutes + ((long) mHours + ((long) mDays + ((long) mWeeks * 7)) * 24) * 60) * 60);
	}

	public void setDuration(long seconds) {
		mForward = seconds >= 0;

		long remainder = seconds;
		if (remainder < 0)
			remainder = -remainder;

		// Is it an exact number of weeks - if so use the weeks value, otherwise
		// days, hours, minutes, seconds
		if (remainder % (7 * 24 * 60 * 60) == 0) {
			mWeeks = (int) (remainder / (7 * 24 * 60 * 60));
			mDays = 0;

			mHours = 0;
			mMinutes = 0;
			mSeconds = 0;
		} else {
			mSeconds = (int) (remainder % 60);
			remainder -= mSeconds;
			remainder /= 60;

			mMinutes = (int) (remainder % 60);
			remainder -= mMinutes;
			remainder /= 60;

			mHours = (int) (remainder % 24);
			remainder -= mHours;

			mDays = (int) (remainder / 24);

			mWeeks = 0;
		}

	}

	public boolean getForward() {
		return mForward;
	}

	public int getWeeks() {
		return mWeeks;
	}

	public int getDays() {
		return mDays;
	}

	public int getHours() {
		return mHours;
	}

	public int getMinutes() {
		return mMinutes;
	}

	public int getSeconds() {
		return mSeconds;
	}

	public void parse(String data) {
		// parse format ([+]/-) "P" (dur-date / dur-time / dur-week)

		StringTokenizer st = new StringTokenizer(data, "+-PTWDHMS", true);
		if (!st.hasMoreTokens())
			return;
		String token = st.nextToken();

		// Look for +/-
		mForward = true;
		if (token.equals("-")) {
			mForward = false;
			if (!st.hasMoreTokens())
				return;
			token = st.nextToken();
		} else if (token.equals("+")) {
			if (!st.hasMoreTokens())
				return;
			token = st.nextToken();
		}

		// Must have a 'P'
		if (!token.equals("P"))
			return;

		// Look for time
		if (!st.hasMoreTokens())
			return;
		token = st.nextToken();
		if (!token.equals("T")) {
			// Must have a number
			int num = Integer.parseInt(token);

			// Now look at character
			if (!st.hasMoreTokens())
				return;
			token = st.nextToken();
			if (token.equals("W")) {
				// Have a number of weeks
				mWeeks = num;

				// There cannot bew anything else after this so just exit
				return;
			} else if (token.equals("D")) {
				// Have a number of days
				mDays = num;

				// Look for more data - exit if none
				if (!st.hasMoreTokens())
					return;
				token = st.nextToken();

				// Look for time - exit if none
				if (!token.equals("T"))
					return;
			} else
				// Error in format
				return;
		}

		// Have time
		if (!st.hasMoreTokens())
			return;
		token = st.nextToken();
		int num = Integer.parseInt(token);

		// Look for hour
		if (!st.hasMoreTokens())
			return;
		token = st.nextToken();
		if (token.equals("H")) {
			// Get hours
			mHours = num;

			// Look for more data - exit if none
			if (!st.hasMoreTokens())
				return;

			// Parse the next number
			token = st.nextToken();
			num = Integer.parseInt(token);

			// Parse the next char
			if (!st.hasMoreTokens())
				return;
			token = st.nextToken();
		}

		// Look for minute
		if (token.equals("M")) {
			// Get hours
			mMinutes = num;

			// Look for more data - exit if none
			if (!st.hasMoreTokens())
				return;

			// Parse the next number
			token = st.nextToken();
			num = Integer.parseInt(token);

			// Parse the next char
			if (!st.hasMoreTokens())
				return;
			token = st.nextToken();
		}

		// Look for seconds
		if (token.equals("S")) {
			// Get hours
			mSeconds = num;
		}

	}

	public void generate(BufferedWriter os) {
		try {
			if (!mForward) {
				os.write("-");
			}
			os.write("P");

			if (mWeeks != 0) {
				os.write(Integer.toString(mWeeks));
				os.write("W");
			} else {
				if (mDays != 0) {
					os.write(Integer.toString(mDays));
					os.write("D");
				}

				if ((mHours != 0) || (mMinutes != 0) || (mSeconds != 0)) {
					os.write("T");
				}

				if (mHours != 0) {
					os.write(Integer.toString(mHours));
					os.write("H");
				}

				if ((mMinutes != 0) || ((mHours != 0) && (mSeconds != 0))) {
					os.write(Integer.toString(mMinutes));
					os.write("M");
				}

				if (mSeconds != 0) {
					os.write(Integer.toString(mSeconds));
					os.write("S");
				}
			}
		} catch (IOException e) {
		}

	}

}