/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.*;
import java.util.*;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarAttribute {

	// Fields
	protected String mName;

	protected String mValue;	// Only used when there is a single value

	protected Vector mValues;	// array of String, only used when there is more than one

	public ICalendarAttribute() {
		mName = new String();
		mValue = new String();
		mValues = null;
	}
	
	public ICalendarAttribute(String name, String value) {
		mName = name;
		mValue = value;
		mValues = null;
	}
	
	public ICalendarAttribute(ICalendarAttribute copy) {
		mName = copy.mName;
		mValue = copy.mValue;
		if (copy.mValues != null) {
		mValues = new Vector();
			for (Enumeration e = copy.mValues.elements(); e.hasMoreElements();)
				mValues.addElement((String) e.nextElement());
		}
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		mName = name;
	}

	public String getFirstValue() {
		return (mValues != null) ? (String) mValues.firstElement() : mValue;
	}

	/**
	 * @return Returns the values.
	 */
	public Vector getValues() {
		return mValues;
	}

	/**
	 * @param values
	 *            The values to set.
	 */
	public void setValues(Vector values) {
		mValues = values;
	}

	public void addValue(String value) {
		// See if switch from single to multi-value is needed
		if (mValues == null) {
			mValues = new Vector();
			mValues.addElement(mValue);
			mValue = null;
		}
		mValues.addElement(value);
	}

	public void generate(BufferedWriter os) {
		try {
			os.write(mName);
			os.write("=");

			if (mValues == null) {
				// Write with quotation if required
				generateValue(os, mValue);
			} else {
			for (int i = 0; i < mValues.size(); i++) {
				String s = (String) mValues.elementAt(i);
				if (i > 0)
					os.write(",");

					// Write with quotation if required
					generateValue(os, s);
				}
			}
		} catch (IOException e) {
			// We ignore errors
		}
	}
	
	private void generateValue(BufferedWriter os, String str) throws IOException
	{
				// Look for quoting
		if (str.indexOf(":") != -1 || str.indexOf(";") != -1
				|| str.indexOf(",") != -1) {
					os.write("\"");
			os.write(str);
					os.write("\"");
				} else
			os.write(str);
	}
}