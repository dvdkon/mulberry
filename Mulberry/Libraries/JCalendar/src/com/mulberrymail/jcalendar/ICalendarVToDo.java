/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Vector;

import com.mulberrymail.utils.SortFunctor;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarVToDo extends ICalendarComponentRecur {

	public static final int eOverdue = 0;

	public static final int eDueNow = 1;

	public static final int eDueLater = 2;

	public static final int eDone = 3;

	public static final int eCancelled = 4;

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINVTODO;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDVTODO;

	protected int mPriority;

	protected int mStatus;

	protected int mPercentComplete;

	protected ICalendarDateTime mCompleted;

	protected boolean mHasCompleted;

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String getVEnd() {
		return sEndDelimiter;
	}

	// std::sort comparitors
	public static class sort_for_display extends SortFunctor {
		public sort_for_display() {
			super();
		}

		public boolean less_than(Object o1, Object o2) {
			ICalendarComponentExpanded e1 = (ICalendarComponentExpanded) o1;
			ICalendarComponentExpanded e2 = (ICalendarComponentExpanded) o2;

			ICalendarVToDo s1 = (ICalendarVToDo) e1.getMaster();
			ICalendarVToDo s2 = (ICalendarVToDo) e2.getMaster();

			// Check status first (convert None -> Needs action for tests)
			int status1 = s1.mStatus;
			int status2 = s2.mStatus;
			if (status1 == ICalendarDefinitions.eStatus_VToDo_None)
				status1 = ICalendarDefinitions.eStatus_VToDo_NeedsAction;
			if (status2 == ICalendarDefinitions.eStatus_VToDo_None)
				status2 = ICalendarDefinitions.eStatus_VToDo_NeedsAction;
			if (status1 != status2) {
				// More important ones at the top
				return status1 < status2;
			}

			// At this point the status of each is the same

			// If status is cancelled sort by start time
			if (s1.mStatus == ICalendarDefinitions.eStatus_VToDo_Cancelled) {
				// Older ones at the bottom
				return s1.mStart.gt(s2.mStart);
			}

			// If status is completed sort by completion time
			if (s1.mStatus == ICalendarDefinitions.eStatus_VToDo_Completed) {
				// Older ones at the bottom
				return s1.mCompleted.gt(s2.mCompleted);
			}

			// Check due date exists
			if (s1.mHasEnd != s2.mHasEnd) {
				ICalendarDateTime now = new ICalendarDateTime();
				now.setToday();

				// Ones with due dates after today below ones without due dates
				if (s1.hasEnd())
					return s1.mEnd.le(now);
				else if (s2.hasEnd())
					return now.lt(s2.mEnd);
			}

			// Check due dates if present
			if (s1.mHasEnd) {
				if (s1.mEnd.ne(s2.mEnd)) {
					// Soonest dues dates above later ones
					return s1.mEnd.lt(s2.mEnd);
				}
			}

			// Check priority next
			if (s1.mPriority != s2.mPriority) {
				// Higher priority above lower ones
				return s1.mPriority < s2.mPriority;
			}

			// Just use start time - older ones at the top
			return s1.mStart.lt(s2.mStart);
		}
	}

	public ICalendarVToDo(int calendar) {
		super(calendar);
		mPriority = 0;
		mStatus = ICalendarDefinitions.eStatus_VToDo_None;
		mPercentComplete = 0;
		mCompleted = new ICalendarDateTime();
		mHasCompleted = false;
	}

	public ICalendarVToDo(ICalendarVToDo copy) {
		super(copy);
		mPriority = copy.mPriority;
		mStatus = copy.mStatus;
		mPercentComplete = copy.mPercentComplete;
		mCompleted = new ICalendarDateTime(copy.mCompleted);
		mHasCompleted = copy.mHasCompleted;
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVToDo(this);
	}

	public int getType() {
		return ICalendarComponent.eVTODO;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

	public String getMimeComponentName() {
		return ITIPDefinitions.cICalMIMEComponent_VTODO;
	}

	public boolean addComponent(ICalendarComponent comp) {
		// We can embed the alarm components only
		if (comp.getType() == ICalendarComponent.eVALARM) {
			if (mEmbedded == null)
				mEmbedded = new Vector();
			mEmbedded.addElement(comp);
			comp.setEmbedder(this);
			return true;
		} else
			return false;
	}

	public int getStatus() {
		return mStatus;
	}

	public void setStatus(int status) {
		mStatus = status;
	}

	public String getStatusText() {
		StringBuffer sout = new StringBuffer();

		switch (mStatus) {
		case ICalendarDefinitions.eStatus_VToDo_NeedsAction:
		case ICalendarDefinitions.eStatus_VToDo_InProcess:
		default:
			if (hasEnd()) {
				// Check due date
				ICalendarDateTime today = new ICalendarDateTime();
				today.setToday();
				if (getEnd().gt(today)) {
					sout.append("Due: ");
					ICalendarDuration whendue = getEnd().subtract(today);
					if ((whendue.getDays() > 0) && (whendue.getDays() <= 7))
						sout.append(whendue.getDays()).append(" days");
					else
						sout.append(getEnd().getLocaleDate(
								ICalendarDateTime.eNumericDate));
				} else if (getEnd().eq(today))
					sout.append("Due today");
				else {
					sout.append("Overdue: ");
					ICalendarDuration overdue = today.subtract(getEnd());
					if (overdue.getWeeks() != 0)
						sout.append(overdue.getWeeks()).append(" weeks");
					else
						sout.append(overdue.getDays() + 1).append(" days");
				}
			} else
				sout.append("Not Completed");
			break;
		case ICalendarDefinitions.eStatus_VToDo_Completed:
			if (hasCompleted()) {
				sout.append("Completed: ").append(
						getCompleted().getLocaleDate(
								ICalendarDateTime.eNumericDate));
			} else
				sout.append("Completed");
			break;
		case ICalendarDefinitions.eStatus_VToDo_Cancelled:
			sout.append("Cancelled");
			break;
		}

		return sout.toString();
	}

	public int getCompletionState() {
		switch (mStatus) {
		case ICalendarDefinitions.eStatus_VToDo_NeedsAction:
		case ICalendarDefinitions.eStatus_VToDo_InProcess:
		default:
			if (hasEnd()) {
				// Check due date
				ICalendarDateTime today = new ICalendarDateTime();
				today.setToday();
				if (getEnd().gt(today))
					return eDueLater;
				else if (getEnd().eq(today))
					return eDueNow;
				else
					return eOverdue;
			} else
				return eDueNow;
		case ICalendarDefinitions.eStatus_VToDo_Completed:
			return eDone;
		case ICalendarDefinitions.eStatus_VToDo_Cancelled:
			return eCancelled;
		}
	}

	public int getPriority() {
		return mPriority;
	}

	public void setPriority(int priority) {
		mPriority = priority;
	}

	public ICalendarDateTime getCompleted() {
		return mCompleted;
	}

	public boolean hasCompleted() {
		return mHasCompleted;
	}

	public void finalise() {
		// Do inherited
		super.finalise();

		// Get DUE
		if (!loadValue(ICalendarDefinitions.cICalProperty_DUE, mEnd)) {
			// Try DURATION instead
			ICalendarDuration temp = new ICalendarDuration();
			if (loadValue(ICalendarDefinitions.cICalProperty_DURATION, temp)) {
				mEnd = mStart.add(temp);
				mHasEnd = true;
			} else
				mHasEnd = false;
		} else
			mHasEnd = true;

		// Get PRIORITY
		loadValue(ICalendarDefinitions.cICalProperty_PRIORITY, mPriority);

		// Get STATUS
		{
			String temp = loadValueString(ICalendarDefinitions.cICalProperty_STATUS);
			if (temp != null) {
				if (temp == ICalendarDefinitions.cICalProperty_STATUS_NEEDS_ACTION) {
					mStatus = ICalendarDefinitions.eStatus_VToDo_NeedsAction;
				} else if (temp == ICalendarDefinitions.cICalProperty_STATUS_COMPLETED) {
					mStatus = ICalendarDefinitions.eStatus_VToDo_Completed;
				} else if (temp == ICalendarDefinitions.cICalProperty_STATUS_IN_PROCESS) {
					mStatus = ICalendarDefinitions.eStatus_VToDo_InProcess;
				} else if (temp == ICalendarDefinitions.cICalProperty_STATUS_CANCELLED) {
					mStatus = ICalendarDefinitions.eStatus_VToDo_Cancelled;
				}
			}
		}

		// Get PERCENT-COMPLETE
		loadValue(ICalendarDefinitions.cICalProperty_PERCENT_COMPLETE,
				mPercentComplete);

		// Get COMPLETED
		mHasCompleted = loadValue(ICalendarDefinitions.cICalProperty_COMPLETED,
				mCompleted);
	}

	// Editing
	public void editStatus(int status) {
		// Only if it is different
		if (mStatus != status) {
			// Updated cached values
			mStatus = status;

			// Remove existing STATUS & COMPLETED items
			removeProperties(ICalendarDefinitions.cICalProperty_STATUS);
			removeProperties(ICalendarDefinitions.cICalProperty_COMPLETED);
			mHasCompleted = false;

			// Now create properties
			{
				String value = null;
				switch (status) {
				case ICalendarDefinitions.eStatus_VToDo_NeedsAction:
				default:
					value = ICalendarDefinitions.cICalProperty_STATUS_NEEDS_ACTION;
					break;
				case ICalendarDefinitions.eStatus_VToDo_Completed:
					value = ICalendarDefinitions.cICalProperty_STATUS_COMPLETED;
					{
						// Add the completed item
						mCompleted.setNowUTC();
						mHasCompleted = true;
						ICalendarProperty prop = new ICalendarProperty(
								ICalendarDefinitions.cICalProperty_STATUS_COMPLETED,
								mCompleted);
						addProperty(prop);
					}
					break;
				case ICalendarDefinitions.eStatus_VToDo_InProcess:
					value = ICalendarDefinitions.cICalProperty_STATUS_IN_PROCESS;
					break;
				case ICalendarDefinitions.eStatus_VToDo_Cancelled:
					value = ICalendarDefinitions.cICalProperty_STATUS_CANCELLED;
					break;
				}
				ICalendarProperty prop = new ICalendarProperty(
						ICalendarDefinitions.cICalProperty_STATUS, value);
				addProperty(prop);
			}
		}
	}

	public void editCompleted(ICalendarDateTime completed) {
		// Remove existing COMPLETED item
		removeProperties(ICalendarDefinitions.cICalProperty_COMPLETED);
		mHasCompleted = false;

		// Always UTC
		mCompleted = new ICalendarDateTime(completed);
		mCompleted.adjustToUTC();
		mHasCompleted = true;
		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_STATUS_COMPLETED, mCompleted);
		addProperty(prop);
	}
}