/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarIntegerValue extends ICalendarValue {

	protected int mValue;
	/**
	 * 
	 */
	public ICalendarIntegerValue() {
		super();
		mValue = 0;
	}

	public ICalendarIntegerValue(int value) {
		super();
		mValue = value;
	}

	public ICalendarIntegerValue(ICalendarIntegerValue copy) {
		super();
		mValue = copy.mValue;
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#clone_it()
	 */
	public ICalendarValue clone_it() {
		return new ICalendarIntegerValue(this);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#getType()
	 */
	public int getType() {
		return eValueType_Integer;
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#parse(java.lang.String)
	 */
	public void parse(String data) {
		mValue = Integer.parseInt(data);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#generate(java.io.DataOutput)
	 */
	public void generate(BufferedWriter os) {
		try {
			os.write(Integer.toString(mValue));
		} catch (IOException e) {
		}
	}

	/**
	 * @return Returns the value.
	 */
	public int getValue() {
		return mValue;
	}
	/**
	 * @param value The value to set.
	 */
	public void setValue(int value) {
		mValue = value;
	}
}
