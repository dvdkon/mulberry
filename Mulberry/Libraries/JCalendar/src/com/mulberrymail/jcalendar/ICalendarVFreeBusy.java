/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Enumeration;
import java.util.Vector;

import com.mulberrymail.utils.MultiMap;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarVFreeBusy extends ICalendarComponent {

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINVFREEBUSY;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDVFREEBUSY;

	protected ICalendarDateTime mStart;

	protected boolean mHasStart;

	protected ICalendarDateTime mEnd;

	protected boolean mHasEnd;

	protected boolean mDuration;

	protected boolean mCachedBusyTime;

	protected ICalendarPeriod mSpanPeriod;

	protected ICalendarFreeBusyList mBusyTime;

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String gGetVEnd() {
		return sEndDelimiter;
	}

	public ICalendarVFreeBusy(int calendar) {
		super(calendar);
		mStart = new ICalendarDateTime();
		mHasStart = false;
		mEnd = new ICalendarDateTime();
		mHasEnd = false;
		mDuration = false;
		mCachedBusyTime = false;
		mBusyTime = null;
	}

	public ICalendarVFreeBusy(ICalendarVFreeBusy copy) {
		super(copy);
		mStart = new ICalendarDateTime(copy.mStart);
		mHasStart = copy.mHasStart;
		mEnd = new ICalendarDateTime(copy.mEnd);
		mHasEnd = copy.mHasEnd;
		mDuration = copy.mDuration;
		mCachedBusyTime = false;
		mBusyTime = null;
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVFreeBusy(this);
	}

	public int getType() {
		return ICalendarComponent.eVFREEBUSY;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

	public String getMimeComponentName() {
		return ITIPDefinitions.cICalMIMEComponent_VFREEBUSY;
	}

	public void finalise() {
		// Do inherited
		super.finalise();

		// Get DTSTART
		mHasStart = loadValue(ICalendarDefinitions.cICalProperty_DTSTART,
				mStart);

		// Get DTEND
		if (!loadValue(ICalendarDefinitions.cICalProperty_DTEND, mEnd)) {
			// Try DURATION instead
			ICalendarDuration temp = new ICalendarDuration();
			if (loadValue(ICalendarDefinitions.cICalProperty_DURATION, temp)) {
				mEnd = mStart.add(temp);
				mDuration = true;
			} else {
				// Force end to start, which will then be fixed to sensible
				// value later
				mEnd = mStart;
			}
		} else {
			mHasEnd = true;
			mDuration = false;
		}
	}

	public ICalendarDateTime getStart() {
		return mStart;
	}

	public boolean hasStart() {
		return mHasStart;
	}

	public ICalendarDateTime getEnd() {
		return mEnd;
	}

	public boolean hasEnd() {
		return mHasEnd;
	}

	public boolean UseDuration() {
		return mDuration;
	}

	public ICalendarPeriod getSpanPeriod() {
		return mSpanPeriod;
	}

	public Vector getBusyTime() {
		return mBusyTime;
	}

	// Generating info
	public void expandPeriodComp(ICalendarPeriod period, Vector list) {
		// Cache the busy-time details if not done already
		if (!mCachedBusyTime)
			cacheBusyTime();

		// See if period intersects the busy time span range
		if ((mBusyTime != null) && period.isPeriodOverlap(mSpanPeriod)) {
			list.add(this);
		}
	}

	public void expandPeriodFB(ICalendarPeriod period, Vector list) {
		// Cache the busy-time details if not done already
		if (!mCachedBusyTime)
			cacheBusyTime();

		// See if period intersects the busy time span range
		if ((mBusyTime != null) && period.isPeriodOverlap(mSpanPeriod)) {
			for (Enumeration e = mBusyTime.elements(); e.hasMoreElements();) {
				ICalendarFreeBusy fb = (ICalendarFreeBusy) e.nextElement();
				list.add(new ICalendarFreeBusy(fb));
			}
		}
	}

	private void cacheBusyTime() {

		// Clear out any existing cache
		mBusyTime = new ICalendarFreeBusyList();

		// Get all FREEBUSY items and add those that are BUSY
		ICalendarDateTime	min_start = new ICalendarDateTime();
		ICalendarDateTime	max_end = new ICalendarDateTime();
		MultiMap props = getProperties();
		Vector result = (Vector) props.find(ICalendarDefinitions.cICalProperty_FREEBUSY);
		for(Enumeration e = result.elements(); e.hasMoreElements(); )
		{
			ICalendarProperty iter = (ICalendarProperty)e.nextElement();

			// Check the properties FBTYPE attribute
			int type;
			boolean is_busy = false;
			if (iter.hasAttribute(ICalendarDefinitions.cICalAttribute_FBTYPE))
			{
				String fbyype = iter.getAttributeValue(ICalendarDefinitions.cICalAttribute_FBTYPE);
				if (fbyype.equalsIgnoreCase(ICalendarDefinitions.cICalAttribute_FBTYPE_BUSY))
				{
					is_busy = true;
					type = ICalendarFreeBusy.eBusy;
				}
				else if (fbyype.equalsIgnoreCase(ICalendarDefinitions.cICalAttribute_FBTYPE_BUSYUNAVAILABLE))
				{
					is_busy = true;
					type = ICalendarFreeBusy.eBusyUnavailable;
				}
				else if (fbyype.equalsIgnoreCase(ICalendarDefinitions.cICalAttribute_FBTYPE_BUSYTENTATIVE))
				{
					is_busy = true;
					type = ICalendarFreeBusy.eBusyTentative;
				}
				else
				{
					is_busy = false;
					type = ICalendarFreeBusy.eFree;
				}
			}
			else
			{
				// Default is busy when no attribute
				is_busy = true;
				type = ICalendarFreeBusy.eBusy;
			}

			// Add this period
			if (is_busy)
			{
				ICalendarMultiValue multi = iter.getMultiValue();
				if ((multi != null) && (multi.getType() == ICalendarValue.eValueType_Period))
				{
					for(Enumeration e2 = multi.getValues().elements(); e2.hasMoreElements();)
					{
						// Double-check type
						Object o = e.nextElement();
						ICalendarPeriodValue period = null;
						if (o instanceof ICalendarPeriodValue) {
							period = (ICalendarPeriodValue) o;
						}
						
						// Double-check type
						if (period != null)
						{
							mBusyTime.add(new ICalendarFreeBusy(type, period.getValue()));
							
							if (mBusyTime.size() == 1)
							{
								min_start = period.getValue().getStart();
								max_end = period.getValue().getEnd();
							}
							else
							{
								if (min_start.gt(period.getValue().getStart()))
									min_start = period.getValue().getStart();
								if (max_end.lt(period.getValue().getEnd()))
									max_end = period.getValue().getEnd();
							}
						}
					}
				}
			}
		}

		// If nothing present, empty the list
		if (mBusyTime.size() == 0)
		{
			mBusyTime = null;
		}
		else
		{
		
			// Sort the list by period
			mBusyTime.sort();

			// Determine range
			ICalendarDateTime	start;
			ICalendarDateTime	end;
			if (mHasStart)
				start = mStart;
			else
				start = min_start;
			if (mHasEnd)
				end = mEnd;
			else
				end = max_end;
			
			mSpanPeriod = new ICalendarPeriod(start, end);
		}
		
		mCachedBusyTime = true;
	}

}