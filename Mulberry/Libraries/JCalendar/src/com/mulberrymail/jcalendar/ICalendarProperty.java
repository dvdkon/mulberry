/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashSet;
import com.mulberrymail.utils.*;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarProperty {

	protected String mName;

	protected MultiMap mAttributes;

	protected ICalendarValue mValue;

	protected static SimpleMap sDefaultValueTypeMap;

	protected static SimpleMap sValueTypeMap;

	protected static SimpleMap sTypeValueMap;

	protected static HashSet sMultiValues;

	public static final void loadStatics()
	{
		_init_map();
	}

	public ICalendarProperty() {
		_init_ICalendarProperty();
	}

	public ICalendarProperty(String name, int int_value) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(int_value);
	}

	public ICalendarProperty(String name, String text_value) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(text_value, ICalendarValue.eValueType_Text);
	}

	public ICalendarProperty(String name, String text_value, int value_type) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(text_value, value_type);
	}

	public ICalendarProperty(String name, ICalendarDateTime dt) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(dt);
	}

	public ICalendarProperty(String name, ICalendarDateTimeList dtl) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(dtl);
	}

	public ICalendarProperty(String name, ICalendarDuration du) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(du);
	}

	public ICalendarProperty(String name, ICalendarPeriod pe) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(pe);
	}

	public ICalendarProperty(String name, ICalendarRecurrence recur) {
		_init_ICalendarProperty();
		mName = name;
		_init_attr_value(recur);
	}

	public ICalendarProperty(ICalendarProperty copy) {
		_init_ICalendarProperty();
		_copy_ICalendarProperty(copy);
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public MultiMap getAttributes() {
		return mAttributes;
	}

	public void setAttributes(MultiMap attributes) {
		mAttributes = attributes;
	}

	public boolean hasAttribute(String attr) {
		return mAttributes.count(attr) != 0;
	}

	public String getAttributeValue(String attr) {
		return ((ICalendarAttribute)mAttributes.findFirst(attr)).getFirstValue();
	}

	public void addAttribute(ICalendarAttribute attr) {
		mAttributes.insert(attr.getName(), attr);
	}

	public void removeAttributes(String attr) {
		mAttributes.erase(attr);
	}

	public ICalendarValue getValue() {
		return mValue;
	}

	public ICalendarCalAddressValue getCalAddressValue() {

		if (mValue instanceof ICalendarCalAddressValue)
			return (ICalendarCalAddressValue) mValue;
		else
			return null;
	}

	public ICalendarDateTimeValue getDateTimeValue() {

		if (mValue instanceof ICalendarDateTimeValue)
			return (ICalendarDateTimeValue) mValue;
		else
			return null;
	}

	public ICalendarDurationValue getDurationValue() {

		if (mValue instanceof ICalendarDurationValue)
			return (ICalendarDurationValue) mValue;
		else
			return null;
	}

	public ICalendarIntegerValue getIntegerValue() {

		if (mValue instanceof ICalendarIntegerValue)
			return (ICalendarIntegerValue) mValue;
		else
			return null;
	}

	public ICalendarMultiValue getMultiValue() {

		if (mValue instanceof ICalendarMultiValue)
			return (ICalendarMultiValue) mValue;
		else
			return null;
	}

	public ICalendarPeriodValue getPeriodValue() {

		if (mValue instanceof ICalendarPeriodValue)
			return (ICalendarPeriodValue) mValue;
		else
			return null;
	}

	public ICalendarRecurrenceValue getRecurrenceValue() {

		if (mValue instanceof ICalendarRecurrenceValue)
			return (ICalendarRecurrenceValue) mValue;
		else
			return null;
	}

	public ICalendarPlainTextValue getTextValue() {

		if (mValue instanceof ICalendarPlainTextValue)
			return (ICalendarPlainTextValue) mValue;
		else
			return null;
	}

	public ICalendarURIValue getURIValue() {

		if (mValue instanceof ICalendarURIValue)
			return (ICalendarURIValue) mValue;
		else
			return null;
	}

	public ICalendarUTCOffsetValue getUTCOffsetValue() {

		if (mValue instanceof ICalendarUTCOffsetValue)
			return (ICalendarUTCOffsetValue) mValue;
		else
			return null;
	}

	public boolean parse(String data) {
		// Look for attribute or value delimiter
		Stringref dataref = new Stringref(data);
		{
			String prop_name = StringUtils.strduptokenstr(dataref, ";:");
			if ((prop_name == null) || (prop_name.length() == 0))
				return false;

			// We have the name
			mName = prop_name;
			
			// Try to use static string for the name
			for(Enumeration e = sDefaultValueTypeMap.keys(); e.hasMoreElements(); )
			{
				String str = (String)e.nextElement();
				if (str.equals(prop_name))
					mName = str;
			}
		}

		// Now loop getting data
		boolean done = false;
		while (!done && (dataref.get().length() != 0)) {
			switch (dataref.get().charAt(0)) {
			case ';':
			// Parse attribute
			{
				// Move past delimiter
				dataref.set(dataref.get().substring(1));

				// Get quoted string or token
				String attribute_name = StringUtils
						.strduptokenstr(dataref, "=");
				if (attribute_name == null)
					return false;
				dataref.set(dataref.get().substring(1));
				String attribute_value = StringUtils.strduptokenstr(dataref,
						":;,");
				if (attribute_value == null)
					return false;

				// Now add attribute value
				ICalendarAttribute attrvalue = new ICalendarAttribute(
						attribute_name, attribute_value);
				mAttributes.insert(attribute_name, attrvalue);

				// Look for additional values
				while (dataref.get().charAt(0) == ',') {
					dataref.set(dataref.get().substring(1));
					String attribute_value2 = StringUtils.strduptokenstr(
							dataref, ":;,");
					if (attribute_value2 == null)
						return false;
				}
			}
				break;
			case ':':
				dataref.set(dataref.get().substring(1));
				createValue(dataref.get());
				done = true;
				break;
			default:
				;
			}
		}

		// We must have a value of some kind
		return (mValue != null);
	}

	public void generate(BufferedWriter os) {

		// Write it out always with value
		generate(os, false);
	}

	public void generateFiltered(BufferedWriter os, ICalendarOutputFilter filter) {
		
		// Check for property in filter and whether value is written out
		Booleanref novalue = new Booleanref();
		if (filter.testPropertyValue(mName, novalue))
		{
			generate(os, novalue.get());
		}
	}

	// Write out the actual property, possibly skipping the value
	protected void generate(BufferedWriter os, boolean novalue) {

		setupValueAttribute();

		// Must write to temp buffer and then wrap
		try {
			StringWriter bout = new StringWriter();
			BufferedWriter sout = new BufferedWriter(bout);
			sout.write(mName);

			// Write all attributes
			for (Enumeration e = mAttributes.iterator(); e.hasMoreElements();) {
				sout.write(";");
				((ICalendarAttribute) e.nextElement()).generate(sout);
			}

			// Write value
			sout.write(":");
			if ((mValue != null) && !novalue)
				mValue.generate(sout);

			/*
			 * cdstring temp; temp.steal(sout.str()); if (temp.length() < 75) os < <
			 * temp.c_str(); else { const char* p = temp.c_str(); const char* q =
			 * p + temp.length(); while(p < q) { if (p != temp.c_str()) { os < <
			 * endl < < " "; }
			 * 
			 * int bytes = q - p; if (bytes > 74) bytes = 74; os.write(p,
			 * bytes); p += bytes; } }
			 */

			// Flush all data to stream
			sout.flush();
			sout.close();

			// Get string text
			String temp = bout.toString();

			// Convert to UTF8 byte array
			byte[] utf8 = temp.getBytes("UTF8");

			// Look for line length exceed
			if (utf8.length < 75) {
				os.write(temp);
			} else {
				// Look for valid utf8 range and write that out
				int start = 0;
				int written = 0;
				while (written < utf8.length) {
					// Start 74 chars on from where we are
					int offset = start + 74;
					if (offset < utf8.length) {
						String line = new String(utf8, start, utf8.length
								- start, "UTF8");
						os.write(line);
						written = utf8.length;
					} else {
						// Check whether next char is valid utf8 lead byte
						while ((utf8[offset] > 0x7F)
								&& ((utf8[offset] & 0xC0) == 0x80)) {
							// Step back until we have a valid char
							offset--;
						}
						
						String line = new String(utf8, start, offset - start, "UTF8");
						os.write(line);
						os.write("\n ");
						written += offset - start;
					}
				}
			}
			os.write("\n");
		} catch (IOException e) {
		}
	}

	private void _init_ICalendarProperty() {
		mName = new String();
		mAttributes = new MultiMap();
		mValue = null;
	}

	private void _copy_ICalendarProperty(ICalendarProperty copy) {
		mName = new String(copy.mName);
		mAttributes = new MultiMap(copy.mAttributes);
		mValue = copy.mValue.clone_it();
	}

	private static final void _init_map() {
		// Only if empty
		if (sDefaultValueTypeMap == null) {
			sDefaultValueTypeMap = new SimpleMap();

			// 2445 �4.8.1
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_ATTACH,
					new Integer(ICalendarValue.eValueType_URI));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_CATEGORIES, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_CLASS,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_COMMENT, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_DESCRIPTION,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_GEO,
					new Integer(ICalendarValue.eValueType_Geo));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_LOCATION, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_PERCENT_COMPLETE,
					new Integer(ICalendarValue.eValueType_Integer));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_PRIORITY, new Integer(
							ICalendarValue.eValueType_Integer));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_RESOURCES, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_STATUS,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_SUMMARY, new Integer(
							ICalendarValue.eValueType_Text));

			// 2445 �4.8.2
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_COMPLETED, new Integer(
							ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_DTEND,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_DUE,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_DTSTART, new Integer(
							ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_DURATION, new Integer(
							ICalendarValue.eValueType_Duration));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_FREEBUSY, new Integer(
							ICalendarValue.eValueType_Period));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_TRANSP,
					new Integer(ICalendarValue.eValueType_Text));

			// 2445 �4.8.3
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_TZID,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_TZNAME,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_TZOFFSETFROM,
					new Integer(ICalendarValue.eValueType_UTC_Offset));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_TZOFFSETTO, new Integer(
							ICalendarValue.eValueType_UTC_Offset));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_TZURL,
					new Integer(ICalendarValue.eValueType_URI));

			// 2445 �4.8.4
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_ATTENDEE, new Integer(
							ICalendarValue.eValueType_CalAddress));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_CONTACT, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_ORGANIZER, new Integer(
							ICalendarValue.eValueType_CalAddress));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_RECURRENCE_ID,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_RELATED_TO, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_URL,
					new Integer(ICalendarValue.eValueType_URI));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_UID,
					new Integer(ICalendarValue.eValueType_Text));

			// 2445 �4.8.5
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_EXDATE,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_EXRULE,
					new Integer(ICalendarValue.eValueType_Recur));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_RDATE,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_RRULE,
					new Integer(ICalendarValue.eValueType_Recur));

			// 2445 �4.8.6
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_ACTION,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_REPEAT,
					new Integer(ICalendarValue.eValueType_Integer));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_TRIGGER, new Integer(
							ICalendarValue.eValueType_Duration));

			// 2445 �4.8.7
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_CREATED, new Integer(
							ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_DTSTAMP, new Integer(
							ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_LAST_MODIFIED,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(ICalendarDefinitions.cICalProperty_SEQUENCE,
					new Integer(ICalendarValue.eValueType_Integer));

			// Apple Extensions
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_XWRCALNAME, new Integer(
							ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_XWRCALDESC, new Integer(
							ICalendarValue.eValueType_Text));

			// Mulberry extensions
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_X_PRIVATE_RURL,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_X_PRIVATE_ETAG,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_ACTION_X_SPEAKTEXT,
					new Integer(ICalendarValue.eValueType_Text));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_ALARM_X_LASTTRIGGER,
					new Integer(ICalendarValue.eValueType_DateTime));
			sDefaultValueTypeMap.put(
					ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS,
					new Integer(ICalendarValue.eValueType_Text));
		}

		// Only if empty
		if (sValueTypeMap == null) {
			sValueTypeMap = new SimpleMap();

			// 2445 �4.3
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_BINARY,
					new Integer(ICalendarValue.eValueType_Binary));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_BOOLEAN,
					new Integer(ICalendarValue.eValueType_Boolean));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_CAL_ADDRESS,
					new Integer(ICalendarValue.eValueType_CalAddress));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_DATE,
					new Integer(ICalendarValue.eValueType_Date));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_DATE_TIME,
					new Integer(ICalendarValue.eValueType_DateTime));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_DURATION,
					new Integer(ICalendarValue.eValueType_Duration));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_FLOAT,
					new Integer(ICalendarValue.eValueType_Float));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_INTEGER,
					new Integer(ICalendarValue.eValueType_Integer));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_PERIOD,
					new Integer(ICalendarValue.eValueType_Period));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_RECUR,
					new Integer(ICalendarValue.eValueType_Recur));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_TEXT,
					new Integer(ICalendarValue.eValueType_Text));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_TIME,
					new Integer(ICalendarValue.eValueType_Time));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_URI, new Integer(
					ICalendarValue.eValueType_URI));
			sValueTypeMap.put(ICalendarDefinitions.cICalValue_UTC_OFFSET,
					new Integer(ICalendarValue.eValueType_UTC_Offset));

		}

		if (sTypeValueMap == null) {
			sTypeValueMap = new SimpleMap();

			// 2445 �4.3
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Binary),
					ICalendarDefinitions.cICalValue_BINARY);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Boolean),
					ICalendarDefinitions.cICalValue_BOOLEAN);
			sTypeValueMap.put(
					new Integer(ICalendarValue.eValueType_CalAddress),
					ICalendarDefinitions.cICalValue_CAL_ADDRESS);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Date),
					ICalendarDefinitions.cICalValue_DATE);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_DateTime),
					ICalendarDefinitions.cICalValue_DATE_TIME);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Duration),
					ICalendarDefinitions.cICalValue_DURATION);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Float),
					ICalendarDefinitions.cICalValue_FLOAT);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Geo),
					ICalendarDefinitions.cICalValue_FLOAT);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Integer),
					ICalendarDefinitions.cICalValue_INTEGER);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Period),
					ICalendarDefinitions.cICalValue_PERIOD);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Recur),
					ICalendarDefinitions.cICalValue_RECUR);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Text),
					ICalendarDefinitions.cICalValue_TEXT);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_Time),
					ICalendarDefinitions.cICalValue_TIME);
			sTypeValueMap.put(new Integer(ICalendarValue.eValueType_URI),
					ICalendarDefinitions.cICalValue_URI);
			sTypeValueMap.put(
					new Integer(ICalendarValue.eValueType_UTC_Offset),
					ICalendarDefinitions.cICalValue_UTC_OFFSET);

		}

		if (sMultiValues == null) {
			sMultiValues = new HashSet();

			sMultiValues.add(ICalendarDefinitions.cICalProperty_CATEGORIES);
			sMultiValues.add(ICalendarDefinitions.cICalProperty_RESOURCES);
			sMultiValues.add(ICalendarDefinitions.cICalProperty_FREEBUSY);
			sMultiValues.add(ICalendarDefinitions.cICalProperty_EXDATE);
			sMultiValues.add(ICalendarDefinitions.cICalProperty_RDATE);
		}
	}

	private void createValue(String data) {
		// Tidy first
		mValue = null;

		// Get value type from property name
		int type = ICalendarValue.eValueType_Text;
		_init_map();
		Integer found = (Integer) sDefaultValueTypeMap.get(mName);
		if (found != null)
			type = found.intValue();

		// Check whether custom value is set
		if (mAttributes.count(ICalendarDefinitions.cICalAttribute_VALUE) != 0) {
			found = (Integer) sValueTypeMap
					.get(getAttributeValue(ICalendarDefinitions.cICalAttribute_VALUE));
			if (found != null)
				type = found.intValue();
		}

		// Check for multivalued
		if (sMultiValues.contains(mName)) {
			mValue = new ICalendarMultiValue(type);
		} else {
			// Create the type
			mValue = ICalendarValue.createFromType(type);

			// Special post-create for some types
			switch (type) {
			case ICalendarValue.eValueType_Time:
			case ICalendarValue.eValueType_DateTime:
				// Look for TZID attribute
				if (hasAttribute(ICalendarDefinitions.cICalAttribute_TZID)) {
					((ICalendarDateTimeValue) mValue)
							.getValue()
							.setTimezoneID(getAttributeValue(ICalendarDefinitions.cICalAttribute_TZID));
				} else
					((ICalendarDateTimeValue) mValue).getValue().setTimezoneID(null);
				break;
			default:
				;
			}
		}

		// Now parse the data
		mValue.parse(data);
	}

	private void setupValueAttribute() {
		mAttributes.remove(ICalendarDefinitions.cICalAttribute_VALUE);

		// Only if we have a value right now
		if (mValue == null)
			return;

		// See if current type is default for this property
		_init_map();
		Integer found = (Integer) sDefaultValueTypeMap.get(mName);
		if (found != null) {
			int default_type = found.intValue();
			if (default_type != mValue.getType()) {
				String found2 = (String) sTypeValueMap.get(new Integer(mValue
						.getType()));
				if (found2 != null) {
					mAttributes.insert(
							ICalendarDefinitions.cICalAttribute_VALUE,
							new ICalendarAttribute(
									ICalendarDefinitions.cICalAttribute_VALUE,
									found2));
				}
			}
		}
	}

	// Creation
	private void _init_attr_value(int ival) {
		// Value
		mValue = new ICalendarIntegerValue(ival);

		// Attributes
		setupValueAttribute();
	}

	private void _init_attr_value(String txt, int value_type) {
		// Value
		mValue = ICalendarValue.createFromType(value_type);
		try {
			ICalendarPlainTextValue tvalue = (ICalendarPlainTextValue) mValue;
			tvalue.setValue(txt);
		} catch (ClassCastException e) {
		}

		// Attributes
		setupValueAttribute();
	}

	private void _init_attr_value(ICalendarDateTime dt) {
		// Value
		mValue = new ICalendarDateTimeValue(dt);

		// Attributes
		setupValueAttribute();

		// Look for timezone
		if (!dt.isDateOnly() && !dt.getTimezoneUTC()
				&& (dt.getTimezoneID() != null)) {
			mAttributes.remove(ICalendarDefinitions.cICalAttribute_TZID);
			mAttributes.insert(ICalendarDefinitions.cICalAttribute_TZID,
					new ICalendarAttribute(
							ICalendarDefinitions.cICalAttribute_TZID, dt
									.getTimezoneID()));
		}
	}

	private void _init_attr_value(ICalendarDateTimeList dtl) {
		// Value
		mValue = new ICalendarMultiValue(
				(dtl.size() > 0)
						&& ((ICalendarDateTime) dtl.firstElement())
								.isDateOnly() ? ICalendarValue.eValueType_Date
						: ICalendarValue.eValueType_DateTime);
		for (Enumeration e = dtl.elements(); e.hasMoreElements();) {
			ICalendarDateTimeValue iter = (ICalendarDateTimeValue) e
					.nextElement();
			((ICalendarMultiValue) mValue).addValue(new ICalendarDateTimeValue(
					iter));
		}

		// Attributes
		setupValueAttribute();

		// Look for timezone
		if ((dtl.size() > 0)
				&& !((ICalendarDateTime) dtl.firstElement()).isDateOnly()
				&& !((ICalendarDateTime) dtl.firstElement()).getTimezoneUTC()
				&& (((ICalendarDateTime) dtl.firstElement()).getTimezoneID() != null)) {
			mAttributes.remove(ICalendarDefinitions.cICalAttribute_TZID);
			mAttributes.insert(ICalendarDefinitions.cICalAttribute_TZID,
					new ICalendarAttribute(
							ICalendarDefinitions.cICalAttribute_TZID,
							((ICalendarDateTime) dtl.firstElement())
									.getTimezoneID()));
		}
	}

	private void _init_attr_value(ICalendarDuration du) {
		// Value
		mValue = new ICalendarDurationValue(du);

		// Attributes
		setupValueAttribute();
	}

	private void _init_attr_value(ICalendarPeriod pe) {
		// Value
		mValue = new ICalendarPeriodValue(pe);

		// Attributes
		setupValueAttribute();
	}

	private void _init_attr_value(ICalendarRecurrence recur) {
		// Value
		mValue = new ICalendarRecurrenceValue(recur);

		// Attributes
		setupValueAttribute();
	}

}