/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarUTCOffsetValue extends ICalendarValue {

	protected int mValue;
	/**
	 * 
	 */
	public ICalendarUTCOffsetValue() {
		super();
		mValue = 0;
	}

	public ICalendarUTCOffsetValue(int value) {
		super();
		mValue = value;
	}

	public ICalendarUTCOffsetValue(ICalendarUTCOffsetValue copy) {
		super();
		mValue = copy.mValue;
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#clone_it()
	 */
	public ICalendarValue clone_it() {
		return new ICalendarUTCOffsetValue(this);
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#getType()
	 */
	public int getType() {
		return eValueType_UTC_Offset;
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#parse(java.lang.String)
	 */
	public void parse(String data) {
		// Must be of specific lengths
		if ((data.length() != 5) && (data.length() != 7))
		{
			mValue = 0;
			return;
		}

		// Get sign
		boolean plus = (data.charAt(0) == '+');

		// Get hours
		String buf = data.substring(1, 3);

		int hours = Integer.parseInt(buf);

		// Get minutes
		buf = data.substring(3, 5);

		int mins = Integer.parseInt(buf);

		// Get seconds if present
		int secs = 0;
		if (data.length() == 7)
		{
			buf = data.substring(6);

			secs = Integer.parseInt(buf);
		}


		mValue = ((hours * 60) + mins) * 60 + secs;
		if (!plus)
			mValue = -mValue;
	}

	/* (non-Javadoc)
	 * @see com.cyrusoft.jcalendar.ICalendarValue#generate(java.io.DataOutput)
	 */
	public void generate(BufferedWriter os) {
		try {
			int abs_value = mValue;
			if (mValue < 0)
			{
				os.write("-");
				abs_value = -mValue;
			}
			else
				os.write("+");

			int secs = abs_value % 60;
			int mins = (abs_value / 60) % 60;
			int hours = abs_value / (60 * 60);

			if (hours < 10)
				os.write("0");
			os.write(Integer.toString(hours));
			if (mins < 10)
				os.write("0");
			os.write(Integer.toString(mins));
			if (secs != 0)
			{
				if (secs < 10)
					os.write("0");
				os.write(Integer.toString(secs));
			}
		} catch (IOException e) {
		}
	}

	/**
	 * @return Returns the value.
	 */
	public int getValue() {
		return mValue;
	}
	/**
	 * @param value The value to set.
	 */
	public void setValue(int value) {
		mValue = value;
	}
}
