/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarDateTime {

	public static final int eSunday = 0;

	public static final int eMonday = 1;

	public static final int eTuesday = 2;

	public static final int eWednesday = 3;

	public static final int eThursday = 4;

	public static final int eFriday = 5;

	public static final int eSaturday = 6;

	public static final int eFullDate = 0;

	public static final int eAbbrevDate = 1;

	public static final int eNumericDate = 2;

	public static final int eFullDateNoYear = 3;

	public static final int eAbbrevDateNoYear = 4;

	public static final int eNumericDateNoYear = 5;

	protected int mYear; // full 4-digit year

	protected int mMonth; // 1...12

	protected int mDay; // 1...31

	protected int mHours; // 0...23

	protected int mMinutes; // 0...59

	protected int mSeconds; // 0...60

	protected boolean mDateOnly;

	//protected ICalendarTimezone mTimezone;

	protected boolean mTZUTC;

	protected String mTZID;

	protected boolean mPosixTimeCached;

	protected long mPosixTime;

	/**
	 *  
	 */
	public ICalendarDateTime() {
		_init_ICalendarDateTime();
	}

	public ICalendarDateTime(int packed, ICalendarTimezone tzid) {
		_init_ICalendarDateTime();

		// Unpack a packed date
		mYear = ICalendarUtils.unpackDateYear(packed);
		mMonth = ICalendarUtils.unpackDateMonth(packed);
		mDay = ICalendarUtils.unpackDateDay(packed);
		if (mDay < 0)
			mDay = -mDay;
		mDateOnly = true;

		if (tzid != null) {
			mTZUTC = tzid.getUTC();
			mTZID = tzid.getTimezoneID();
		}

		normalise();
	}

	public ICalendarDateTime(int year, int month, int day,
			ICalendarTimezone tzid) {
		_init_ICalendarDateTime();
		mYear = year;
		mMonth = month;
		mDay = day;
		mDateOnly = true;
		if (tzid != null) {
			mTZUTC = tzid.getUTC();
			mTZID = tzid.getTimezoneID();
		}
	}

	public ICalendarDateTime(int year, int month, int day, int hours,
			int minutes, int seconds, ICalendarTimezone tzid) {
		_init_ICalendarDateTime();
		mYear = year;
		mMonth = month;
		mDay = day;
		mHours = hours;
		mMinutes = minutes;
		mSeconds = seconds;
		if (tzid != null) {
			mTZUTC = tzid.getUTC();
			mTZID = tzid.getTimezoneID();
		}
	}

	public ICalendarDateTime(ICalendarDateTime copy) {
		_copy_ICalendarDateTime(copy);
	}

	private void _init_ICalendarDateTime() {
		mYear = 1970;
		mMonth = 1;
		mDay = 1;

		mHours = 0;
		mMinutes = 0;
		mSeconds = 0;

		mDateOnly = false;

		mTZUTC = true;
		mTZID = null;

		mPosixTimeCached = false;
		mPosixTime = 0;

	}

	private void _copy_ICalendarDateTime(ICalendarDateTime copy) {
		mYear = copy.mYear;
		mMonth = copy.mMonth;
		mDay = copy.mDay;

		mHours = copy.mHours;
		mMinutes = copy.mMinutes;
		mSeconds = copy.mSeconds;

		mDateOnly = copy.mDateOnly;

		mTZUTC = copy.mTZUTC;
		mTZID = copy.mTZID;

		mPosixTimeCached = copy.mPosixTimeCached;
		mPosixTime = copy.mPosixTime;
	}

	public void copy(ICalendarDateTime copy) {
		_copy_ICalendarDateTime(copy);
	}

	// Operators
	public ICalendarDateTime add(ICalendarDuration duration) {
		// Add duration seconds to temp object and normalise it
		ICalendarDateTime result = new ICalendarDateTime(this);
		result.mSeconds += duration.getTotalSeconds();
		result.changed();
		result.normalise();

		return result;
	}

	public ICalendarDuration subtract(ICalendarDateTime date) {
		// Look for floating
		if (floating() || date.floating()) {
			// Adjust the floating ones to fixed
			ICalendarDateTime copy1 = new ICalendarDateTime(this);
			ICalendarDateTime copy2 = new ICalendarDateTime(date);

			if (copy1.floating() && copy2.floating()) {
				// Set both to UTC and do comparison
				copy1.setTimezoneUTC(true);
				copy2.setTimezoneUTC(true);
				return copy1.subtract(copy2);
			} else if (copy1.floating()) {
				// Set to be the same
				copy1.setTimezoneUTC(copy2.getTimezoneUTC());
				copy1.setTimezoneID(copy2.getTimezoneID());
				return copy1.subtract(copy2);
			} else {
				// Set to be the same
				copy2.setTimezoneUTC(copy1.getTimezoneUTC());
				copy2.setTimezoneID(copy1.getTimezoneID());
				return copy1.subtract(copy2);
			}
		} else {
			// Do diff of date-time in seconds
			long diff = getPosixTime() - date.getPosixTime();
			return new ICalendarDuration((int) diff);
		}

	}

	// Comparators
	public boolean eq(ICalendarDateTime comp) {
		return compareDateTime(comp) == 0;
	}

	public boolean ne(ICalendarDateTime comp) {
		return compareDateTime(comp) != 0;
	}

	public boolean ge(ICalendarDateTime comp) {
		return compareDateTime(comp) >= 0;
	}

	public boolean le(ICalendarDateTime comp) {
		return compareDateTime(comp) <= 0;
	}

	public boolean gt(ICalendarDateTime comp) {
		return compareDateTime(comp) > 0;
	}

	public boolean lt(ICalendarDateTime comp) {
		return compareDateTime(comp) < 0;
	}

	public int compareDateTime(ICalendarDateTime comp) {
		// If either are date only, then just do date compare
		if (mDateOnly || comp.mDateOnly) {
			if (mYear == comp.mYear) {
				if (mMonth == comp.mMonth) {
					if (mDay == comp.mDay)
						return 0;
					else
						return mDay < comp.mDay ? -1 : 1;
				} else
					return mMonth < comp.mMonth ? -1 : 1;
			} else
				return mYear < comp.mYear ? -1 : 1;
		}

		// If they have the same timezone do simple compare - no posix calc
		// needed
		else if (ICalendarTimezone.same(mTZUTC, mTZID, comp.mTZUTC, comp.mTZID)) {
			if (mYear == comp.mYear) {
				if (mMonth == comp.mMonth) {
					if (mDay == comp.mDay) {
						if (mHours == comp.mHours) {
							if (mMinutes == comp.mMinutes) {
								if (mSeconds == comp.mSeconds) {
									return 0;
								} else
									return mSeconds < comp.mSeconds ? -1 : 1;
							} else
								return mMinutes < comp.mMinutes ? -1 : 1;
						} else
							return mHours < comp.mHours ? -1 : 1;
					} else
						return mDay < comp.mDay ? -1 : 1;
				} else
					return mMonth < comp.mMonth ? -1 : 1;
			} else
				return mYear < comp.mYear ? -1 : 1;
		} else {
			long posix1 = getPosixTime();
			long posix2 = comp.getPosixTime();
			if (posix1 == posix2)
				return 0;
			else
				return posix1 < posix2 ? -1 : 1;
		}

	}

	public boolean compareDate(ICalendarDateTime comp) {
		return (mYear == comp.mYear) && (mMonth == comp.mMonth)
				&& (mDay == comp.mDay);
	}

	public long getPosixTime() {
		// Look for cached value (or floating time which has to be calculated
		// each time)
		if (!mPosixTimeCached || floating()) {
			long result = 0;

			// Add hour/mins/secs
			result = (mHours * 60L + mMinutes) * 60L + mSeconds;

			// Number of days since 1970
			result += daysSince1970() * 24L * 60L * 60L;

			// Adjust for timezone offset
			result -= timeZoneSecondsOffset();

			// Now indcate cache state
			mPosixTimeCached = true;
			mPosixTime = result;
		}

		return mPosixTime;

	}

	public boolean isDateOnly() {
		return mDateOnly;
	}

	public void setDateOnly(boolean date_only) {
		mDateOnly = date_only;
	}

	public int getYear() {
		return mYear;
	}

	public void setYear(int year) {
		if (mYear != year) {
			mYear = year;
			changed();
		}
	}

	public void offsetYear(int diff_year) {
		mYear += diff_year;
		normalise();
	}

	public int getMonth() {
		return mMonth;
	}

	public void setMonth(int month) {
		if (mMonth != month) {
			mMonth = month;
			changed();
		}
	}

	public void offsetMonth(int diff_month) {
		mMonth += diff_month;
		normalise();
	}

	public int getDay() {
		return mDay;
	}

	public void setDay(int day) {
		if (mDay != day) {
			mDay = day;
			changed();
		}
	}

	public void offsetDay(int diff_day) {
		mDay += diff_day;
		normalise();
	}

	public void setYearDay(int day) {
		// 1 .. 366 offset from start, or
		// -1 .. -366 offset from end

		if (day > 0) {
			// Offset current date to 1st January of current year
			mMonth = 1;
			mDay = 1;

			// Increment day
			mDay += day - 1;

			// Normalise to get proper month/day values
			normalise();
		} else if (day < 0) {
			// Offset current date to 1st January of next year
			mYear++;
			mMonth = 1;
			mDay = 1;

			// Decrement day
			mDay += day;

			// Normalise to get proper year/month/day values
			normalise();
		}
	}

	public int getYearDay() {
		return mDay + ICalendarUtils.daysUptoMonth(mMonth, mYear);
	}

	public void setMonthDay(int day) {
		// 1 .. 31 offset from start, or
		// -1 .. -31 offset from end

		if (day > 0) {
			// Offset current date to 1st of current month
			mDay = 1;

			// Increment day
			mDay += day - 1;

			// Normalise to get proper month/day values
			normalise();
		} else if (day < 0) {
			// Offset current date to 1st of next month
			mMonth++;
			mDay = 1;

			// Decrement day
			mDay += day;

			// Normalise to get proper year/month/day values
			normalise();
		}
	}

	public boolean isMonthDay(int day) {
		if (day > 0)
			return mDay == day;
		else if (day < 0)
			return mDay - 1 - ICalendarUtils.daysInMonth(mMonth, mYear) == day;
		else
			return false;
	}

	public void setWeekNo(int weekno) {
		// This is the iso 8601 week number definition

		// What day does the current year start on
		ICalendarDateTime temp = new ICalendarDateTime(mYear, 1, 1, null);
		int first_day = temp.getDayOfWeek();

		// Calculate and set yearday for start of week
		switch (first_day) {
		case eSunday:
		case eMonday:
		case eTuesday:
		case eWednesday:
		case eThursday:
			setYearDay((weekno - 1) * 7 - first_day);
		case eFriday:
		case eSaturday:
			setYearDay((weekno - 1) * 7 - first_day + 7);
		}
	}

	public int getWeekNo() {
		// This is the iso 8601 week number definition

		// What day does the current year start on
		ICalendarDateTime temp = new ICalendarDateTime(mYear, 1, 1, null);
		int first_day = temp.getDayOfWeek();

		// Get days upto the current one
		int yearday = getYearDay();

		switch (first_day) {
		case eSunday:
		case eMonday:
		case eTuesday:
		case eWednesday:
		case eThursday:
		default:
			return (yearday + first_day) / 7 + 1;
		case eFriday:
		case eSaturday:
			return (yearday + first_day - 7) / 7 + 1;
		}
	}

	public boolean isWeekNo(int weekno) {
		// This is the iso 8601 week number definition

		if (weekno > 0)
			return getWeekNo() == weekno;
		else
			// This needs to calculate the negative offset from the last week in
			// the current year
			return false;
	}

	public void setDayOfWeekInYear(int offset, int day) {
		// Set to first day in year
		mMonth = 1;
		mDay = 1;

		// Determine first weekday in year
		int first_day = getDayOfWeek();

		if (offset > 0) {
			int cycle = (offset - 1) * 7 + day;
			cycle -= first_day;
			if (first_day > day)
				cycle += 7;
			mDay = cycle + 1;
		} else if (offset < 0) {
			first_day += 365 + (ICalendarUtils.isLeapYear(mYear) ? 1 : 0) - 1;
			first_day %= 7;

			int cycle = (-offset - 1) * 7 - day;
			cycle += first_day;
			if (day > first_day)
				cycle += 7;
			mDay = 365 + (ICalendarUtils.isLeapYear(mYear) ? 1 : 0) - cycle;
		}

		normalise();
	}

	public void setDayOfWeekInMonth(int offset, int day) {
		// Set to first day in month
		mDay = 1;

		// Determine first weekday in month
		int first_day = getDayOfWeek();

		if (offset > 0) {
			int cycle = (offset - 1) * 7 + day;
			cycle -= first_day;
			if (first_day > day)
				cycle += 7;
			mDay = cycle + 1;
		} else if (offset < 0) {
			first_day += ICalendarUtils.daysInMonth(mMonth, mYear) - 1;
			first_day %= 7;

			int cycle = (-offset - 1) * 7 - day;
			cycle += first_day;
			if (day > first_day)
				cycle += 7;
			mDay = ICalendarUtils.daysInMonth(mMonth, mYear) - cycle;
		}

		normalise();
	}

	public boolean isDayOfWeekInMonth(int offset, int day) {
		// First of the actual day must match
		if (getDayOfWeek() != day)
			return false;

		// If there is no count the we match any of this day in the month
		if (offset == 0)
			return true;

		// Create temp date-time with the appropriate parameters and then
		// compare
		ICalendarDateTime temp = new ICalendarDateTime(this);
		temp.setDayOfWeekInMonth(offset, day);

		// Now compare dates
		return compareDate(temp);
	}

	public int getDayOfWeek() {
		// Count days since 01-Jan-1970 which was a Thursday
		int result = eThursday + daysSince1970();
		result %= 7;
		if (result < 0)
			result += 7;

		return result;
	}

	public String getMonthText(boolean short_txt) {
		// Make sure range is valid
		if ((mMonth < 1) || (mMonth > 12))
			return "";
		else
			return ICalendarLocale.getMonth(mMonth,
					short_txt ? ICalendarLocale.eShort : ICalendarLocale.eLong);
	}

	public String getDayOfWeekText(int day) {
		return ICalendarLocale.getDay(day, ICalendarLocale.eShort);
	}

	public void setHHMMSS(int hours, int minutes, int seconds) {
		if ((mHours != hours) || (mMinutes != minutes) || (mSeconds != seconds)) {
			mHours = hours;
			mMinutes = minutes;
			mSeconds = seconds;
			changed();
		}
	}

	public int getHours() {
		return mHours;
	}

	public void setHours(int hours) {
		if (mHours != hours) {
			mHours = hours;
			changed();
		}
	}

	public void offsetHours(int diff_hour) {
		mHours += diff_hour;
		normalise();
	}

	public int getMinutes() {
		return mMinutes;
	}

	public void setMinutes(int minutes) {
		if (mMinutes != minutes) {
			mMinutes = minutes;
			changed();
		}
	}

	public void offsetMinutes(int diff_minutes) {
		mMinutes += diff_minutes;
		normalise();
	}

	public int getSeconds() {
		return mSeconds;
	}

	public void setSeconds(int seconds) {
		if (mSeconds != seconds) {
			mSeconds = seconds;
			changed();
		}
	}

	public void offsetSeconds(int diff_seconds) {
		mSeconds += diff_seconds;
		normalise();
	}

	public boolean getTimezoneUTC() {
		return mTZUTC;
	}

	public void setTimezoneUTC(boolean utc) {
		mTZUTC = utc;
	}

	public String getTimezoneID() {
		return mTZID;
	}

	public void setTimezoneID(String tzid) {
		mTZUTC = false;
		mTZID = tzid;
		changed();
	}

	public boolean floating() {
		return !mTZUTC && (mTZID == null);
	}

	//public ICalendarTimezone getTimezone() {
	//	return mTimezone;
	//}

	public void setTimezone(ICalendarTimezone tzid) {
		mTZUTC = tzid.getUTC();
		mTZID = tzid.getTimezoneID();
		changed();
	}

	public void adjustTimezone(ICalendarTimezone tzid) {
		// Only if different
		String s1 = tzid.getTimezoneID();
		if ((tzid.getUTC() != mTZUTC) || (s1 != mTZID)) {
			int offset_from = timeZoneSecondsOffset();
			setTimezone(tzid);
			int offset_to = timeZoneSecondsOffset();

			offsetSeconds(offset_to - offset_from);
		}
	}

	public void adjustToUTC() {
		if (!mTZUTC) {
			ICalendarTimezone utc = new ICalendarTimezone(true);

			int offset_from = timeZoneSecondsOffset();
			setTimezone(utc);
			int offset_to = timeZoneSecondsOffset();

			offsetSeconds(offset_to - offset_from);
		}
	}

	public ICalendarDateTime getAdjustedTime() {
		return getAdjustedTime(ICalendarManager.sICalendarManager
				.getDefaultTimezone());
	}

	public ICalendarDateTime getAdjustedTime(ICalendarTimezone tzid) {
		// Copy this and adjust to input timezone
		ICalendarDateTime adjusted = new ICalendarDateTime(this);
		adjusted.adjustTimezone(tzid);
		return adjusted;
	}

	public void setToday() {
		_copy_ICalendarDateTime(getToday(new ICalendarTimezone(mTZUTC, mTZID)));
	}

	public static ICalendarDateTime getToday(ICalendarTimezone tzid) {
		// Get from posix time
		Calendar rightNow = Calendar.getInstance();

		return new ICalendarDateTime(rightNow.get(Calendar.YEAR), rightNow
				.get(Calendar.MONTH) + 1, rightNow.get(Calendar.DAY_OF_MONTH),
				tzid);
	}

	public void setNow() {
		_copy_ICalendarDateTime(getNow(new ICalendarTimezone(mTZUTC, mTZID)));
	}

	public static ICalendarDateTime getNow(ICalendarTimezone tzid) {
		// Get from posix time
		Calendar rightNow = Calendar.getInstance();

		return new ICalendarDateTime(rightNow.get(Calendar.YEAR), rightNow
				.get(Calendar.MONTH) + 1, rightNow.get(Calendar.DAY_OF_MONTH),
				rightNow.get(Calendar.HOUR), rightNow.get(Calendar.MINUTE),
				rightNow.get(Calendar.SECOND), tzid);
	}

	public void setNowUTC() {
		_copy_ICalendarDateTime(getNowUTC());
	}

	public static ICalendarDateTime getNowUTC() {
		// Get from posix time
		Calendar rightNow = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		ICalendarTimezone tzid = new ICalendarTimezone(true);

		return new ICalendarDateTime(rightNow.get(Calendar.YEAR), rightNow
				.get(Calendar.MONTH) + 1, rightNow.get(Calendar.DAY_OF_MONTH),
				rightNow.get(Calendar.HOUR), rightNow.get(Calendar.MINUTE),
				rightNow.get(Calendar.SECOND), tzid);
	}

	public void recur(int freq, int interval) {
		// Add appropriate interval
		switch (freq) {
		case ICalendarDefinitions.eRecurrence_SECONDLY:
			mSeconds += interval;
			break;
		case ICalendarDefinitions.eRecurrence_MINUTELY:
			mMinutes += interval;
			break;
		case ICalendarDefinitions.eRecurrence_HOURLY:
			mHours += interval;
			break;
		case ICalendarDefinitions.eRecurrence_DAILY:
			mDay += interval;
			break;
		case ICalendarDefinitions.eRecurrence_WEEKLY:
			mDay += 7 * interval;
			break;
		case ICalendarDefinitions.eRecurrence_MONTHLY:
			// Iterate until a valid day in the next month is found.
			// This can happen if adding one month to e.g. 31 January.
			// That is an undefined operation - does it mean 28/29 February
			// or 1/2 May, or 31 March or what? We choose to find the next month with
			// the same day number as the current one.
			do
			{
				mMonth += interval;
			} while(mDay > ICalendarUtils.daysInMonth(mMonth, mYear));
			break;
		case ICalendarDefinitions.eRecurrence_YEARLY:
			mYear += interval;
			break;
		}

		// Normalise to standard date-time ranges
		normalise();
	}

	public String getLocaleDate(int locale) {

		StringBuffer buf = new StringBuffer(64);

		switch (locale) {
		case eFullDate:
			buf.append(ICalendarLocale.getDay(getDayOfWeek(),
					ICalendarLocale.eLong));
			buf.append(", ");
			buf.append(ICalendarLocale.getMonth(mMonth, ICalendarLocale.eLong));
			buf.append(" ");
			buf.append(Integer.toString(mDay));
			buf.append(", ");
			buf.append(Integer.toString(mYear));
			break;
		case eAbbrevDate:
			buf.append(ICalendarLocale.getDay(getDayOfWeek(),
					ICalendarLocale.eShort));
			buf.append(", ");
			buf
					.append(ICalendarLocale.getMonth(mMonth,
							ICalendarLocale.eShort));
			buf.append(" ");
			buf.append(Integer.toString(mDay));
			buf.append(", ");
			buf.append(Integer.toString(mYear));
			break;
		case eNumericDate:
			buf.append(Integer.toString(mMonth));
			buf.append("/");
			buf.append(Integer.toString(mDay));
			buf.append("/");
			buf.append(Integer.toString(mYear));
			break;
		case eFullDateNoYear:
			buf.append(ICalendarLocale.getDay(getDayOfWeek(),
					ICalendarLocale.eLong));
			buf.append(", ");
			buf.append(ICalendarLocale.getMonth(mMonth, ICalendarLocale.eLong));
			buf.append(" ");
			buf.append(Integer.toString(mDay));
			break;
		case eAbbrevDateNoYear:
			buf.append(ICalendarLocale.getDay(getDayOfWeek(),
					ICalendarLocale.eShort));
			buf.append(", ");
			buf
					.append(ICalendarLocale.getMonth(mMonth,
							ICalendarLocale.eShort));
			buf.append(" ");
			buf.append(Integer.toString(mDay));
			break;
		case eNumericDateNoYear:
			buf.append(Integer.toString(mMonth));
			buf.append("/");
			buf.append(Integer.toString(mDay));
			break;
		}

		return buf.toString();
	}

	public String getTime(boolean with_seconds, boolean am_pm, boolean tzid) {

		StringBuffer buf = new StringBuffer(32);
		int adjusted_hour = mHours;

		if (am_pm) {
			boolean am = true;
			if (adjusted_hour >= 12) {
				adjusted_hour -= 12;
				am = false;
			}
			if (adjusted_hour == 0)
				adjusted_hour = 12;

			buf.append(Integer.toString(adjusted_hour));
			buf.append(":");
			if (mMinutes < 10)
				buf.append("0");
			buf.append(Integer.toString(mMinutes));
			if (with_seconds) {
				buf.append(":");
				if (mSeconds < 10)
					buf.append("0");
				buf.append(Integer.toString(mSeconds));
			}
			buf.append(am ? " AM" : " PM");
		} else {
			if (mHours < 10)
				buf.append("0");
			buf.append(Integer.toString(mHours));
			buf.append(":");
			if (mMinutes < 10)
				buf.append("0");
			buf.append(Integer.toString(mMinutes));
			if (with_seconds) {
				buf.append(":");
				if (mSeconds < 10)
					buf.append("0");
				buf.append(Integer.toString(mSeconds));
			}
		}

		if (tzid) {
			String desc = timeZoneDescriptor();
			if (desc.length() > 0) {
				buf.append(" ");
				buf.append(desc);
			}
		}

		return buf.toString();
	}

	public String getLocaleDateTime(int locale, boolean with_seconds,
			boolean am_pm, boolean tzid) {

		return getLocaleDate(locale) + " " + getTime(with_seconds, am_pm, tzid);
	}

	public String getText() {
		StringBuffer buf = new StringBuffer();

		// Do year
		if (mYear < 1000) {
			if (mYear < 10)
				buf.append("000");
			else if (mYear < 100)
				buf.append("00");
			else
				buf.append("0");
		}
		buf.append(Integer.toString(mYear));

		// Do month
		if (mMonth < 10)
			buf.append("0");
		buf.append(Integer.toString(mMonth));

		// Do day
		if (mDay < 10)
			buf.append("0");
		buf.append(Integer.toString(mDay));

		// Check for time
		if (!mDateOnly) {
			buf.append("T");

			// Do hour
			if (mHours < 10)
				buf.append("0");
			buf.append(Integer.toString(mHours));

			// Do minute
			if (mMinutes < 10)
				buf.append("0");
			buf.append(Integer.toString(mMinutes));

			// Do sedond
			if (mSeconds < 10)
				buf.append("0");
			buf.append(Integer.toString(mSeconds));

			if (mTZUTC) {
				buf.append("Z");
			}
		}

		return buf.toString();
	}

	public void parse(String data) {

		// parse format YYYYMMDD[THHMMSS[Z]]

		// Size must be at least 8
		if (data.length() < 8)
			return;

		// Get year
		String buf = data.substring(0, 4);
		mYear = Integer.parseInt(buf);

		// Get month
		buf = data.substring(4, 6);
		mMonth = Integer.parseInt(buf);

		// Get day
		buf = data.substring(6, 8);
		mDay = Integer.parseInt(buf);

		// Now look for more
		if ((data.length() >= 15) && (data.charAt(8) == 'T')) {
			// Get hours
			buf = data.substring(9, 11);
			mHours = Integer.parseInt(buf);

			// Get minutes
			buf = data.substring(11, 13);
			mMinutes = Integer.parseInt(buf);

			// Get seconds
			buf = data.substring(13, 15);
			mSeconds = Integer.parseInt(buf);

			mDateOnly = false;

			mTZUTC = (data.length() > 15) ? (data.charAt(15) == 'Z') : false;
		} else
			mDateOnly = true;

		// Always uncache posix time
		changed();
	}

	public void generate(BufferedWriter os) {
		try {
			os.write(getText());
		} catch (IOException e) {
		}
	}

	public void generateRFC2822(BufferedWriter os) {

	}

	protected void normalise() {
		// Normalise seconds
		int normalised_secs = mSeconds % 60;
		int adjustment_mins = mSeconds / 60;
		if (normalised_secs < 0) {
			normalised_secs += 60;
			adjustment_mins--;
		}
		mSeconds = normalised_secs;
		mMinutes += adjustment_mins;

		// Normalise minutes
		int normalised_mins = mMinutes % 60;
		int adjustment_hours = mMinutes / 60;
		if (normalised_mins < 0) {
			normalised_mins += 60;
			adjustment_hours--;
		}
		mMinutes = normalised_mins;
		mHours += adjustment_hours;

		// Normalise hours
		int normalised_hours = mHours % 24;
		int adjustment_days = mHours / 24;
		if (normalised_hours < 0) {
			normalised_hours += 24;
			adjustment_days--;
		}
		mHours = normalised_hours;
		mDay += adjustment_days;

		// Wipe the time if date only
		if (mDateOnly) {
			mSeconds = mMinutes = mHours = 0;
		}

		// Adjust the month first, since the day adjustment is month dependent

		// Normalise month
		int normalised_month = ((mMonth - 1) % 12) + 1;
		int adjustment_year = (mMonth - 1) / 12;
		if ((normalised_month - 1) < 0) {
			normalised_month += 12;
			adjustment_year--;
		}
		mMonth = normalised_month;
		mYear += adjustment_year;

		// Now do days
		if (mDay > 0) {
			while (mDay > ICalendarUtils.daysInMonth(mMonth, mYear)) {
				mDay -= ICalendarUtils.daysInMonth(mMonth, mYear);
				mMonth++;
				if (mMonth > 12) {
					mMonth = 1;
					mYear++;
				}
			}
		} else {
			while (mDay <= 0) {
				mMonth--;
				if (mMonth < 1) {
					mMonth = 12;
					mYear--;
				}
				mDay += ICalendarUtils.daysInMonth(mMonth, mYear);
			}
		}

		// Always invalidate posix time cache
		changed();
	}

	protected int timeZoneSecondsOffset() {
		return (int) new ICalendarTimezone(mTZUTC, mTZID)
				.timeZoneSecondsOffset(this);
	}

	protected String timeZoneDescriptor() {
		return new ICalendarTimezone(mTZUTC, mTZID).timeZoneDescriptor(this);
	}

	private void changed() {
		mPosixTimeCached = false;
	}

	private int daysSince1970() {
		// Add days betweenn 1970 and current year (ignoring leap days)
		int result = (mYear - 1970) * 365;

		// Add leap days between years
		result += ICalendarUtils.leapDaysSince1970(mYear - 1970);

		// Add days in current year up to current month (includes leap day for
		// current year as needed)
		result += ICalendarUtils.daysUptoMonth(mMonth, mYear);

		// Add days in month
		result += mDay - 1;

		return result;
	}

}