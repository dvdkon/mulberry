/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.*;

/**
 * @author cyrusdaboo
 *
 */
abstract public class ICalendarValue {

	public static final int eValueType_Binary = 0;
	public static final int eValueType_Boolean = 1;
	public static final int eValueType_CalAddress = 2;
	public static final int eValueType_Date = 3;
	public static final int eValueType_DateTime = 4;
	public static final int eValueType_Duration = 5;
	public static final int eValueType_Float = 6;
	public static final int eValueType_Geo = 7;
	public static final int eValueType_Integer = 8;
	public static final int eValueType_Period = 9;
	public static final int eValueType_Recur = 10;
	public static final int eValueType_Text = 11;
	public static final int eValueType_Time = 12;
	public static final int eValueType_URI = 13;
	public static final int eValueType_UTC_Offset = 14;
	public static final int eValueType_MultiValue = 15;
	public static final int eValueType_XName = 16;

	public static ICalendarValue createFromType(int type) {
		// Create the type
		switch(type)
		{
		case eValueType_Text:
			return new ICalendarTextValue();
		case eValueType_CalAddress:
			return new ICalendarCalAddressValue();
		case eValueType_URI:
			return new ICalendarURIValue();
		case eValueType_Integer:
			return new ICalendarIntegerValue();
		case eValueType_Date:
		case eValueType_DateTime:
			return new ICalendarDateTimeValue();
		case eValueType_Duration:
			return new ICalendarDurationValue();
		case eValueType_Period:
			return new ICalendarPeriodValue();
		case eValueType_Recur:
			return new ICalendarRecurrenceValue();
		case eValueType_UTC_Offset:
			return new ICalendarUTCOffsetValue();
		default:
			return new ICalendarDummyValue(type);
		}
	}

	public ICalendarValue() {}

	public abstract ICalendarValue clone_it();

	public abstract int getType();

	public abstract void parse(String data);
	public abstract void generate(BufferedWriter os);

}
