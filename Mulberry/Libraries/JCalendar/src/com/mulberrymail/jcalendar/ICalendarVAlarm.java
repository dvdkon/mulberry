/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarVAlarm extends ICalendarComponent {

	protected static final String sBeginDelimiter = ICalendarDefinitions.cICalComponent_BEGINVALARM;

	protected static final String sEndDelimiter = ICalendarDefinitions.cICalComponent_ENDVALARM;

	protected int mAction;

	protected ICalendarVAlarmAction mActionData;

	protected boolean mTriggerAbsolute;

	protected ICalendarDateTime mTriggerOn;

	protected ICalendarDuration mTriggerBy;

	protected boolean mTriggerOnStart;

	protected int mRepeats;

	protected ICalendarDuration mRepeatInterval;

	protected boolean mStatusInit;

	protected int mAlarmStatus;

	protected ICalendarDateTime mLastTrigger;

	protected ICalendarDateTime mNextTrigger;

	protected int mDoneCount;

	// Classes for each action encapsulating action-specific data
	private abstract class ICalendarVAlarmAction {
		protected int mType;

		public ICalendarVAlarmAction(int type) {
			mType = type;
		}

		public ICalendarVAlarmAction(ICalendarVAlarmAction copy) {
			mType = copy.mType;
		}

		public abstract ICalendarVAlarmAction clone_it();

		public abstract void load(ICalendarVAlarm valarm);

		public abstract void add(ICalendarVAlarm valarm);

		public abstract void remove(ICalendarVAlarm valarm);

		public int getType() {
			return mType;
		}
	}

	private class ICalendarVAlarmAudio extends ICalendarVAlarmAction {
		protected String mSpeakText;

		public ICalendarVAlarmAudio() {
			super(ICalendarDefinitions.eAction_VAlarm_Audio);
		}

		public ICalendarVAlarmAudio(String speak) {
			super(ICalendarDefinitions.eAction_VAlarm_Audio);
			mSpeakText = speak;
		}

		public ICalendarVAlarmAudio(ICalendarVAlarmAudio copy) {
			super(copy);
			mSpeakText = new String(copy.mSpeakText);
		}

		public ICalendarVAlarmAction clone_it() {
			return new ICalendarVAlarmAudio(this);
		}

		public void load(ICalendarVAlarm valarm) {
			// Get properties
			mSpeakText = valarm
					.loadValueString(ICalendarDefinitions.cICalProperty_ACTION_X_SPEAKTEXT);
		}

		public void add(ICalendarVAlarm valarm) {
			// Delete existing then add
			remove(valarm);

			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_ACTION_X_SPEAKTEXT,
					mSpeakText);
			valarm.addProperty(prop);
		}

		public void remove(ICalendarVAlarm valarm) {
			valarm
					.removeProperties(ICalendarDefinitions.cICalProperty_ACTION_X_SPEAKTEXT);
		}

		public boolean isSpeakText() {
			return mSpeakText.length() != 0;
		}

		public String getSpeakText() {
			return mSpeakText;
		}
	}

	private class ICalendarVAlarmDisplay extends ICalendarVAlarmAction {
		protected String mDescription;

		public ICalendarVAlarmDisplay() {
			super(ICalendarDefinitions.eAction_VAlarm_Display);
		}

		public ICalendarVAlarmDisplay(String description) {
			super(ICalendarDefinitions.eAction_VAlarm_Display);
			mDescription = description;
		}

		public ICalendarVAlarmDisplay(ICalendarVAlarmDisplay copy) {
			super(copy);
			mDescription = copy.mDescription;
		}

		public ICalendarVAlarmAction clone_it() {
			return new ICalendarVAlarmDisplay(this);
		}

		public void load(ICalendarVAlarm valarm) {
			// Get properties
			mDescription = valarm
					.loadValueString(ICalendarDefinitions.cICalProperty_DESCRIPTION);
		}

		public void add(ICalendarVAlarm valarm) {
			// Delete existing then add
			remove(valarm);

			ICalendarProperty prop = new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DESCRIPTION,
					mDescription);
			valarm.addProperty(prop);
		}

		public void remove(ICalendarVAlarm valarm) {
			valarm
					.removeProperties(ICalendarDefinitions.cICalProperty_DESCRIPTION);
		}

		public String getDescription() {
			return mDescription;
		}
	}

	private class ICalendarVAlarmEmail extends ICalendarVAlarmAction {
		protected String mDescription;

		protected String mSummary;

		protected Vector mAttendees;

		public ICalendarVAlarmEmail() {
			super(ICalendarDefinitions.eAction_VAlarm_Email);
		}

		public ICalendarVAlarmEmail(String description, String summary,
				Vector attendees) {
			super(ICalendarDefinitions.eAction_VAlarm_Email);

			mDescription = description;
			mSummary = summary;
			mAttendees = attendees;
		}

		public ICalendarVAlarmEmail(ICalendarVAlarmEmail copy) {
			super(copy);

			mDescription = new String(copy.mDescription);
			mSummary = new String(copy.mSummary);
			mAttendees = new Vector();
			for (Enumeration e = copy.mAttendees.elements(); e
					.hasMoreElements();) {
				mAttendees.addElement(new String((String) e.nextElement()));
			}
		}

		public ICalendarVAlarmAction clone_it() {
			return new ICalendarVAlarmEmail(this);
		}

		public void load(ICalendarVAlarm valarm) {
			// Get properties
			mDescription = valarm
					.loadValueString(ICalendarDefinitions.cICalProperty_DESCRIPTION);
			mSummary = valarm
					.loadValueString(ICalendarDefinitions.cICalProperty_SUMMARY);

			mAttendees.removeAllElements();
			if (valarm.hasProperty(ICalendarDefinitions.cICalProperty_ATTENDEE)) {
				// Get each attendee
				Vector range = valarm.getProperties().findItems(
						ICalendarDefinitions.cICalProperty_ATTENDEE);
				for (Enumeration e = range.elements(); e.hasMoreElements();) {
					ICalendarProperty iter = (ICalendarProperty) e
							.nextElement();

					// Get the attendee value
					ICalendarCalAddressValue attendee = iter
							.getCalAddressValue();
					if (attendee != null) {
						mAttendees.addElement(attendee.getValue());
					}
				}
			}
		}

		public void add(ICalendarVAlarm valarm) {
			// Delete existing then add
			remove(valarm);

			{
				ICalendarProperty prop = new ICalendarProperty(
						ICalendarDefinitions.cICalProperty_DESCRIPTION,
						mDescription);
				valarm.addProperty(prop);
			}

			{
				ICalendarProperty prop = new ICalendarProperty(
						ICalendarDefinitions.cICalProperty_SUMMARY, mSummary);
				valarm.addProperty(prop);
			}

			for (Enumeration e = mAttendees.elements(); e.hasMoreElements();) {
				String iter = (String) e.nextElement();
				ICalendarProperty prop = new ICalendarProperty(
						ICalendarDefinitions.cICalProperty_ATTENDEE, iter,
						ICalendarValue.eValueType_CalAddress);
				valarm.addProperty(prop);
			}
		}

		public void remove(ICalendarVAlarm valarm) {
			valarm
					.removeProperties(ICalendarDefinitions.cICalProperty_DESCRIPTION);
			valarm.removeProperties(ICalendarDefinitions.cICalProperty_SUMMARY);
			valarm
					.removeProperties(ICalendarDefinitions.cICalProperty_ATTENDEE);
		}

		public String getDescription() {
			return mDescription;
		}

		public String getSummary() {
			return mSummary;
		}

		public Vector getAttendees() {
			return mAttendees;
		}
	}

	private class ICalendarVAlarmUnknown extends ICalendarVAlarmAction {
		public ICalendarVAlarmUnknown() {
			super(ICalendarDefinitions.eAction_VAlarm_Unknown);
		}

		public ICalendarVAlarmUnknown(ICalendarVAlarmUnknown copy) {
			super(copy);
		}

		public ICalendarVAlarmAction clone_it() {
			return new ICalendarVAlarmUnknown(this);
		}

		public void load(ICalendarVAlarm valarm) {
		}

		public void add(ICalendarVAlarm valarm) {
		}

		public void remove(ICalendarVAlarm valarm) {
		}
	}

	public static String getVBegin() {
		return sBeginDelimiter;
	}

	public static String getVEnd() {
		return sEndDelimiter;
	}

	public String getMimeComponentName() {
		// Cannot be sent as a separate MIME object
		return null;
	}

	public ICalendarVAlarm(int calendar) {
		super(calendar);

		mAction = ICalendarDefinitions.eAction_VAlarm_Display;
		mTriggerAbsolute = false;
		mTriggerOnStart = true;
		mTriggerOn = new ICalendarDateTime();

		// Set duration default to 1 hour
		mTriggerBy = new ICalendarDuration();
		mTriggerBy.setDuration(60 * 60);

		// Does not repeat by default
		mRepeats = 0;
		mRepeatInterval = new ICalendarDuration();
		mRepeatInterval.setDuration(5 * 60); // Five minutes

		// Status
		mStatusInit = false;
		mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Pending;
		mLastTrigger = new ICalendarDateTime();
		mNextTrigger = new ICalendarDateTime();
		mDoneCount = 0;

		// Create action data
		mActionData = new ICalendarVAlarmDisplay(new String());
	}

	public ICalendarVAlarm(ICalendarVAlarm copy) {
		super(copy);

		mAction = copy.mAction;
		mTriggerAbsolute = copy.mTriggerAbsolute;
		mTriggerOn = new ICalendarDateTime(copy.mTriggerOn);
		mTriggerBy = new ICalendarDuration(copy.mTriggerBy);
		mTriggerOnStart = copy.mTriggerOnStart;

		mRepeats = copy.mRepeats;
		mRepeatInterval = new ICalendarDuration(copy.mRepeatInterval);

		mAlarmStatus = copy.mAlarmStatus;
		if (copy.mLastTrigger != null)
			mLastTrigger = new ICalendarDateTime(copy.mLastTrigger);
		if (copy.mNextTrigger != null)
			mNextTrigger = new ICalendarDateTime(copy.mNextTrigger);
		mDoneCount = copy.mDoneCount;

		mActionData = copy.mActionData.clone_it();
	}

	public ICalendarComponent clone_it() {
		return new ICalendarVAlarm(this);
	}

	public int getType() {
		return ICalendarComponent.eVALARM;
	}

	public String getBeginDelimiter() {
		return sBeginDelimiter;
	}

	public String getEndDelimiter() {
		return sEndDelimiter;
	}

	public int getAction() {
		return mAction;
	}

	public ICalendarVAlarmAction getActionData() {
		return mActionData;
	}

	public boolean isTriggerAbsolute() {
		return mTriggerAbsolute;
	}

	public ICalendarDateTime getTriggerOn() {
		return mTriggerOn;
	}

	public ICalendarDuration getTriggerDuration() {
		return mTriggerBy;
	}

	public boolean isTriggerOnStart() {
		return mTriggerOnStart;
	}

	public int getRepeats() {
		return mRepeats;
	}

	public ICalendarDuration getInterval() {
		return mRepeatInterval;
	}

	public void added() {
		// Added to calendar so add to calendar notifier
		/* calstore::CCalendarNotifier::sCalendarNotifier.AddAlarm(this); */

		// Do inherited
		super.added();
	}

	public void removed() {
		// Removed from calendar so add to calendar notifier
		/* calstore::CCalendarNotifier::sCalendarNotifier.RemoveAlarm(this); */

		// Do inherited
		super.removed();
	}

	public void changed() {
		// Always force recalc of trigger status
		mStatusInit = false;

		// Changed in calendar so change in calendar notifier
		/* calstore::CCalendarNotifier::sCalendarNotifier.ChangedAlarm(this); */

		// Do not do inherited as this is always a sub-component and we do not
		// do top-level component changes
		// super.changed();
	}

	public void finalise() {
		// Do inherited
		super.finalise();

		// Get the ACTION
		{
			String temp = loadValueString(ICalendarDefinitions.cICalProperty_ACTION);
			if (temp != null) {
				if (temp
						.equals(ICalendarDefinitions.cICalProperty_ACTION_AUDIO)) {
					mAction = ICalendarDefinitions.eAction_VAlarm_Audio;
				} else if (temp
						.equals(ICalendarDefinitions.cICalProperty_ACTION_DISPLAY)) {
					mAction = ICalendarDefinitions.eAction_VAlarm_Display;
				} else if (temp
						.equals(ICalendarDefinitions.cICalProperty_ACTION_EMAIL)) {
					mAction = ICalendarDefinitions.eAction_VAlarm_Email;
				} else if (temp
						.equals(ICalendarDefinitions.cICalProperty_ACTION_PROCEDURE)) {
					mAction = ICalendarDefinitions.eAction_VAlarm_Procedure;
				} else {
					mAction = ICalendarDefinitions.eAction_VAlarm_Unknown;
				}
				loadAction();
			}
		}

		// Get the trigger
		if (getProperties().count(ICalendarDefinitions.cICalProperty_TRIGGER) != 0) {
			// Determine the type of the value
			if (loadValue(ICalendarDefinitions.cICalProperty_TRIGGER,
					mTriggerOn)) {
				mTriggerAbsolute = true;
			} else if (loadValue(ICalendarDefinitions.cICalProperty_TRIGGER,
					mTriggerBy)) {
				mTriggerAbsolute = false;

				// Get the property
				ICalendarProperty prop = (ICalendarProperty) getProperties()
						.findFirst(ICalendarDefinitions.cICalProperty_TRIGGER);

				// Look for RELATED attribute
				if (prop
						.hasAttribute(ICalendarDefinitions.cICalAttribute_RELATED)) {
					String temp = prop
							.getAttributeValue(ICalendarDefinitions.cICalAttribute_RELATED);
					if (temp == ICalendarDefinitions.cICalAttribute_RELATED_START) {
						mTriggerOnStart = true;
					} else if (temp == ICalendarDefinitions.cICalAttribute_RELATED_END) {
						mTriggerOnStart = false;
					}
				} else
					mTriggerOnStart = true;
			}
		}

		// Get repeat & interval
		Integer temp = loadValueInteger(ICalendarDefinitions.cICalProperty_REPEAT);
		if (temp != null)
			mRepeats = temp.intValue();
		loadValue(ICalendarDefinitions.cICalProperty_DURATION, mRepeatInterval);

		// Alarm status - private to Mulberry
		String status = loadValueString(ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS);
		if (status != null) {
			if (status
					.equals(ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_PENDING))
				mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Pending;
			else if (status
					.equals(ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_COMPLETED))
				mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Completed;
			else if (status
					.equals(ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_DISABLED))
				mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Disabled;
			else
				mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Pending;
		}

		// Last trigger time - private to Mulberry
		loadValue(ICalendarDefinitions.cICalProperty_ALARM_X_LASTTRIGGER,
				mLastTrigger);
	}

	public void editStatus(int status) {
		// Remove existing
		removeProperties(ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS);

		// Updated cached values
		mAlarmStatus = status;

		// Add new
		String status_txt = new String();
		if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Pending)
			status_txt = ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_PENDING;
		else if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Completed)
			status_txt = ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_COMPLETED;
		else if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Disabled)
			status_txt = ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_DISABLED;
		addProperty(new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS,
				status_txt));
	}

	public void editAction(int action, ICalendarVAlarmAction data) {
		// Remove existing
		removeProperties(ICalendarDefinitions.cICalProperty_ACTION);
		mActionData.remove(this);
		mActionData = null;

		// Updated cached values
		mAction = action;
		mActionData = data;

		// Add new properties to alarm
		String action_txt;
		switch (mAction) {
		case ICalendarDefinitions.eAction_VAlarm_Audio:
			action_txt = ICalendarDefinitions.cICalProperty_ACTION_AUDIO;
			break;
		case ICalendarDefinitions.eAction_VAlarm_Display:
			action_txt = ICalendarDefinitions.cICalProperty_ACTION_DISPLAY;
			break;
		case ICalendarDefinitions.eAction_VAlarm_Email:
			action_txt = ICalendarDefinitions.cICalProperty_ACTION_EMAIL;
			break;
		default:
			action_txt = ICalendarDefinitions.cICalProperty_ACTION_PROCEDURE;
			break;
		}
		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_ACTION, action_txt);
		addProperty(prop);

		mActionData.add(this);
	}

	public void editTrigger(ICalendarDateTime dt) {
		// Remove existing
		removeProperties(ICalendarDefinitions.cICalProperty_TRIGGER);

		// Updated cached values
		mTriggerAbsolute = true;
		mTriggerOn = dt;

		// Add new
		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_TRIGGER, dt);
		addProperty(prop);
	}

	public void editTrigger(ICalendarDuration duration, boolean trigger_start) {
		// Remove existing
		removeProperties(ICalendarDefinitions.cICalProperty_TRIGGER);

		// Updated cached values
		mTriggerAbsolute = false;
		mTriggerBy = duration;
		mTriggerOnStart = trigger_start;

		// Add new (with attribute)
		ICalendarProperty prop = new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_TRIGGER, duration);
		ICalendarAttribute attr = new ICalendarAttribute(
				ICalendarDefinitions.cICalAttribute_RELATED,
				trigger_start ? ICalendarDefinitions.cICalAttribute_RELATED_START
						: ICalendarDefinitions.cICalAttribute_RELATED_END);
		prop.addAttribute(attr);
		addProperty(prop);
	}

	public void editRepeats(int repeat, ICalendarDuration interval) {
		// Remove existing
		removeProperties(ICalendarDefinitions.cICalProperty_REPEAT);
		removeProperties(ICalendarDefinitions.cICalProperty_DURATION);

		// Updated cached values
		mRepeats = repeat;
		mRepeatInterval = interval;

		// Add new
		if (mRepeats > 0) {
			addProperty(new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_REPEAT, repeat));
			addProperty(new ICalendarProperty(
					ICalendarDefinitions.cICalProperty_DURATION, interval));
		}
	}

	public int getAlarmStatus() {
		return mAlarmStatus;
	}

	public void getNextTrigger(ICalendarDateTime dt) {
		if (!mStatusInit)
			initNextTrigger();
		dt.copy(mNextTrigger);
	}

	public boolean alarmTriggered(ICalendarDateTime dt) {
		// Remove existing
		removeProperties(ICalendarDefinitions.cICalProperty_ALARM_X_LASTTRIGGER);
		removeProperties(ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS);

		// Updated cached values
		mLastTrigger.copy(dt);

		if (mDoneCount < mRepeats) {
			dt.copy(mNextTrigger = mLastTrigger.add(mRepeatInterval));
			mDoneCount++;
			mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Pending;
		} else
			mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Completed;

		// Add new
		addProperty(new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_ALARM_X_LASTTRIGGER, dt));
		String status = new String();
		if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Pending)
			status = ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_PENDING;
		else if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Completed)
			status = ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_COMPLETED;
		else if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Disabled)
			status = ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS_DISABLED;
		addProperty(new ICalendarProperty(
				ICalendarDefinitions.cICalProperty_ALARM_X_ALARMSTATUS, status));

		// Now update dt to the next alarm time
		return mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Pending;
	}

	protected void loadAction() {
		// Delete current one
		mActionData = null;
		switch (mAction) {
		case ICalendarDefinitions.eAction_VAlarm_Audio:
			mActionData = new ICalendarVAlarmAudio();
			mActionData.load(this);
			break;
		case ICalendarDefinitions.eAction_VAlarm_Display:
			mActionData = new ICalendarVAlarmDisplay();
			mActionData.load(this);
			break;
		case ICalendarDefinitions.eAction_VAlarm_Email:
			mActionData = new ICalendarVAlarmEmail();
			mActionData.load(this);
			break;
		default:
			mActionData = new ICalendarVAlarmUnknown();
			mActionData.load(this);
			break;
		}
	}

	protected void initNextTrigger() {
		// Do not bother if its completed
		if (mAlarmStatus == ICalendarDefinitions.eAlarm_Status_Completed)
			return;
		mStatusInit = true;

		// Look for trigger immediately preceeding or equal to utc now
		ICalendarDateTime nowutc = ICalendarDateTime.getNowUTC();

		// Init done counter
		mDoneCount = 0;

		// Determine the first trigger
		ICalendarDateTime trigger = new ICalendarDateTime();
		getFirstTrigger(trigger);

		while (mDoneCount < mRepeats) {
			// See if next trigger is later than now
			ICalendarDateTime next_trigger = trigger.add(mRepeatInterval);
			if (next_trigger.gt(nowutc))
				break;
			mDoneCount++;
			trigger = next_trigger;
		}

		// Check for completion
		if (trigger.eq(mLastTrigger)) {
			if (mDoneCount == mRepeats) {
				mAlarmStatus = ICalendarDefinitions.eAlarm_Status_Completed;
				return;
			} else {
				trigger = trigger.add(mRepeatInterval);
				mDoneCount++;
			}
		}

		mNextTrigger = trigger;
	}

	protected void getFirstTrigger(ICalendarDateTime dt) {
		// If absolute trigger, use that
		if (isTriggerAbsolute()) {
			// Get the trigger on
			dt.copy(getTriggerOn());
		} else {
			// Get the parent embedder class (must be CICalendarComponentRecur
			// type)
			ICalendarComponentRecur owner = (ICalendarComponentRecur) getEmbedder();
			if (owner != null) {
				// Determine time at which alarm will trigger
				ICalendarDateTime trigger = isTriggerOnStart() ? owner
						.getStart() : owner.getEnd();

				// Offset by duration
				dt.copy(trigger.add(getTriggerDuration()));
			}
		}
	}
}