/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author cyrusdaboo
 * 
 */
public class ICalendarPeriod {

	protected ICalendarDateTime mStart;

	protected ICalendarDateTime mEnd;

	protected ICalendarDuration mDuration;

	protected boolean mUseDuration;

	/**
	 * 
	 */
	public ICalendarPeriod() {
		super();
		mStart = new ICalendarDateTime();
		mEnd = new ICalendarDateTime();
		mDuration = new ICalendarDuration();
		mUseDuration = false;
	}

	public ICalendarPeriod(ICalendarDateTime start, ICalendarDateTime end) {
		mStart = start;
		mEnd = end;
		mDuration = mEnd.subtract(mStart);
		mUseDuration = false;
	}

	public ICalendarPeriod(ICalendarDateTime start, ICalendarDuration duration) {
		mStart = start;
		mDuration = duration;
		mEnd = mStart.add(mDuration);
		mUseDuration = true;
	}

	public ICalendarPeriod(ICalendarPeriod copy) {
		_copy_ICalendarPeriod(copy);
	}

	private void _copy_ICalendarPeriod(ICalendarPeriod copy) {
		mStart = new ICalendarDateTime(copy.mStart);
		mEnd = new ICalendarDateTime(copy.mEnd);
		mDuration = new ICalendarDuration(copy.mDuration);
		mUseDuration = copy.mUseDuration;
	}

	public boolean equals(ICalendarPeriod comp) {
		return mStart.equals(comp.mStart) && mEnd.equals(comp.mEnd);
	}

	public boolean gt(ICalendarDateTime comp) {
		return mStart.gt(comp);
	}

	public boolean lt(ICalendarPeriod comp) {
		return mStart.lt(comp.mStart)
				|| (mStart.eq(comp.mStart) && mEnd.lt(comp.mEnd));
	}

	public void copy(ICalendarPeriod copy) {
		_copy_ICalendarPeriod(copy);
	}

	public void parse(String data) {
		int slash_pos = data.indexOf('/');
		if (slash_pos != -1) {
			String start = data.substring(0, slash_pos);
			String end = data.substring(slash_pos + 1);

			mStart.parse(start);
			if (end.charAt(0) == 'P') {
				mDuration.parse(end);
				mUseDuration = true;
				mEnd = mStart.add(mDuration);
			} else {
				mEnd.parse(data);
				mUseDuration = false;
				mDuration = mEnd.subtract(mStart);
			}
		}
	}

	public void generate(BufferedWriter os) {
		try {
			mStart.generate(os);
			os.write("/");
			if (mUseDuration)
				mDuration.generate(os);
			else
				mEnd.generate(os);
		} catch (IOException e) {
		}
	}

	public ICalendarDateTime getStart() {
		return mStart;
	}

	public ICalendarDateTime getEnd() {
		return mEnd;
	}

	public ICalendarDuration getDuration() {
		return mDuration;
	}

	public boolean isDateWithinPeriod(ICalendarDateTime dt) {
		// Inclusive start, exclusive end
		return dt.ge(mStart) && dt.lt(mEnd);
	}

	public boolean isDateBeforePeriod(ICalendarDateTime dt) {
		// Inclusive start
		return dt.lt(mStart);
	}

	public boolean isDateAfterPeriod(ICalendarDateTime dt) {
		// Exclusive end
		return dt.ge(mEnd);
	}

	boolean isPeriodOverlap(ICalendarPeriod p)
	{
		// Inclusive start, exclusive end
		return !(mStart.ge(p.mEnd) || mEnd.le(p.mStart));
	}

	public String describeDuration() {
		return "";
	}
}