/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarLocale {

	public static final int eLong = 0;

	public static final int eShort = 1;

	public static final int eAbbreviated = 2;

	private static final String cLongDays[] = { "Sunday", "Monday", "Tuesday",
			"Wednesday", "Thursday", "Friday", "Saturday" };

	private static final String cShortDays[] = { "Sun", "Mon", "Tue", "Wed",
			"Thu", "Fri", "Sat" };

	private static final String cAbbrevDays[] = { "S", "M", "T", "W", "T", "F",
			"S" };

	private static final String cLongMonths[] = { "", "January", "February",
			"March", "April", "May", "June", "July", "August", "September",
			"October", "November", "December" };

	private static final String cShortMonths[] = { "", "Jan", "Feb", "Mar",
			"Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	private static final String cAbbrevMonths[] = { "", "J", "F", "M", "A",
			"M", "J", "J", "A", "S", "O", "Nov", "Dec" };

	private static boolean s24HourTime = false;
	private static boolean sDDMMDate = false;

	//	 0..6 - Sunday - Saturday
	public static String getDay(int day, int strl) {
		switch (strl) {
		case eLong:
			return cLongDays[day];
		case eShort:
			return cShortDays[day];
		case eAbbreviated:
		default:
			return cAbbrevDays[day];
		}
	}

	//	 1..12 - January - December
	public static String getMonth(int month, int strl) {
		switch (strl) {
		case eLong:
			return cLongMonths[month];
		case eShort:
			return cShortMonths[month];
		case eAbbreviated:
		default:
			return cAbbrevMonths[month];
		}
	}

	//	 Use 24 hour time display
	public static boolean use24HourTime() {
		/**
		 * TODO get 24 hour option from system prefs
		 */
		return s24HourTime;
	}

	//	 Use DD/MM date display
	public static boolean useDDMMDate() {
		/**
		 * TODO get 24 hour option from system prefs
		 */
		return sDDMMDate;
	}

}