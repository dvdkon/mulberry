/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Enumeration;
import java.util.Vector;

import com.mulberrymail.utils.CompareFunctor;
import com.mulberrymail.utils.VectorUtils;

/**
 * @author cyrusdaboo
 *
 */
public class ICalendarDateTimeList extends Vector {
	
	private static class comparator extends CompareFunctor
	{
		
		public Object cloneObject(Object o) {

			ICalendarDateTime dt = (ICalendarDateTime)o;

			return new ICalendarDateTime(dt);
		}

		public int compare(Object o1, Object o2) {

			ICalendarDateTime dt1 = (ICalendarDateTime)o1;
			ICalendarDateTime dt2 = (ICalendarDateTime)o2;

			return dt1.compareDateTime(dt2);
		}
}

	public ICalendarDateTimeList()
	{
		super();
	}
	
	public ICalendarDateTimeList(ICalendarDateTimeList copy)
	{
		for(Enumeration e = copy.elements(); e.hasMoreElements(); )
			addElement(new ICalendarDateTime((ICalendarDateTime)e.nextElement()));
	}
	
	public void copy(ICalendarDateTimeList copy)
	{
		removeAllElements();
		for(Enumeration e = copy.elements(); e.hasMoreElements(); )
			addElement(new ICalendarDateTime((ICalendarDateTime)e.nextElement()));
	}

	public void sort()
	{
		VectorUtils.sort(this, new comparator());
	}
	
	public void unique()
	{
		VectorUtils.unique(this, new comparator());
	}
	
	public boolean equal(ICalendarDateTimeList comp)
	{
		return VectorUtils.equal(this, comp, new comparator());
	}
	
	public static void set_difference(ICalendarDateTimeList set1, ICalendarDateTimeList set2, ICalendarDateTimeList result)
	{
		result.removeAllElements();
		VectorUtils.set_difference(set1, set2, result, new comparator());
	}
	
	public void removeOnOrAfter(ICalendarDateTime dt)
	{
		/**
		 * TODO
		 */
	}
}
