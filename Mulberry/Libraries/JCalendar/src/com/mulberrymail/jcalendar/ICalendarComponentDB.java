/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.util.Enumeration;
import java.util.Vector;

import com.mulberrymail.utils.SimpleMap;

/**
 * @author cyrusdaboo
 *  
 */
public class ICalendarComponentDB extends SimpleMap {

	protected SimpleMap mRecurMap;

	public ICalendarComponentDB() {
		mRecurMap = null;
	}

	public ICalendarComponentDB(ICalendarComponentDB copy) {
		super(copy);
		mRecurMap = null;
		if (copy.mRecurMap != null) {
			mRecurMap = new SimpleMap();
			for (Enumeration e = copy.mRecurMap.keys(); e.hasMoreElements();) {
				String key = (String) e.nextElement();
				ICalendarDateTimeList dtl = (ICalendarDateTimeList) copy.mRecurMap
						.get(key);
				mRecurMap.put(key, new ICalendarDateTimeList(dtl));
			}
		}
	}

	public void close()
	{
		for (Enumeration iter = elements(); iter.hasMoreElements();) {
			// Tell component it is removed and delete it
			((ICalendarComponent) iter.nextElement()).close();
		}
		clear();
	}

	public boolean addComponent(ICalendarComponent comp) {
		// Must have valid UID
		if (comp.getMapKey().length() == 0)
			return false;

		// Add the component to the calendar
		boolean bresult = true;

		// See if duplicate
		if (containsKey(comp.getMapKey())) {
			ICalendarComponent result = (ICalendarComponent) get(comp
					.getMapKey());

			// Replace existing if sequence is higher
			if (comp.getSeq() > result.getSeq()) {
				put(comp.getMapKey(), comp);
				bresult = true;
			} else
				bresult = false;
		} else {
			put(comp.getMapKey(), comp);
			bresult = true;
		}

		// Now look for a recurrence component if it was added
		if (bresult
				&& (comp instanceof ICalendarComponentRecur)) {

			ICalendarComponentRecur recur = (ICalendarComponentRecur) comp;

			// Add each overridden instance to the override map
			if (recur.isRecurrenceInstance()) {
				// Look for existiung UID->RID map entry\
				if (mRecurMap == null)
					mRecurMap = new SimpleMap();
				ICalendarDateTimeList found = (ICalendarDateTimeList) mRecurMap
						.get(recur.getUID());
				if (found != null) {
					// Add to existing list
					found.addElement(recur.getRecurrenceID());
				} else {
					// Create new entry for this UID
					ICalendarDateTimeList temp = new ICalendarDateTimeList();
					temp.addElement(recur.getRecurrenceID());
					mRecurMap.put(recur.getUID(), temp);
				}

				// Now try and find the master component if it currently exists
				ICalendarComponent found2 = (ICalendarComponent) get(recur
						.getUID());
				if (found2 != null) {
					// Tell the instance who its master is
					recur.setMaster((ICalendarComponentRecur) found2);
				}
			}

			// Make sure the master is sync'd with any instances that may have
			// been
			// added before it, those added after
			// will be sync'd when they are added
			else if (mRecurMap != null) {
				// See if master has an entry in the UID->RID map
				ICalendarDateTimeList found = (ICalendarDateTimeList) mRecurMap
						.get(comp.getUID());
				if (found != null) {
					// Make sure each instance knows about its master
					for (Enumeration e = found.elements(); e.hasMoreElements();) {
						ICalendarDateTime iter = (ICalendarDateTime) e
								.nextElement();

						// Get the instance
						ICalendarComponentRecur instance = getRecurrenceInstance(
								comp.getUID(), iter);
						if (instance != null) {
							// Tell the instance who its master is
							instance.setMaster(recur);
						}
					}
				}
			}
		}

		// Tell component it has now been added
		if (bresult)
			comp.added();

		return bresult;
	}

	public void removeComponent(ICalendarComponent comp) {
		// Tell component it is removed
		comp.removed();

		// Only if present
		remove(comp.getMapKey());
	}

	public void removeAllComponents() {
		for (Enumeration iter = elements(); iter.hasMoreElements();) {
			// Tell component it is removed and delete it
			ICalendarComponent comp = (ICalendarComponent)iter.nextElement();
			comp.removed();
			comp.close();
		}

		clear();
	}

	public void changedComponent(ICalendarComponent comp) {
		// Tell component it is changed
		comp.changed();
	}

	public void getRecurrenceInstances(String uid, ICalendarDateTimeList ids) {
		if (mRecurMap == null)
			return;

		// Look for matching UID in recurrence instance map
		ICalendarDateTimeList found = (ICalendarDateTimeList) mRecurMap
				.get(uid);
		if (found != null) {
			// Return the recurrence ids
			ids = found;
		}
	}

	public void getRecurrenceInstances(String uid, Vector items) {
		if (mRecurMap == null)
			return;

		// Look for matching UID in recurrence instance map
		ICalendarDateTimeList found = (ICalendarDateTimeList) mRecurMap
				.get(uid);
		if (found != null) {
			// Return all the recurrence ids
			for (Enumeration e = found.elements(); e.hasMoreElements();) {
				ICalendarDateTime iter = (ICalendarDateTime) e.nextElement();

				// Look it up
				ICalendarComponentRecur recur = getRecurrenceInstance(uid, iter);
				if (recur != null)
					items.addElement(recur);
			}
		}
	}

	protected ICalendarComponentRecur getRecurrenceInstance(String uid,
			ICalendarDateTime rid) {
		ICalendarComponent found = (ICalendarComponent) get(ICalendarComponentRecur
				.mapKey(uid, rid.getText()));
		if (found != null) {
			// Tell the instance who its master is
			return (ICalendarComponentRecur) found;
		}

		return null;
	}
}