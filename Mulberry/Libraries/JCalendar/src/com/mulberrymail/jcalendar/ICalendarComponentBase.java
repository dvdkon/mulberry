/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.jcalendar;

import java.io.BufferedWriter;
import java.util.Enumeration;
import java.util.Vector;

import com.mulberrymail.utils.MultiMap;

/**
 * @author cyrusdaboo
 * 
 */
public abstract class ICalendarComponentBase {

	protected MultiMap mProperties;

	public ICalendarComponentBase() {
		mProperties = new MultiMap();
	}

	public ICalendarComponentBase(ICalendarComponentBase copy) {
		mProperties = new MultiMap(copy.mProperties);
	}

	public MultiMap getProperties() {
		return mProperties;
	}

	public void setProperties(MultiMap props) {
		mProperties = props;
	}

	public void addProperty(ICalendarProperty prop) {
		mProperties.insert(prop.getName(), prop);
	}

	public boolean hasProperty(String prop) {
		return mProperties.count(prop) > 0;
	}

	public int countProperty(String prop) {
		return mProperties.count(prop);
	}

	public void removeProperties(String prop) {
		mProperties.erase(prop);
	}

	public Integer getPropertyInteger(String prop) {
		return loadValueInteger(prop);
	}

	public Integer getPropertyInteger(String prop, int type) {
		return loadValueInteger(prop, type);
	}

	public String getPropertyString(String prop) {
		return loadValueString(prop);
	}

	public boolean getProperty(String prop, ICalendarDateTime value) {
		return loadValue(prop, value);
	}

	public abstract void finalise();

	public abstract void generate(BufferedWriter os, boolean for_cache);

	public abstract void generateFiltered(BufferedWriter os,
			ICalendarOutputFilter filter);

	protected Integer loadValueInteger(String value_name) {
		return loadValueInteger(value_name, ICalendarValue.eValueType_Integer);
	}

	protected Integer loadValueInteger(String value_name, int type) {
		if (getProperties().count(value_name) != 0) {
			switch (type) {
			case ICalendarValue.eValueType_Integer: {
				ICalendarIntegerValue ivalue = ((ICalendarProperty) getProperties()
						.findFirst(value_name)).getIntegerValue();
				if (ivalue != null) {
					return new Integer(ivalue.getValue());
				}
				break;
			}
			case ICalendarValue.eValueType_UTC_Offset: {
				ICalendarUTCOffsetValue uvalue = ((ICalendarProperty) getProperties()
						.findFirst(value_name)).getUTCOffsetValue();
				if (uvalue != null) {
					return new Integer(uvalue.getValue());
				}
				break;
			}
			default:
				;
			}
		}

		return null;
	}

	protected String loadValueString(String value_name) {
		if (getProperties().count(value_name) != 0) {
			ICalendarPlainTextValue tvalue = ((ICalendarProperty) getProperties()
					.findFirst(value_name)).getTextValue();
			if (tvalue != null) {
				return tvalue.getValue();
			}
		}

		return null;
	}

	protected boolean loadValue(String value_name, ICalendarDateTime value) {
		if (getProperties().count(value_name) != 0) {
			ICalendarDateTimeValue dtvalue = ((ICalendarProperty) getProperties()
					.findFirst(value_name)).getDateTimeValue();
			if (dtvalue != null) {
				value.copy(dtvalue.getValue());
				return true;
			}
		}

		return false;
	}

	protected boolean loadValue(String value_name, ICalendarDuration value) {
		if (getProperties().count(value_name) != 0) {
			ICalendarDurationValue dvalue = ((ICalendarProperty) getProperties()
					.findFirst(value_name)).getDurationValue();
			if (dvalue != null) {
				value.copy(dvalue.getValue());
				return true;
			}
		}

		return false;
	}

	protected boolean loadValue(String value_name, ICalendarPeriod value) {
		if (getProperties().count(value_name) != 0) {
			ICalendarPeriodValue pvalue = ((ICalendarProperty) getProperties()
					.findFirst(value_name)).getPeriodValue();
			if (pvalue != null) {
				value.copy(pvalue.getValue());
				return true;
			}
		}

		return false;
	}

	protected boolean loadValueRRULE(String value_name,
			ICalendarRecurrenceSet value, boolean add) {
		// Get RRULEs
		if (getProperties().count(value_name) != 0) {
			Vector items = getProperties().findItems(value_name);
			for (Enumeration e = items.elements(); e.hasMoreElements();) {
				ICalendarProperty iter = (ICalendarProperty) e.nextElement();
				ICalendarRecurrenceValue rvalue = iter.getRecurrenceValue();
				if (rvalue != null) {
					if (add)
						value.add(rvalue.getValue());
					else
						value.subtract(rvalue.getValue());
				}
			}
			return true;
		} else
			return false;
	}

	protected boolean loadValueRDATE(String value_name,
			ICalendarRecurrenceSet value, boolean add) {
		// Get RDATEs
		if (getProperties().count(value_name) != 0) {
			Vector items = getProperties().findItems(value_name);
			for (Enumeration eq = items.elements(); eq.hasMoreElements();) {
				ICalendarProperty iter = (ICalendarProperty) eq.nextElement();
				ICalendarMultiValue mvalue = iter.getMultiValue();
				if (mvalue != null) {
					for (Enumeration e2 = mvalue.getValues().elements(); e2
							.hasMoreElements();) {
						// cast to date-time
						Object obj = e2.nextElement();
						if (obj instanceof ICalendarDateTimeValue) {
							ICalendarDateTimeValue dtv = (ICalendarDateTimeValue) obj;
							if (add)
								value.add(dtv.getValue());
							else
								value.subtract(dtv.getValue());
						} else if (obj instanceof ICalendarPeriodValue) {
							ICalendarPeriodValue dtp = (ICalendarPeriodValue) obj;
							if (add)
								value.add(dtp.getValue().getStart());
							else
								value.subtract(dtp.getValue().getStart());
						}

					}
				}
			}
			return true;
		} else
			return false;
	}

	protected void writeProperties(BufferedWriter os) {
		for (Enumeration e = mProperties.iterator(); e.hasMoreElements();)
			((ICalendarProperty) e.nextElement()).generate(os);
	}

	protected void writePropertiesFiltered(BufferedWriter os,
			ICalendarOutputFilter filter) {

		// Shortcut for all properties
		if (filter.isAllProperties()) {
			for (Enumeration e = mProperties.iterator(); e.hasMoreElements();)
				((ICalendarProperty) e.nextElement()).generate(os);
		} else if (filter.hasPropertyFilters()) {
			for (Enumeration e = mProperties.iterator(); e.hasMoreElements();)
				((ICalendarProperty) e.nextElement()).generateFiltered(os,
						filter);
		}
	}

	protected String loadPrivateValue(String value_name) {
		// Read it in from properties list and then delete the property from the
		// main list
		String result = loadValueString(value_name);
		if (result != null)
			removeProperties(value_name);
		return result;
	}

	protected void writePrivateProperty(BufferedWriter os, String key,
			String value) {
		ICalendarProperty prop = new ICalendarProperty(key, value);
		prop.generate(os);
	}

	protected void editProperty(String propname, String propvalue) {

		// Remove existing items
		removeProperties(propname);

		// Now create properties
		if ((propvalue != null) && (propvalue.length() != 0)) {
			addProperty(new ICalendarProperty(propname, propvalue));
		}

	}

}