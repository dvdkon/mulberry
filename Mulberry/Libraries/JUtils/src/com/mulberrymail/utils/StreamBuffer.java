/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.utils;

import java.io.IOException;
import java.io.Reader;

/**
 * @author cyrusdaboo
 *  
 */
public class StreamBuffer {

	private final int cBufferSize = 8192;

	private Reader mStream;

	private String mData;

	private char[] mBuffer;

	private int bnext;

	private int beof;

	private int bend;

	private boolean bfail;

	private int bcount;

	public StreamBuffer() {
		mStream = null;
		mData = null;
		bnext = beof = bend = 0;
		bfail = false;
		bcount = 0;
	}

	public StreamBuffer(Reader is) {
		mStream = null;
		mData = null;
		bnext = beof = bend = 0;
		bfail = false;
		bcount = 0;

		setStream(is);
	}

	public void setStream(Reader is) {
		// Create internal buffer
		mBuffer = new char[cBufferSize];
		beof = bnext = 0;
		bend = cBufferSize;

		// Assign stream and read in first block
		mStream = is;
		fillFromStream();
	}

	public void setData(String data) {
		// Assign data
		mData = data;

		// Set internal buffer
		mBuffer = mData.toCharArray();
		bnext = 0;
		beof = bend = data.length();
	}

	public boolean hasData() {
		return bnext != 0;
	}

	public void needData(int amount) {
		// Ensure that remaining data in buffer is at least amount bytes long
		if (remaining() < amount) {
			readMore();
		}
	}

	public char next() {
		return mBuffer[bnext];
	}

	public char nextpp() {
		return get();
	}

	public void ignore() {
		// Get single char and ignore
		get();
	}

	public String starts(int count)
	{
		needData(count);
		return new String(mBuffer, bnext, count);
	}

	public void skip(int bump) {
		// Fill the buffer with more
		needData(bump);

		// Verify we have enough
		if (bnext + bump > beof) {
			bfail = true;
			bcount = beof - bnext;
			bnext = beof;
		} else {
			bnext += bump;
			bcount += bump;
		}
	}

	public int count() {
		return bcount;
	}

	public int remaining() {
		return beof - bnext;
	}

	public boolean fail() {
		return bfail;
	}

	private char get() {
		// Load more into buffer
		if (bnext == beof) {
			readMore();
		}

		// If no more then we are done
		if (bnext == beof) {
			bfail = true;
			return 0;
		}

		bcount++;
		return mBuffer[bnext++];
	}

	private void readMore() {
		// Not if using fixed buffer
		if (mData != null)
			return;

		int bytes_used = bnext;
		int bytes_to_copy = beof - bnext;
		if (bytes_to_copy != 0) {
			// Shift down the remaining bytes so we can append some more
			for (int i = 0; i < bytes_to_copy; i++)
				mBuffer[i] = mBuffer[i + bnext];
		}

		// Adjust buffer pointers
		bnext = 0;
		beof -= bytes_used;

		// Fill remaining
		fillFromStream();
	}

	private void fillFromStream() {
		// Not if using fixed buffer
		if (mData != null)
			return;

		// Read as much fromt he stream as possible

		// Determine how much can be read in
		int remaining_space = bend - beof;

		// Read in up to that much and adjust count to the number actually read
		// in
		try {
			int count = mStream.read(mBuffer, beof, remaining_space);
			if (count == -1)
				bfail = true;
			else
				beof += count;
		} catch (IOException e) {
			bfail = true;
		}
	}

}