/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.utils;

import java.security.MessageDigest;

/**
 * @author cyrusdaboo
 * 
 */
public class StringUtils {

	public static final String strduptokenstr(Stringref txt, String tokens) {
		String result = null;
		int start = 0;

		// First punt over any leading space
		while ((start < txt.get().length()) && (txt.get().charAt(start) == ' '))
			start++;
		if (start == txt.get().length()) {
			txt.set(new String());
			return null;
		}

		// Handle quoted string
		if (txt.get().charAt(start) == '\"') {
			// Punt leading quote
			int end = ++start;

			boolean done = false;
			while (!done) {
				if (end == txt.get().length())
					return null;

				switch (txt.get().charAt(end)) {
				case '\"':
					done = true;
					break;
				case '\\':
					// Punt past quote
					end++;
					break;
				default:
					end++;
					break;
				}

				if (end == txt.get().length())
					return null;
			}

			result = txt.get().substring(start, end);
			txt.set(txt.get().substring(end + 1));

			return result;
		} else {
			int end = start;
			for (; end < txt.get().length(); end++) {
				if (tokens.indexOf(txt.get().charAt(end)) != -1) {
					// Grab portion of string upto delimiter
					if (end > start)
						result = txt.get().substring(start, end);
					else
						result = new String();
					txt.set(txt.get().substring(end));
					return result;
				}
			}

			result = txt.get().substring(start);
			txt.set(new String());

			return result;
		}
	}

	public static final int strindexfind(String s, String[] ss,
			int default_index) {
		if ((s != null) && (ss != null)) {
			int i = 0;
			while (ss[i] != null) {
				if (s.equalsIgnoreCase(ss[i]))
					return i;
				i++;
			}
		}

		return default_index;
	}

	public static final int strtoi(Stringref txt) {
		int result = 0;
		boolean positive = true;
		int start = 0;

		// First punt over any leading space
		while ((start < txt.get().length()) && (txt.get().charAt(start) == ' '))
			start++;
		if (start == txt.get().length()) {
			txt.set(new String());
			return 0;
		}

		// Look for +/- at start
		if (txt.get().charAt(start) == '-') {
			positive = false;
			start++;
		} else if (txt.get().charAt(start) == '+')
			start++;
		if (start == txt.get().length()) {
			txt.set(new String());
			return 0;
		}

		// Look for valid digits
		int end = start;
		while ((end < txt.get().length())
				&& Character.isDigit(txt.get().charAt(end)))
			end++;

		// Parse as int
		String temp = txt.get().substring(start, end);
		result = Integer.parseInt(temp);
		txt.set(txt.get().substring(end));

		return positive ? result : -result;
	}

	public static final int strnindexfind(String s, String[] ss,
			int default_index) {
		if ((s != null) && (ss != null)) {
			int i = 0;
			String su = s.toUpperCase();
			while (ss[i] != null) {
				if (su.startsWith(ss[i]))
					return i;
				i++;
			}
		}

		return default_index;
	}

	public static final boolean compareStringsSafe(String s1, String s2) {
		if (s1 == s2)
			return true;

		if ((s1 == null) ^ (s2 == null))
			return false;

		return s1.equals(s2);
	}

	private static final String byteToHex = "0123456789ABCDEF";

	public static final String md5(String txt) {

		String result = new String();

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] digested = md.digest(txt.getBytes());
			for (int i = 0; i < ((digested.length > 12) ? 12 : digested.length); i++) {
				byte b = (byte) (digested[i] & 0xF0 >> 4);
				result += byteToHex.charAt(b);
				b = (byte) (digested[i] & 0x0F);
				result += byteToHex.charAt(b);
			}
		} catch (Exception e) {
			md = null;
		} finally {
			md = null;
		}

		return result;
	}
}