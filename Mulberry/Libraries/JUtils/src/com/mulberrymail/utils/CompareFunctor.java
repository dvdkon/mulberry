/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.utils;

/**
 * @author cyrusdaboo
 *  
 */
public abstract class CompareFunctor {

	public CompareFunctor() {
	}

	public abstract int compare(Object o1, Object o2);

	public final boolean eq(Object o1, Object o2)
	{
		return compare(o1, o2) == 0;
	}
	public final boolean ne(Object o1, Object o2)
	{
		return compare(o1, o2) != 0;
	}
	public final boolean lt(Object o1, Object o2)
	{
		return compare(o1, o2) < 0;
	}
	public final boolean le(Object o1, Object o2)
	{
		return compare(o1, o2) <= 0;
	}
	public final boolean gt(Object o1, Object o2)
	{
		return compare(o1, o2) > 0;
	}
	public final boolean ge(Object o1, Object o2)
	{
		return compare(o1, o2) >= 0;
	}
	
	public abstract Object cloneObject(Object o);
}