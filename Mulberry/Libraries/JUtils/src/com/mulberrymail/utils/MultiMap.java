/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.utils;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author cyrusdaboo
 * 
 * This is a multimap implementation that uses Vectors to store multiple values
 */
public class MultiMap extends SimpleMap {

	/**
	 * Create a new multimap object
	 */
	public MultiMap() {
		super();
	}

	public MultiMap(MultiMap copy) {
		super();

		// Do deep copy of map
		for (Enumeration e1 = copy.keys(); e1.hasMoreElements();) {
			Object key = e1.nextElement();
			Vector values = (Vector) copy.get(key);
			for (Enumeration e2 = values.elements(); e2.hasMoreElements();) {
				insert(key, e2.nextElement());
			}
		}
	}

	/**
	 * Insert a key/value pair. Creates a new Vector with value as its only
	 * content if key does not already exist. Otherwise the value is added to
	 * the end of the existing Vector for the key.
	 * 
	 * @param key
	 * @param value
	 */
	public void insert(Object key, Object value) {
		// First see if key is already present
		if (containsKey(key)) {
			// Add this value to the end of the current list
			Vector value_list = findItems(key);
			value_list.addElement(value);
		} else {
			Vector value_list = new Vector();
			value_list.addElement(value);
			put(key, value_list);
		}
	}

	/**
	 * Remove the key-value pair from the map
	 * 
	 * @param key
	 */
	public void erase(Object key) {
		remove(key);
	}

	/**
	 * Find the Vector for the chosen key if it exists.
	 * 
	 * @param key
	 * @return - null returned if key does not exist, otherwise Vector
	 */
	public Vector findItems(Object key) {
		if (containsKey(key)) {
			return (Vector) get(key);
		} else
			return null;
	}

	/**
	 * Find first matching value for the chosen key if it exists.
	 * 
	 * @param key
	 * @return - null returned if key does not exist, otherwise Vector
	 */
	public Object findFirst(Object key) {
		if (containsKey(key)) {
			return ((Vector) get(key)).firstElement();
		} else
			return null;
	}

	/**
	 * Count the number of entries for the chosen key.
	 * 
	 * @param key
	 * @return
	 */
	public int count(Object key) {
		if (containsKey(key)) {
			return ((Vector) get(key)).size();
		} else
			return 0;
	}

	public Enumeration iterator() {
		return new MMEnumeration(this);
	}

	private class MMEnumeration implements Enumeration {

		private Enumeration mCurrentElement;

		private Enumeration mCurrentValue;

		public MMEnumeration(MultiMap map) {
			mCurrentElement = map.elements();
			mCurrentValue = null;
		}

		public boolean hasMoreElements() {
			// Check whether current values vector has more
			while ((mCurrentValue == null) || !mCurrentValue.hasMoreElements()) {
				// See if current set has more
				if (mCurrentElement.hasMoreElements()) {
					// Get the next vector enumeration
					Vector values = (Vector) mCurrentElement.nextElement();
					mCurrentValue = values.elements();
				} else
					return false;
			}

			return true;
		}

		public Object nextElement() {
			return mCurrentValue.nextElement();
		}
	}
}