/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.utils;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

/**
 * @author cyrusdaboo
 * 
 */
public class VectorUtils {

	public static final int find(Vector v, Object comp) {
		int index = 0;
		for (Enumeration e = v.elements(); e.hasMoreElements(); index++) {
			if (e.nextElement() == comp)
				return index;
		}

		return -1;
	}

	public static final void sort(Vector v, CompareFunctor comp) {
		/**
		 * Crude selection sort
		 */
		int max_count = v.size();
		for (int i = 0; i < max_count; i++) {
			int min_element = i;
			for (int j = i; j < max_count; j++) {
				if (comp.lt(v.elementAt(j), v.elementAt(min_element)))
					min_element = j;
			}
			if (min_element != i) {
				Object o1 = v.elementAt(i);
				Object o2 = v.elementAt(min_element);
				v.setElementAt(o2, i);
				v.setElementAt(o1, min_element);
			}
		}
	}

	public static final void sort(Vector v, SortFunctor comp) {
		/**
		 * Crude selection sort
		 */
		int max_count = v.size();
		for (int i = 0; i < max_count; i++) {
			int min_element = i;
			for (int j = i; j < max_count; j++) {
				if (comp.less_than(v.elementAt(j), v.elementAt(min_element)))
					min_element = j;
			}
			if (min_element != i) {
				Object o1 = v.elementAt(i);
				Object o2 = v.elementAt(min_element);
				v.setElementAt(o2, i);
				v.setElementAt(o1, min_element);
			}
		}
	}

	public static final void unique(Vector v, CompareFunctor comp) {
		/**
		 * Crude selection sort
		 */
		int max_count = v.size();
		for (int i = 0; i < max_count - 1; i++) {
			while ((i < max_count - 1)
					&& comp.eq(v.elementAt(i), v.elementAt(i + 1))) {
				v.removeElementAt(i + 1);
				max_count--;
			}
		}
	}

	public static final boolean equal(Vector v1, Vector v2, CompareFunctor comp) {
		
		if (v1.size() != v2.size())
			return false;

		/**
		 * Scan and compare all elements
		 */
		int max_count = v1.size();
		for (int i = 0; i < max_count - 1; i++) {
			if (!comp.eq(v1.elementAt(i), v2.elementAt(i)))
				return false;
		}
		
		return true;
	}

	public static final void set_union(Vector v1, Vector v2, Vector v3,
			CompareFunctor comp) {

		Iterator iter1 = v1.iterator();
		Iterator iter2 = v2.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			Object o1 = iter1.next();
			Object o2 = iter2.next();
			if (comp.lt(o2, o1)) {
				v3.addElement(comp.cloneObject(o2));
				o2 = iter2.next();
			} else if (comp.lt(o1, o2)) {
				v3.addElement(comp.cloneObject(o1));
				o1 = iter1.next();
			} else {
				v3.addElement(comp.cloneObject(o1));
				o1 = iter1.next();
				o2 = iter2.next();
			}
		}

		if (iter1.hasNext()) {
			while (iter1.hasNext()) {
				Object o = iter1.next();
				v3.addElement(comp.cloneObject(o));
			}
		} else {
			while (iter2.hasNext()) {
				Object o = iter2.next();
				v3.addElement(comp.cloneObject(o));
			}
		}
	}

	public static final void set_intersection(Vector v1, Vector v2, Vector v3,
			CompareFunctor comp) {

		Iterator iter1 = v1.iterator();
		Iterator iter2 = v2.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			Object o1 = iter1.next();
			Object o2 = iter2.next();
			if (comp.lt(o2, o1))
				o2 = iter2.next();
			else if (comp.lt(o1, o2))
				o1 = iter1.next();
			else {
				v3.addElement(comp.cloneObject(o1));
				o1 = iter1.next();
				o2 = iter2.next();
			}
		}
	}

	public static final void set_difference(Vector v1, Vector v2, Vector v3,
			CompareFunctor comp) {

		Iterator iter1 = v1.iterator();
		Iterator iter2 = v2.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			Object o1 = iter1.next();
			Object o2 = iter2.next();
			if (comp.lt(o1, o2)) {
				v3.addElement(comp.cloneObject(o1));
				o1 = iter1.next();
			} else if (comp.lt(o2, o1))
				o2 = iter2.next();
			else {
				o1 = iter1.next();
				o2 = iter2.next();
			}
		}

		while (iter1.hasNext()) {
			Object o = iter1.next();
			v3.addElement(comp.cloneObject(o));
		}
	}
}