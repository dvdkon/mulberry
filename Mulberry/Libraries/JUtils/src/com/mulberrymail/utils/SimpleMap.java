/*
    Copyright (c) 2007 Cyrus Daboo. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.mulberrymail.utils;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author cyrusdaboo
 * 
 */
public class SimpleMap extends Hashtable {

	public SimpleMap() {
		super();
	}

	public SimpleMap(SimpleMap copy) {
		super();
		
		// Deep copy of references
		for(Enumeration e = copy.keys(); e.hasMoreElements(); )
		{
			String key = (String)e.nextElement();
			insert(key, copy.find(key));
		}
	}

	/**
	 * Insert a key/value pair. Creates a new Vector with value as its only
	 * content if key does not already exist. Otherwise the value is added to
	 * the end of the existing Vector for the key.
	 * 
	 * @param key
	 * @param value
	 */
	public void insert(Object key, Object value) {
		// First see if key is already present
		put(key, value);
	}

	/**
	 * Remove the key-value pair from the map
	 * 
	 * @param key
	 */
	public void erase(Object key) {
		remove(key);
	}

	/**
	 * Find the Vector for the chosen key if it exists.
	 * 
	 * @param key
	 * @return - null returned if key does not exist, otherwise Vector
	 */
	public Object find(Object key) {
		if (containsKey(key)) {
			return get(key);
		} else
			return null;
	}

	/**
	 * Count the number of entries for the chosen key.
	 * 
	 * @param key
	 * @return
	 */
	public int count(Object key) {
		if (containsKey(key)) {
			return 1;
		} else
			return 0;
	}
}